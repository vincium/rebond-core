<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Permission;
use Rebond\Repository\Data;

class PermissionRepository extends BasePermissionRepository
{
    /**
     * Find all Permission
     * @param int $roleId
     * @param array $options = []
     * @return Permission[]
     */
    public static function loadByRoleId($roleId, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_permission permission');
        $db->buildQuery('join', 'core_role_permission rp ON rp.permission_id = permission.id');
        $db->buildQuery('where', 'rp.role_id = ' . $roleId);
        $db->extendQuery($options);
        return self::mapList($db);
    }

}
