<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Site;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseSiteRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = site
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'site')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.google_analytics AS ' . $alias . 'GoogleAnalytics, ' .
                $alias . '.keywords AS ' . $alias . 'Keywords, ' .
                $alias . '.description AS ' . $alias . 'Description, ' .
                $alias . '.css AS ' . $alias . 'Css, ' .
                $alias . '.js AS ' . $alias . 'Js, ' .
                $alias . '.sign_in_url AS ' . $alias . 'SignInUrl, ' .
                $alias . '.is_debug AS ' . $alias . 'IsDebug, ' .
                $alias . '.log_sql AS ' . $alias . 'LogSql, ' .
                $alias . '.timezone AS ' . $alias . 'Timezone, ' .
                $alias . '.is_cms AS ' . $alias . 'IsCms, ' .
                $alias . '.cache_time AS ' . $alias . 'CacheTime, ' .
                $alias . '.use_device_template AS ' . $alias . 'UseDeviceTemplate, ' .
                $alias . '.skin AS ' . $alias . 'Skin, ' .
                $alias . '.send_mail_on_error AS ' . $alias . 'SendMailOnError, ' .
                $alias . '.mail_list_on_error AS ' . $alias . 'MailListOnError, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(site.id)');
        $db->buildQuery('from', 'core_site `site`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Site
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Site() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_site `site`');
        $db->buildQuery('where', ['site.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Site();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Site
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_site `site`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return Site[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_site `site`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Site $model
     * @return int
     */
    public static function save(Site $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_site (`title`, `google_analytics`, `keywords`, `description`, `css`, `js`, `sign_in_url`, `is_debug`, `log_sql`, `timezone`, `is_cms`, `cache_time`, `use_device_template`, `skin`, `send_mail_on_error`, `mail_list_on_error`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getGoogleAnalytics(),
                $model->getKeywords(),
                $model->getDescription(),
                $model->getCss(),
                $model->getJs(),
                $model->getSignInUrl(),
                (int)$model->getIsDebug(),
                (int)$model->getLogSql(),
                $model->getTimezone(),
                (int)$model->getIsCms(),
                $model->getCacheTime(),
                (int)$model->getUseDeviceTemplate(),
                $model->getSkin(),
                (int)$model->getSendMailOnError(),
                $model->getMailListOnError(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_site SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getGoogleAnalytics() !== null) {
                $query .= '`google_analytics` = ?, ';
                $params[] = $model->getGoogleAnalytics();
            }
            if ($model->getKeywords() !== null) {
                $query .= '`keywords` = ?, ';
                $params[] = $model->getKeywords();
            }
            if ($model->getDescription() !== null) {
                $query .= '`description` = ?, ';
                $params[] = $model->getDescription();
            }
            if ($model->getCss() !== null) {
                $query .= '`css` = ?, ';
                $params[] = $model->getCss();
            }
            if ($model->getJs() !== null) {
                $query .= '`js` = ?, ';
                $params[] = $model->getJs();
            }
            if ($model->getSignInUrl() !== null) {
                $query .= '`sign_in_url` = ?, ';
                $params[] = $model->getSignInUrl();
            }
            if ($model->getIsDebug() !== null) {
                $query .= '`is_debug` = ?, ';
                $params[] = $model->getIsDebug();
            }
            if ($model->getLogSql() !== null) {
                $query .= '`log_sql` = ?, ';
                $params[] = $model->getLogSql();
            }
            if ($model->getTimezone() !== null) {
                $query .= '`timezone` = ?, ';
                $params[] = $model->getTimezone();
            }
            if ($model->getIsCms() !== null) {
                $query .= '`is_cms` = ?, ';
                $params[] = $model->getIsCms();
            }
            if ($model->getCacheTime() !== null) {
                $query .= '`cache_time` = ?, ';
                $params[] = $model->getCacheTime();
            }
            if ($model->getUseDeviceTemplate() !== null) {
                $query .= '`use_device_template` = ?, ';
                $params[] = $model->getUseDeviceTemplate();
            }
            if ($model->getSkin() !== null) {
                $query .= '`skin` = ?, ';
                $params[] = $model->getSkin();
            }
            if ($model->getSendMailOnError() !== null) {
                $query .= '`send_mail_on_error` = ?, ';
                $params[] = $model->getSendMailOnError();
            }
            if ($model->getMailListOnError() !== null) {
                $query .= '`mail_list_on_error` = ?, ';
                $params[] = $model->getMailListOnError();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_site SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_site WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = site
     * @return Site
     */
    public static function join(array $row, $alias = 'site')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Site
     */
    protected static function mapper(array $row, $alias = 'site')
    {
        $model = new Site(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'GoogleAnalytics'])) {
            $model->setGoogleAnalytics($row[$alias . 'GoogleAnalytics']);
        }
        if (isset($row[$alias . 'Keywords'])) {
            $model->setKeywords($row[$alias . 'Keywords']);
        }
        if (isset($row[$alias . 'Description'])) {
            $model->setDescription($row[$alias . 'Description']);
        }
        if (isset($row[$alias . 'Css'])) {
            $model->setCss($row[$alias . 'Css']);
        }
        if (isset($row[$alias . 'Js'])) {
            $model->setJs($row[$alias . 'Js']);
        }
        if (isset($row[$alias . 'SignInUrl'])) {
            $model->setSignInUrl($row[$alias . 'SignInUrl']);
        }
        if (isset($row[$alias . 'IsDebug'])) {
            $model->setIsDebug($row[$alias . 'IsDebug']);
        }
        if (isset($row[$alias . 'LogSql'])) {
            $model->setLogSql($row[$alias . 'LogSql']);
        }
        if (isset($row[$alias . 'Timezone'])) {
            $model->setTimezone($row[$alias . 'Timezone']);
        }
        if (isset($row[$alias . 'IsCms'])) {
            $model->setIsCms($row[$alias . 'IsCms']);
        }
        if (isset($row[$alias . 'CacheTime'])) {
            $model->setCacheTime($row[$alias . 'CacheTime']);
        }
        if (isset($row[$alias . 'UseDeviceTemplate'])) {
            $model->setUseDeviceTemplate($row[$alias . 'UseDeviceTemplate']);
        }
        if (isset($row[$alias . 'Skin'])) {
            $model->setSkin($row[$alias . 'Skin']);
        }
        if (isset($row[$alias . 'SendMailOnError'])) {
            $model->setSendMailOnError($row[$alias . 'SendMailOnError']);
        }
        if (isset($row[$alias . 'MailListOnError'])) {
            $model->setMailListOnError($row[$alias . 'MailListOnError']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
