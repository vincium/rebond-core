<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Enums\Core\Code;
use Rebond\Models\Core\User;
use Rebond\Repository\Data;
use Rebond\Models\DateTime;
use Rebond\Services\Log;
use Rebond\Services\Security;

class UserRepository extends BaseUserRepository
{
    /**
     * @param string $email
     * @param string $password
     * @return User|null
     */
    public static function loadByEmailAndPassword($email, $password)
    {
        $user = UserRepository::loadByEmail($email);
        if (!isset($user)) {
            return null;
        }

        if (!Security::isValidPassword($password, $user->getPassword())) {
            return null;
        }

        return $user;
    }

    /**
     * Has user access to resource
     * @param int $userId
     * @param int $permission
     * @param bool $isAdmin
     * @return bool
     */
    public static function hasAccess($userId, $permission, $isAdmin)
    {
        $db = new Data();
        $db->buildQuery('select', 'count(user.id)');
        $db->buildQuery('from', 'core_user user');
        $db->buildQuery('join', 'core_user_role user_role ON user_role.user_id = user.id');
        $db->buildQuery('join', 'core_role role ON role.id = user_role.role_id');
        $db->buildQuery('join', 'core_role_permission role_permission ON role_permission.role_id = user_role.role_id');
        $db->buildQuery('join', 'core_permission permission ON permission.id = role_permission.permission_id');
        $db->buildQuery('where', ['user.id = ?', $userId]);
        if ($isAdmin) {
            $db->buildQuery('where', 'user.is_admin = 1');
        }
        $db->buildQuery('where', ['permission.title LIKE CONCAT(?, "%")', $permission]);
        $db->buildQuery('where', 'user.status = 1');
        $db->buildQuery('where', 'role.status = 1');
        $db->buildQuery('where', 'permission.status = 1');
        return $db->count() > 0;
    }


    /**
     * @param int $userId
     * @return array
     */
    public static function getPermissions($userId)
    {
        $db = new Data();
        $db->buildQuery('select', 'permission.title');
        $db->buildQuery('from', 'core_user user');
        $db->buildQuery('join', 'core_user_role user_role ON user_role.user_id = user.id');
        $db->buildQuery('join', 'core_role role ON role.id = user_role.role_id');
        $db->buildQuery('join', 'core_role_permission role_permission ON role_permission.role_id = user_role.role_id');
        $db->buildQuery('join', 'core_permission permission ON permission.id = role_permission.permission_id');
        $db->buildQuery('where', ['user_role.user_id = ?', $userId]);
        $db->buildQuery('where', 'user.status = 1');
        $db->buildQuery('where', 'role.status = 1');
        $db->buildQuery('where', 'permission.status = 1');
        return $db->select()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param User $user
     * @return int
     */
    public static function savePassword(User $user)
    {
        Log::log(Code::PASSWORD_CHANGE, $user->getFullName() . ' [' . $user->getId() . ']', __FILE__, __LINE__);
        $db = new Data();
        $query = 'UPDATE core_user SET password = ?, modified_date = ? WHERE id = ?';
        return $db->execute($query, [$user->getPassword(), (new DateTime())->format('datetime'), $user->getId()]);
    }
}
