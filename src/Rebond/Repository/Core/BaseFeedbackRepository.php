<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Feedback;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseFeedbackRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = feedback
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'feedback')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.user_id AS ' . $alias . 'UserId, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.type AS ' . $alias . 'Type, ' .
                $alias . '.description AS ' . $alias . 'Description, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(feedback.id)');
        $db->buildQuery('from', 'core_feedback `feedback`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Feedback
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Feedback() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_feedback `feedback`');
        $db->buildQuery('where', ['feedback.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Feedback();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Feedback
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_feedback `feedback`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Feedback[]
     */
    public static function loadAllByUserId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_feedback `feedback`');
        $db->buildQuery('where', ['feedback.user_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Feedback[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_feedback `feedback`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Feedback $model
     * @return int
     */
    public static function save(Feedback $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_feedback (user_id, `title`, `type`, `description`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?)';
            $params = [
                $model->getUserId(),
                $model->getTitle(),
                $model->getType(),
                $model->getDescription(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_feedback SET ';
            $params = [];
            if ($model->getUserId() !== null) {
                $query .= 'user_id = ?, ';
                $params[] = $model->getUserId();
            }
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getType() !== null) {
                $query .= '`type` = ?, ';
                $params[] = $model->getType();
            }
            if ($model->getDescription() !== null) {
                $query .= '`description` = ?, ';
                $params[] = $model->getDescription();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_feedback SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_feedback WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByUserId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_feedback WHERE user_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = feedback
     * @return Feedback
     */
    public static function join(array $row, $alias = 'feedback')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Feedback
     */
    protected static function mapper(array $row, $alias = 'feedback')
    {
        $model = new Feedback(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'UserId'])) {
            $model->setUserId($row[$alias . 'UserId']);
            $model->setUser(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_user'));
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'Type'])) {
            $model->setType($row[$alias . 'Type']);
        }
        if (isset($row[$alias . 'Description'])) {
            $model->setDescription($row[$alias . 'Description']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
