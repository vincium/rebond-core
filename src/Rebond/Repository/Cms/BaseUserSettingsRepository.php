<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\UserSettings;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseUserSettingsRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = user_settings
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'user_settings')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.media_view AS ' . $alias . 'MediaView, ' .
                $alias . '.media_paging AS ' . $alias . 'MediaPaging, ' .
                $alias . '.content_paging AS ' . $alias . 'ContentPaging, ' .
                $alias . '.paging AS ' . $alias . 'Paging';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(user_settings.id)');
        $db->buildQuery('from', 'cms_user_settings `user_settings`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return UserSettings
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new UserSettings() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_user_settings `user_settings`');
        $db->buildQuery('where', ['user_settings.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new UserSettings();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return UserSettings
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_user_settings `user_settings`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return UserSettings[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_user_settings `user_settings`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param UserSettings $model
     * @return int
     */
    public static function save(UserSettings $model)
    {
        $db = new Data();
        $query = 'INSERT INTO cms_user_settings (id, `media_view`, `media_paging`, `content_paging`, `paging`) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), ';
        $params = [
            $model->getId(),
                $model->getMediaView(),
                $model->getMediaPaging(),
                $model->getContentPaging(),
                $model->getPaging() 
        ];
            if ($model->getMediaView() !== null) {
                $query .= '`media_view` = ?, ';
                $params[] = $model->getMediaView();
            }
            if ($model->getMediaPaging() !== null) {
                $query .= '`media_paging` = ?, ';
                $params[] = $model->getMediaPaging();
            }
            if ($model->getContentPaging() !== null) {
                $query .= '`content_paging` = ?, ';
                $params[] = $model->getContentPaging();
            }
            if ($model->getPaging() !== null) {
                $query .= '`paging` = ?, ';
                $params[] = $model->getPaging();
            }
 
        $query = rtrim(trim($query), ',');
        $id = $db->execute($query, $params);
        $model->setId($id);
        return $id;
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_user_settings WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = user_settings
     * @return UserSettings
     */
    public static function join(array $row, $alias = 'user_settings')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return UserSettings
     */
    protected static function mapper(array $row, $alias = 'user_settings')
    {
        $model = new UserSettings(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
            $model->setIdItem(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_id'));
        }
        if (isset($row[$alias . 'MediaView'])) {
            $model->setMediaView($row[$alias . 'MediaView']);
        }
        if (isset($row[$alias . 'MediaPaging'])) {
            $model->setMediaPaging($row[$alias . 'MediaPaging']);
        }
        if (isset($row[$alias . 'ContentPaging'])) {
            $model->setContentPaging($row[$alias . 'ContentPaging']);
        }
        if (isset($row[$alias . 'Paging'])) {
            $model->setPaging($row[$alias . 'Paging']);
        }
        return $model;
    }
}
