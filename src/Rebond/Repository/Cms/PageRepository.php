<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Repository\Cms;

use Rebond\Enums\Cms\Menu;
use Rebond\Enums\Core\Code;
use Rebond\Models\Cms\Page;
use Rebond\Repository\Data;
use Rebond\Services\Cms\PageService;
use Rebond\Services\Nav;

class PageRepository extends BasePageRepository
{
    /**
     * Find a Page
     * @param string $url
     * @return Page
     */
    public static function loadByUrl($url)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page page');
        $db->buildQuery('where', ['LOWER(CONCAT(page.friendly_url_path, page.friendly_url)) = ?', strtolower($url)]);
        $db->buildQuery('where', 'page.status = 1');
        $db->buildQuery('limit', 1);
        return self::map($db);
    }

    /**
     * Load multiple level of parent page id
     * @param int $pageId
     * @param int $depth
     * @return int
     */
    public static function loadByParent($pageId, $depth)
    {
        $db = new Data();

        $db->buildQuery('select', self::getList(['id']));
        $db->buildQuery('from', 'cms_page page');
        $db->buildQuery('limit', 1);

        for ($i = 1; $i <= $depth; $i++) {
            if ($pageId == Nav::HOME_PAGE) {
                return $pageId;
            }

            $db->clearQuery('where');
            $db->buildQuery('where', ['id = ?', $pageId]);
            $pageId = $db->count();
        }
        return $pageId;
    }

    /**
     * Find Nav
     * @param string $nav
     * @return array Model
     */
    public static function findNav($nav)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page page');
        $db->buildQuery('where', 'page.' . $nav . ' = 1');
        $db->buildQuery('where', 'page.status = 1');
        $db->buildQuery('order', 'page.display_order, page.title');
        return self::mapList($db);
    }

    /**
     * Find relativeUrl
     * @param int $pageId
     * @return array
     */
    private static function loadFriendlyUrl($pageId)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList(['friendly_url', 'friendly_url_path']));
        $db->buildQuery('from', 'cms_page page');
        $db->buildQuery('where', ['page.id = ?', $pageId]);
        return $db->selectRow();
    }

    /**
     * Find relativeUrl
     * @param int $pageId
     * @return string
     * @throws \Exception
     */
    public static function fullFriendlyUrl($pageId)
    {
        if ($pageId <= Nav::HOME_PAGE) {
            return '/';
        }

        $parentPage = self::loadFriendlyUrl($pageId);
        if (!isset($parentPage)) {
            throw new \Exception($pageId, Code::PAGE_NOT_FOUND);
        }

        return strtolower($parentPage['pageFriendlyUrlPath'] . $parentPage['pageFriendlyUrl']) . '/';
    }

    /**
     * Find main parent id
     * @param int $pageId
     * @return int
     */
    public static function findMainParentId($pageId)
    {
        $page = self::loadById($pageId);
        while ($page->getParentId() != Nav::HOME_PAGE) {
            $page = self::loadById($page->getParentId());
            if (!isset($page)) {
                return Nav::HOME_PAGE;
            }
        }
        return $page->getId();
    }

    /**
     * Get children's pageIds of page
     * @param int $pageId
     * @return array
     */
    public static function getPagesIdToDelete($pageId)
    {
        return self::getPagesIdToDeleteByPage($pageId);
    }

    private static function getPagesIdToDeleteByPage($pageId, $pageIds = [])
    {
        // Add current page
        $pageIds[] = $pageId;
        // Find children
        $pages = self::loadAllByParentId($pageId);
        if ($pages) {
            foreach ($pages as $page) {
                $pageIds = self::getPagesIdToDeleteByPage($page->getId(), $pageIds);
            }
        }
        return $pageIds;
    }

    /**
     * Update Children FriendlyUrl
     * @param int $pageId
     * @param string $friendlyUrlPath
     * @param string $friendlyUrl
     */
    public static function updateChildrenFriendlyUrl($pageId, $friendlyUrlPath, $friendlyUrl)
    {
        if ($pageId == Nav::HOME_PAGE) {
            return;
        }
        $pageChildren = self::loadAllByParentId($pageId);
        $url = $friendlyUrlPath . $friendlyUrl . '/';
        if (isset($pageChildren)) {
            foreach ($pageChildren as $pageChild) {
                $pageChild->setFriendlyUrlPath($url);
                $pageChild->save();
                self::updateChildrenFriendlyUrl($pageChild->getId(), $url, $pageChild->getFriendlyUrl());
            }
        }
    }

    /**
     * BuildTree
     * @param void
     * @return array
     */
    public static function buildTree()
    {
        $options = [];
        $options['where'][] = 'page.status IN (0, 1)';
        $options['order'][] = 'page.display_order, page.title';
        $pages = self::loadAll($options);
        return PageService::buildNavRecursion($pages, 0, 0);
    }

    /**
     * BuildHeaderNav
     * @param int $headerNavDepth
     * @param int $menuKey
     * @return array
     */
    public static function buildHeaderNav($headerNavDepth, $menuKey)
    {
        $pages = self::findNav('in_nav_header');
        return PageService::buildNavRecursion($pages, 1, $headerNavDepth, $menuKey == Menu::YES);
    }

    /**
     * BuildSideNav
     * @param int $pageId
     * @param int $maxDepth
     * @return array
     */
    public static function buildSideNav($pageId, $maxDepth)
    {
        $pages = self::findNav('in_nav_side');
        return PageService::buildNavRecursion($pages, $pageId, $maxDepth);
    }

    /**
     * BuildFooterNav
     * @param int $maxDepth
     * @return array
     */
    public static function buildFooterNav($maxDepth)
    {
        $pages = self::findNav('in_nav_footer');
        return PageService::buildNavRecursion($pages, 1, $maxDepth);
    }

    /**
     * Build Sitemap Nav
     * @return array
     */
    public static function buildSitemapNav()
    {
        $pages = self::findNav('in_sitemap');
        return PageService::buildNavRecursion($pages, 1, 0);
    }
}
