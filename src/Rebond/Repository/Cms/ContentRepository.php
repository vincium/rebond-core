<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Enums\Cms\Version;
use Rebond\Enums\Core\Code;
use Rebond\Models\Cms\Content;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Data;
use Rebond\Models\DateTime;
use Rebond\Services\Error;

class ContentRepository extends BaseContentRepository
{
    /**
     * Join an app model with the content model
     * @param Data $db
     * @param string $moduleName
     * @param bool $select = true
     * @throws \Exception
     */
    protected static function autoJoin(Data $db, $moduleName, $select = true)
    {
        $module = ModuleRepository::loadByName($moduleName);
        if (!isset($module)) {
            throw new \Exception($module, Code::MODULE_NOT_FOUND);
        }

        if ($select) {
            $db->buildQuery('select', ContentRepository::getList());
            $db->buildQuery('select', ModuleRepository::getList([], 'content_module'));
            if ($module->getHasFilter()) {
                $db->buildQuery('select', FilterRepository::getList([], 'content_filter'));
            }
        }
        $db->buildQuery('join', 'cms_content content ON x.app_id = content.app_id');
        $db->buildQuery('join', 'cms_module content_module ON content_module.id = content.module_id');
        if ($module->getHasFilter()) {
            $db->buildQuery('leftJoin', 'cms_filter content_filter ON content_filter.id = content.filter_id');
        }
        $db->buildQuery('where', ['content.module_id = ?', $module->getId()]);
    }

    /**
     * Find new contentGroup
     * @param int $moduleId
     * @return int $count
     */
    public static function getNextContentGroup($moduleId)
    {
        $query = 'SELECT MAX(content_group) FROM cms_content WHERE module_id = ?';
        $db = new Data();
        return $db->count($query, [$moduleId]) + 1;
    }

    /**
     * Update the version of Content, and set all other content to old
     * @param Content $model
     * @param int $version
     * @return int $result
     */
    public static function updateVersion(Content $model, $version)
    {
        self::updateContentGroupVersionOther($model, $version);
        $publishedDate = '';
        $params = [];
        $params[] = $version;
        $params[] = (new DateTime())->format('datetime');

        if ($version ==  Version::PUBLISHED) {
            $publishedDate = ', published_date = ?';
            $params[] =  (new DateTime())->format('datetime');
        }
        $query = 'UPDATE cms_content SET version = ?, modified_date = ?' . $publishedDate . ' WHERE id = ?';
        $params[] = $model->getId();
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * Update the version of a content
     * @param Content $model
     * @param int $versionFrom
     * @param int $versionTo
     * @return int $result
     */
    public static function updateContentGroupVersion(Content $model, $versionFrom, $versionTo)
    {
        $query = 'UPDATE cms_content SET version = ? WHERE content_group = ? AND version = ? AND module_id = ?';
        $db = new Data();
        return $db->execute($query, [$versionTo, $model->getContentGroup(), $versionFrom, $model->getModuleId()]);
    }

    /**
     * Update the version of all content
     * @param Content $model
     * @return int $result
     */
    public static function updateContentGroupVersionAll(Content $model)
    {
        $query = 'UPDATE cms_content SET version = ? WHERE content_group = ? AND module_id = ?';
        $db = new Data();
        return $db->execute($query, [ Version::OLD, $model->getContentGroup(), $model->getModuleId()]);
    }

    /**
     * Update the version of older contents
     * @param Content $model
     * @param int $version
     * @return int $result
     */
    public static function updateContentGroupVersionOther(Content $model, $version)
    {
        // undelete
        if ($version == Version::PUBLISHING) {
            return 0;
        }

        // publish from new content
        if ($version == Version::PUBLISHED && $model->getVersion() == Version::PENDING) {
            return 0;
        }

        // delete pending or published content
        if ($version == Version::DELETED && in_array($model->getVersion(), [Version::PENDING,  Version::PUBLISHED])) {
            return 0;
        }

        $query = 'UPDATE cms_content
            SET version = ?
            WHERE app_id != ? AND version = ? AND content_group = ? AND module_id = ?';

        $params = [];
        // publish from modified content
        if ($version ==  Version::PUBLISHED && $model->getVersion() == Version::PUBLISHING) {
            $params = [
                 Version::OLD,
                $model->getAppId(),
                 Version::UPDATING,
                $model->getContentGroup(),
                $model->getModuleId()
            ];

            // delete from publishing content
        } else if ($version == Version::DELETED && $model->getVersion() == Version::PUBLISHING) {
            $params = [
                 Version::PUBLISHED,
                $model->getAppId(),
                 Version::UPDATING,
                $model->getContentGroup(),
                $model->getModuleId()
            ];

            // delete from updating content
        } else if ($version == Version::DELETED && $model->getVersion() == Version::UPDATING) {
            $params = [
                 Version::PENDING,
                $model->getAppId(),
                 Version::PUBLISHING,
                $model->getContentGroup(),
                $model->getModuleId()
            ];
        }
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * Clear the filter of contents
     * @param int $filterId
     * @return int $result
     */
    public static function clearFilter($filterId)
    {
        $query = 'UPDATE cms_content SET filter_id = 0 WHERE filter_id = ?';
        $db = new Data();
        return $db->execute($query, [$filterId]);
    }

    protected static function condition(Data $db, $option)
    {
        switch ($option) {
            case 'expiration':
                $now = (new DateTime())->format('datetime');
                $db->buildQuery('where', [
                    'content.use_expiration = 0 || (content.go_live_date <= ? AND content.expiry_date >= ?)',
                    $now, $now
                ]);
                break;
            case 'published':
                $db->buildQuery('where', ['content.version IN (?)', [Version::PUBLISHED, Version::UPDATING]]);
                break;
            case 'pending':
                $db->buildQuery('where', ['content.version IN (?)', [Version::PENDING, Version::PUBLISHING]]);
                break;
            case 'preview':
                $db->buildQuery('where', ['content.version IN (?)', [Version::PENDING, Version::PUBLISHED, Version::PUBLISHING]]);
                break;
            case 'deleted':
                $db->buildQuery('where', ['content.version = ?', Version::DELETED]);
                break;
        }
    }

    protected static function mapEntity(Content $model, $row, $alias = 'content')
    {
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'ModuleId'])) {
            $model->setModuleId($row[$alias . 'ModuleId']);
            $model->setModule(ModuleRepository::join($row, $alias . '_module'));
        }
        if (isset($row[$alias . 'ContentGroup'])) {
            $model->setContentGroup($row[$alias . 'ContentGroup']);
        }
        if (isset($row[$alias . 'FilterId'])) {
            $model->setFilterId($row[$alias . 'FilterId']);
            $model->setFilter(FilterRepository::join($row, $alias . '_filter'));
        }
        if (isset($row[$alias . 'AuthorId'])) {
            $model->setAuthorId($row[$alias . 'AuthorId']);
            $model->setAuthor(UserRepository::join($row, $alias . '_author'));
        }
        if (isset($row[$alias . 'PublisherId'])) {
            $model->setPublisherId($row[$alias . 'PublisherId']);
            $model->setPublisher(UserRepository::join($row, $alias . '_publisher'));
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'UrlFriendlyTitle'])) {
            $model->setUrlFriendlyTitle($row[$alias . 'UrlFriendlyTitle']);
        }
        if (isset($row[$alias . 'UseExpiration'])) {
            $model->setUseExpiration($row[$alias . 'UseExpiration']);
        }
        if (isset($row[$alias . 'GoLiveDate'])) {
            $model->setGoLiveDate($row[$alias . 'GoLiveDate']);
        }
        if (isset($row[$alias . 'ExpiryDate'])) {
            $model->setExpiryDate($row[$alias . 'ExpiryDate']);
        }
        if (isset($row[$alias . 'PublishedDate'])) {
            $model->setPublishedDate($row[$alias . 'PublishedDate']);
        }
        if (isset($row[$alias . 'Version'])) {
            $model->setVersion($row[$alias . 'Version']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
