<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Component;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseComponentRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = component
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'component')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.module_id AS ' . $alias . 'ModuleId, ' .
                $alias . '.summary AS ' . $alias . 'Summary, ' .
                $alias . '.method AS ' . $alias . 'Method, ' .
                $alias . '.can_be_cached AS ' . $alias . 'CanBeCached, ' .
                $alias . '.type AS ' . $alias . 'Type, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(component.id)');
        $db->buildQuery('from', 'cms_component `component`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Component
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Component() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_component `component`');
        $db->buildQuery('where', ['component.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Component();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Component
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_component `component`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Component[]
     */
    public static function loadAllByModuleId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_component `component`');
        $db->buildQuery('where', ['component.module_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Component[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_component `component`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Component $model
     * @return int
     */
    public static function save(Component $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_component (`title`, module_id, `summary`, `method`, `can_be_cached`, `type`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getModuleId(),
                $model->getSummary(),
                $model->getMethod(),
                (int)$model->getCanBeCached(),
                $model->getType(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_component SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getModuleId() !== null) {
                $query .= 'module_id = ?, ';
                $params[] = $model->getModuleId();
            }
            if ($model->getSummary() !== null) {
                $query .= '`summary` = ?, ';
                $params[] = $model->getSummary();
            }
            if ($model->getMethod() !== null) {
                $query .= '`method` = ?, ';
                $params[] = $model->getMethod();
            }
            if ($model->getCanBeCached() !== null) {
                $query .= '`can_be_cached` = ?, ';
                $params[] = $model->getCanBeCached();
            }
            if ($model->getType() !== null) {
                $query .= '`type` = ?, ';
                $params[] = $model->getType();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE cms_component SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_component WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByModuleId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_component WHERE module_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = component
     * @return Component
     */
    public static function join(array $row, $alias = 'component')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Component
     */
    protected static function mapper(array $row, $alias = 'component')
    {
        $model = new Component(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'ModuleId'])) {
            $model->setModuleId($row[$alias . 'ModuleId']);
            $model->setModule(\Rebond\Repository\Cms\ModuleRepository::join($row, $alias . '_module'));
        }
        if (isset($row[$alias . 'Summary'])) {
            $model->setSummary($row[$alias . 'Summary']);
        }
        if (isset($row[$alias . 'Method'])) {
            $model->setMethod($row[$alias . 'Method']);
        }
        if (isset($row[$alias . 'CanBeCached'])) {
            $model->setCanBeCached($row[$alias . 'CanBeCached']);
        }
        if (isset($row[$alias . 'Type'])) {
            $model->setType($row[$alias . 'Type']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
