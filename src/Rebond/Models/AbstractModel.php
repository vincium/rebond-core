<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

use Rebond\App;

abstract class AbstractModel
{
    protected abstract function setDefaultBase();
    abstract function toArray();
    abstract function save();
    abstract function delete();

    public function ns($ns)
    {
        $app = App::instance();
        if ($app->isTest()) {
            return str_replace('\Rebond', '\Test', $ns);
        }
        return $ns;
    }
}
