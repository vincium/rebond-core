<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseGadget extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var \Rebond\Models\Cms\Page */
    protected $page;
    /* @var int */
    protected $pageId;
    /* @var \Rebond\Models\Cms\Component */
    protected $component;
    /* @var int */
    protected $componentId;
    /* @var int */
    protected $col;
    /* @var \Rebond\Models\Cms\Filter */
    protected $filter;
    /* @var int */
    protected $filterId;
    /* @var string */
    protected $customFilter;
    /* @var int */
    protected $displayOrder;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;
    /* @var string */
    protected $filterInfo;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->page = null;
        $this->pageId = 0;
        $this->component = null;
        $this->componentId = 0;
        $this->col = 0;
        $this->filter = null;
        $this->filterId = 0;
        $this->customFilter = '';
        $this->displayOrder = 0;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
        $this->filterInfo = '';
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return int
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Page
     */
    public function getPage($createIfNotExist = false)
    {
        if (!isset($this->page)) {
            $ns = $this->ns('\Rebond\Repository\Cms\PageRepository');
            $this->page = $ns::loadById($this->pageId, $createIfNotExist);
        }
        return $this->page;
    }

    /*
     * @param int $id
     */
    public function setPageId($id)
    {
        if ($this->pageId !== $id) {
            $this->pageId = (int) $id;
            $this->page = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Page $model = null
     */
    public function setPage(\Rebond\Models\Cms\Page $model = null)
    {
        if (!isset($model)) {
            $this->page = null;
            return;
        }
        $this->pageId = (int) $model->getId();
        $this->page = $model;
    }

    /*
     * @return int
     */
    public function getComponentId()
    {
        return $this->componentId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Component
     */
    public function getComponent($createIfNotExist = false)
    {
        if (!isset($this->component)) {
            $ns = $this->ns('\Rebond\Repository\Cms\ComponentRepository');
            $this->component = $ns::loadById($this->componentId, $createIfNotExist);
        }
        return $this->component;
    }

    /*
     * @param int $id
     */
    public function setComponentId($id)
    {
        if ($this->componentId !== $id) {
            $this->componentId = (int) $id;
            $this->component = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Component $model = null
     */
    public function setComponent(\Rebond\Models\Cms\Component $model = null)
    {
        if (!isset($model)) {
            $this->component = null;
            return;
        }
        $this->componentId = (int) $model->getId();
        $this->component = $model;
    }

    /*
     * @return int
     */
    public function getCol()
    {
        return $this->col;
    }

    /*
     * @param int $value
     */
    public function setCol($value)
    {
        $this->col = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addCol($value)
    {
        $this->col += (int) $value;
    }

    /*
     * @return int
     */
    public function getFilterId()
    {
        return $this->filterId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Filter
     */
    public function getFilter($createIfNotExist = false)
    {
        if (!isset($this->filter)) {
            $ns = $this->ns('\Rebond\Repository\Cms\FilterRepository');
            $this->filter = $ns::loadById($this->filterId, $createIfNotExist);
        }
        return $this->filter;
    }

    /*
     * @param int $id
     */
    public function setFilterId($id)
    {
        if ($this->filterId !== $id) {
            $this->filterId = (int) $id;
            $this->filter = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Filter $model = null
     */
    public function setFilter(\Rebond\Models\Cms\Filter $model = null)
    {
        if (!isset($model)) {
            $this->filter = null;
            return;
        }
        $this->filterId = (int) $model->getId();
        $this->filter = $model;
    }

    /*
     * @return string
     */
    public function getCustomFilter()
    {
        return $this->customFilter;
    }

    /*
     * @param string $value
     */
    public function setCustomFilter($value)
    {
        $this->customFilter = $value;
    }

    /*
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /*
     * @param int $value
     */
    public function setDisplayOrder($value)
    {
        $this->displayOrder = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addDisplayOrder($value)
    {
        $this->displayOrder += (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return string
     */
    public function getFilterInfo()
    {
        return $this->filterInfo;
    }

    /*
     * @param string $value
     */
    public function setFilterInfo($value)
    {
        $this->filterInfo = $value;
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'pageId' => $this->getPageId(),
            'componentId' => $this->getComponentId(),
            'col' => $this->getCol(),
            'filterId' => $this->getFilterId(),
            'customFilter' => $this->getCustomFilter(),
            'displayOrder' => $this->getDisplayOrder(),
            'status' => $this->getStatus(),
            'filterInfo' => $this->getFilterInfo(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Save a Gadget
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\GadgetRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Gadget
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\GadgetRepository');
        return $ns::deleteById($this->id);
    }
}
