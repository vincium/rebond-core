<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseTemplate extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var string */
    protected $summary;
    /* @var string */
    protected $filename;
    /* @var int */
    protected $menu;
    /* @var int */
    protected $menuLevel;
    /* @var bool */
    protected $inBreadcrumb;
    /* @var int */
    protected $sideNav;
    /* @var int */
    protected $sideNavLevel;
    /* @var bool */
    protected $inFooter;
    /* @var int */
    protected $footerLevel;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->summary = '';
        $this->filename = '';
        $this->menu = 0;
        $this->menuLevel = 1;
        $this->inBreadcrumb = true;
        $this->sideNav = 0;
        $this->sideNavLevel = 0;
        $this->inFooter = true;
        $this->footerLevel = 0;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /*
     * @param string $value
     */
    public function setSummary($value)
    {
        $this->summary = $value;
    }

    /*
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /*
     * @param string $value
     */
    public function setFilename($value)
    {
        $this->filename = $value;
    }

    /*
     * @return int
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /*
     * @return int
     */
    public function getMenuValue()
    {
        return \Rebond\Enums\Cms\Menu::lang($this->menu);
    }

    /*
     * @return array
     */
    public function getMenuList()
    {
        return \Rebond\Enums\Cms\Menu::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setMenu($value)
    {
        $this->menu = (int) $value;
    }

    /*
     * @return int
     */
    public function getMenuLevel()
    {
        return $this->menuLevel;
    }

    /*
     * @param int $value
     */
    public function setMenuLevel($value)
    {
        $this->menuLevel = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addMenuLevel($value)
    {
        $this->menuLevel += (int) $value;
    }

    /*
     * @return bool
     */
    public function getInBreadcrumb()
    {
        return $this->inBreadcrumb;
    }

    /*
     * @param int $value
     */
    public function setInBreadcrumb($value)
    {
        $this->inBreadcrumb = (int) $value;
    }

    /*
     * @return int
     */
    public function getSideNav()
    {
        return $this->sideNav;
    }

    /*
     * @return int
     */
    public function getSideNavValue()
    {
        return \Rebond\Enums\Cms\SideNav::lang($this->sideNav);
    }

    /*
     * @return array
     */
    public function getSideNavList()
    {
        return \Rebond\Enums\Cms\SideNav::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setSideNav($value)
    {
        $this->sideNav = (int) $value;
    }

    /*
     * @return int
     */
    public function getSideNavLevel()
    {
        return $this->sideNavLevel;
    }

    /*
     * @param int $value
     */
    public function setSideNavLevel($value)
    {
        $this->sideNavLevel = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addSideNavLevel($value)
    {
        $this->sideNavLevel += (int) $value;
    }

    /*
     * @return bool
     */
    public function getInFooter()
    {
        return $this->inFooter;
    }

    /*
     * @param int $value
     */
    public function setInFooter($value)
    {
        $this->inFooter = (int) $value;
    }

    /*
     * @return int
     */
    public function getFooterLevel()
    {
        return $this->footerLevel;
    }

    /*
     * @param int $value
     */
    public function setFooterLevel($value)
    {
        $this->footerLevel = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addFooterLevel($value)
    {
        $this->footerLevel += (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'summary' => $this->getSummary(),
            'filename' => $this->getFilename(),
            'menu' => $this->getMenu(),
            'menuLevel' => $this->getMenuLevel(),
            'inBreadcrumb' => $this->getInBreadcrumb(),
            'sideNav' => $this->getSideNav(),
            'sideNavLevel' => $this->getSideNavLevel(),
            'inFooter' => $this->getInFooter(),
            'footerLevel' => $this->getFooterLevel(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Template
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\TemplateRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Template
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\TemplateRepository');
        return $ns::deleteById($this->id);
    }
}
