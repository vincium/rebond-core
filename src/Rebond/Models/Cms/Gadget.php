<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models\Cms;

use Rebond\Enums\Cms\Component;
use Rebond\Models\FilterInfo;
use Rebond\Repository\Cms\FilterRepository;

class Gadget extends BaseGadget
{
    public function __construct($setDefault = true)
    {
        parent::__construct();
        if ($setDefault) {
            $this->setDefault();
        }
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        // set your defaults values here
    }

    public function prepareFilter()
    {
        $filterInfo = new FilterInfo();

        // FilterId links to the filter table
        if ($this->getComponent()->getType() == Component::FILTERED_CARDS) {
            $filter = FilterRepository::loadById($this->getFilterId());
            if (isset($filter)) {
                $filterInfo->setId($filter->getId());
                $filterInfo->setTitle($filter->getTitle());
            }

        // FilterId links to a single content item
        } else if ($this->getComponent()->getType() == Component::SINGLE_ELEMENT) {
            $appData = '\Own\Repository\App\\' . $this->getComponent()->getModule()->getName() . 'Repository';
            $item = $appData::loadCurrent($this->getFilterId(), false);
            if (isset($item)) {
                $filterInfo->setId($item->getContentGroup());
                $filterInfo->setTitle($item->getTitle());
                $filterInfo->setAppId($item->getAppId());
            }
        // Custom filter, use getFilter() to get custom value in getCustomFilter()
        } else if ($this->getComponent()->getType() == Component::CUSTOM_LISTING) {
            $filterInfo->setTitle($this->getCustomFilter());
        }
        $this->setFilterInfo($filterInfo);
    }
}
