<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models\Cms;

use Rebond\App;
use Rebond\Enums\Cms\Version;
use Rebond\Repository\Cms\ContentRepository;
use Rebond\Services\Format;

class Content extends BaseContent
{
    public function __construct()
    {
        parent::__construct();
        $this->setDefault();
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        $this->setUrlFriendlyTitle(Format::friendlyTitle($this->title));
    }

    public function setTitle($value)
    {
        $this->title = $value;
        $this->setUrlFriendlyTitle(Format::friendlyTitle($this->title));
    }

    /**
     * Save a Content
     * @return int
     */
    protected function saveEntity()
    {
        $app = App::instance();
        $userId = $app->getUserId();

        $this->setId(0);
        $this->setAuthorId($userId);
        $workflow = $this->getModule()->getWorkflow();

        // new content
        if ($this->getContentGroup() == 0) {
            $contentGroup = ContentRepository::getNextContentGroup($this->getModuleId());
            $this->setContentGroup($contentGroup);
            if ($workflow == 1) {
                $this->setVersion(Version::PENDING);
            } else {
                $this->setPublisherId($userId);
                $this->setPublishedDate('now');
                $this->setVersion(Version::PUBLISHED);
            }
            // updating content
        } else {
            // use workflow
            if ($workflow == 1) {
                if ($this->getVersion() == Version::PUBLISHED) {
                    ContentRepository::updateContentGroupVersion($this, Version::PUBLISHED, Version::UPDATING);
                    $this->setVersion(Version::PUBLISHING);
                } else if ($this->getVersion() == Version::PENDING) {
                    ContentRepository::updateContentGroupVersion($this, Version::PENDING, Version::OLD);
                    $this->setVersion(Version::PENDING);
                } else if (in_array($this->getVersion(), [Version::PUBLISHING, Version::UPDATING])) {
                    ContentRepository::updateContentGroupVersion($this, Version::PUBLISHING, Version::OLD);
                    $this->setVersion(Version::PUBLISHING);
                }
                // auto published
            } else if ($workflow == 0) {
                ContentRepository::updateContentGroupVersionAll($this);
                $this->setPublisherId($userId);
                $this->setPublishedDate('now');
                $this->setVersion(Version::PUBLISHED);
            }
        }
        return parent::save();
    }
}
