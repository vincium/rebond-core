<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

use Rebond\Services\Lang;

class FilterInfo
{
    private $id;
    private $appId;
    private $title;

    public function __construct()
    {
        $this->id = 0;
        $this->appId = 0;
        $this->title = Lang::lang('element_none');
    }

    /** 
     * Set Id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /** 
     * Get Id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /** 
     * Set Title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /** 
     * Get Title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /** 
     * Set AppId
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /** 
     * Get AppId
     * @return string
     */
    public function getAppId()
    {
        return $this->appId;
    }

    public function __toString()
    {
        return $this->title;
    }
}
