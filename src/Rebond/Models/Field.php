<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

use Rebond\Enums\Core\Result;

class Field
{
    /* @var string */
    private $fieldName;
    /* @var int */
    private $result;
    /* @var string */
    private $message;

    /**
     * @param string $fieldName = ''
     * @param int $result
     * @param string $message = ''
     */
    public function __construct($fieldName = '', $result = Result::SUCCESS, $message = '')
    {
        $this->fieldName = $fieldName;
        $this->result = $result;
        $this->message = $message;
    }

    /** @param string $fieldName */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /** 
     * Get fieldName
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /** 
     * Set Result
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = (int) $result;
    }

    /** 
     * Get Result
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Is Valid
     * @return bool
     */
    public function isValid()
    {
        if ($this->result == Result::SUCCESS) {
            return true;
        }
        return false;
    }

    /** 
     * Set Message
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /** 
     * Get Message
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function __toString()
    {
        return $this->fieldName;
    }
}
