<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseMedia extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var \Rebond\Models\Core\Folder */
    protected $folder;
    /* @var int */
    protected $folderId;
    /* @var string */
    protected $tags;
    /* @var string */
    protected $upload;
    /* @var string */
    protected $originalFilename;
    /* @var string */
    protected $path;
    /* @var string */
    protected $extension;
    /* @var string */
    protected $mimeType;
    /* @var int */
    protected $fileSize;
    /* @var int */
    protected $width;
    /* @var int */
    protected $height;
    /* @var string */
    protected $alt;
    /* @var bool */
    protected $isSelectable;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->folder = null;
        $this->folderId = 1;
        $this->tags = '';
        $this->upload = '';
        $this->originalFilename = '';
        $this->path = '';
        $this->extension = '';
        $this->mimeType = '';
        $this->fileSize = 0;
        $this->width = 0;
        $this->height = 0;
        $this->alt = '';
        $this->isSelectable = true;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getFolderId()
    {
        return $this->folderId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Folder
     */
    public function getFolder($createIfNotExist = false)
    {
        if (!isset($this->folder)) {
            $ns = $this->ns('\Rebond\Repository\Core\FolderRepository');
            $this->folder = $ns::loadById($this->folderId, $createIfNotExist);
        }
        return $this->folder;
    }

    /*
     * @param int $id
     */
    public function setFolderId($id)
    {
        if ($this->folderId !== $id) {
            $this->folderId = (int) $id;
            $this->folder = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Folder $model = null
     */
    public function setFolder(\Rebond\Models\Core\Folder $model = null)
    {
        if (!isset($model)) {
            $this->folder = null;
            return;
        }
        $this->folderId = (int) $model->getId();
        $this->folder = $model;
    }

    /*
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /*
     * @param string $value
     */
    public function setTags($value)
    {
        $this->tags = $value;
    }

    /*
     * @return string
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /*
     * @param string $value
     */
    public function setUpload($value)
    {
        $this->upload = $value;
    }

    /*
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /*
     * @param string $value
     */
    public function setOriginalFilename($value)
    {
        $this->originalFilename = $value;
    }

    /*
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /*
     * @param string $value
     */
    public function setPath($value)
    {
        $this->path = $value;
    }

    /*
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /*
     * @param string $value
     */
    public function setExtension($value)
    {
        $this->extension = $value;
    }

    /*
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /*
     * @param string $value
     */
    public function setMimeType($value)
    {
        $this->mimeType = $value;
    }

    /*
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /*
     * @param int $value
     */
    public function setFileSize($value)
    {
        $this->fileSize = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addFileSize($value)
    {
        $this->fileSize += (int) $value;
    }

    /*
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /*
     * @param int $value
     */
    public function setWidth($value)
    {
        $this->width = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addWidth($value)
    {
        $this->width += (int) $value;
    }

    /*
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /*
     * @param int $value
     */
    public function setHeight($value)
    {
        $this->height = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addHeight($value)
    {
        $this->height += (int) $value;
    }

    /*
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /*
     * @param string $value
     */
    public function setAlt($value)
    {
        $this->alt = $value;
    }

    /*
     * @return bool
     */
    public function getIsSelectable()
    {
        return $this->isSelectable;
    }

    /*
     * @param int $value
     */
    public function setIsSelectable($value)
    {
        $this->isSelectable = (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'folderId' => $this->getFolderId(),
            'tags' => $this->getTags(),
            'upload' => $this->getUpload(),
            'originalFilename' => $this->getOriginalFilename(),
            'path' => $this->getPath(),
            'extension' => $this->getExtension(),
            'mimeType' => $this->getMimeType(),
            'fileSize' => $this->getFileSize(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'alt' => $this->getAlt(),
            'isSelectable' => $this->getIsSelectable(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Media
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\MediaRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Media
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\MediaRepository');
        return $ns::deleteById($this->id);
    }
}
