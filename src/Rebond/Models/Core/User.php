<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models\Core;

use Rebond\Repository\Core\UserRepository;

class User extends BaseUser
{
    private $access;

    public function __construct($setDefault = true)
    {
        parent::__construct();
        if ($setDefault) {
            $this->setDefault();
        }
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        $this->access = null;
    }

    public function getFullName()
    {
        $fullName = trim($this->firstName . ' ' . $this->lastName);
        return $fullName != '' ? $fullName : $this->email;
    }

    /**
     * @param string $permission
     * @param bool $isAdmin
     * @return bool
     */
    public function hasAccess($permission, $isAdmin)
    {
        if ($isAdmin && !$this->getIsAdmin()) {
            return false;
        }

        if (!isset($this->access)) {
            $this->access = UserRepository::getPermissions($this->getId());
        }

        if (empty($permission)) {
            return true;
        }

        foreach ($this->access as $access) {
            if (stripos($access['title'], $permission) === 0) {
                return true;
            }
        }

        return false;
    }
}
