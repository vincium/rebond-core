<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseSite extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var string */
    protected $googleAnalytics;
    /* @var string */
    protected $keywords;
    /* @var string */
    protected $description;
    /* @var string */
    protected $css;
    /* @var string */
    protected $js;
    /* @var string */
    protected $signInUrl;
    /* @var bool */
    protected $isDebug;
    /* @var bool */
    protected $logSql;
    /* @var string */
    protected $timezone;
    /* @var bool */
    protected $isCms;
    /* @var int */
    protected $cacheTime;
    /* @var bool */
    protected $useDeviceTemplate;
    /* @var string */
    protected $skin;
    /* @var bool */
    protected $sendMailOnError;
    /* @var string */
    protected $mailListOnError;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->googleAnalytics = '';
        $this->keywords = '';
        $this->description = '';
        $this->css = '';
        $this->js = '';
        $this->signInUrl = '/login';
        $this->isDebug = false;
        $this->logSql = false;
        $this->timezone = 'Europe/Oslo';
        $this->isCms = true;
        $this->cacheTime = 0;
        $this->useDeviceTemplate = true;
        $this->skin = '';
        $this->sendMailOnError = true;
        $this->mailListOnError = '';
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return string
     */
    public function getGoogleAnalytics()
    {
        return $this->googleAnalytics;
    }

    /*
     * @param string $value
     */
    public function setGoogleAnalytics($value)
    {
        $this->googleAnalytics = $value;
    }

    /*
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /*
     * @param string $value
     */
    public function setKeywords($value)
    {
        $this->keywords = $value;
    }

    /*
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /*
     * @param string $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /*
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /*
     * @param string $value
     */
    public function setCss($value)
    {
        $this->css = $value;
    }

    /*
     * @return string
     */
    public function getJs()
    {
        return $this->js;
    }

    /*
     * @param string $value
     */
    public function setJs($value)
    {
        $this->js = $value;
    }

    /*
     * @return string
     */
    public function getSignInUrl()
    {
        return $this->signInUrl;
    }

    /*
     * @param string $value
     */
    public function setSignInUrl($value)
    {
        $this->signInUrl = $value;
    }

    /*
     * @return bool
     */
    public function getIsDebug()
    {
        return $this->isDebug;
    }

    /*
     * @param int $value
     */
    public function setIsDebug($value)
    {
        $this->isDebug = (int) $value;
    }

    /*
     * @return bool
     */
    public function getLogSql()
    {
        return $this->logSql;
    }

    /*
     * @param int $value
     */
    public function setLogSql($value)
    {
        $this->logSql = (int) $value;
    }

    /*
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /*
     * @param string $value
     */
    public function setTimezone($value)
    {
        $this->timezone = $value;
    }

    /*
     * @return bool
     */
    public function getIsCms()
    {
        return $this->isCms;
    }

    /*
     * @param int $value
     */
    public function setIsCms($value)
    {
        $this->isCms = (int) $value;
    }

    /*
     * @return int
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /*
     * @param int $value
     */
    public function setCacheTime($value)
    {
        $this->cacheTime = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addCacheTime($value)
    {
        $this->cacheTime += (int) $value;
    }

    /*
     * @return bool
     */
    public function getUseDeviceTemplate()
    {
        return $this->useDeviceTemplate;
    }

    /*
     * @param int $value
     */
    public function setUseDeviceTemplate($value)
    {
        $this->useDeviceTemplate = (int) $value;
    }

    /*
     * @return string
     */
    public function getSkin()
    {
        return $this->skin;
    }

    /*
     * @param string $value
     */
    public function setSkin($value)
    {
        $this->skin = $value;
    }

    /*
     * @return bool
     */
    public function getSendMailOnError()
    {
        return $this->sendMailOnError;
    }

    /*
     * @param int $value
     */
    public function setSendMailOnError($value)
    {
        $this->sendMailOnError = (int) $value;
    }

    /*
     * @return string
     */
    public function getMailListOnError()
    {
        return $this->mailListOnError;
    }

    /*
     * @param string $value
     */
    public function setMailListOnError($value)
    {
        $this->mailListOnError = $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'googleAnalytics' => $this->getGoogleAnalytics(),
            'keywords' => $this->getKeywords(),
            'description' => $this->getDescription(),
            'css' => $this->getCss(),
            'js' => $this->getJs(),
            'signInUrl' => $this->getSignInUrl(),
            'isDebug' => $this->getIsDebug(),
            'logSql' => $this->getLogSql(),
            'timezone' => $this->getTimezone(),
            'isCms' => $this->getIsCms(),
            'cacheTime' => $this->getCacheTime(),
            'useDeviceTemplate' => $this->getUseDeviceTemplate(),
            'skin' => $this->getSkin(),
            'sendMailOnError' => $this->getSendMailOnError(),
            'mailListOnError' => $this->getMailListOnError(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Site
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\SiteRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Site
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\SiteRepository');
        return $ns::deleteById($this->id);
    }
}
