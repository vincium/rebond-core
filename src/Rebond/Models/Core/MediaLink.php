<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models\Core;

class MediaLink extends BaseMediaLink
{
    public function __construct($setDefault = true)
    {
        parent::__construct();
        if ($setDefault) {
            $this->setDefault();
        }
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        // set your defaults values here
    }
}
