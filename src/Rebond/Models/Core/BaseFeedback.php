<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseFeedback extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var \Rebond\Models\Core\User */
    protected $user;
    /* @var int */
    protected $userId;
    /* @var string */
    protected $title;
    /* @var int */
    protected $type;
    /* @var array */
    protected $typeList;
    /* @var string */
    protected $description;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
        $this->typeList = [0 => 'General', 1 => 'Bug', 2 => 'Suggestion'];
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->user = null;
        $this->userId = 0;
        $this->title = '';
        $this->type = 0;
        $this->description = '';
        $this->status = 0;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getUser($createIfNotExist = false)
    {
        if (!isset($this->user)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->user = $ns::loadById($this->userId, $createIfNotExist);
        }
        return $this->user;
    }

    /*
     * @param int $id
     */
    public function setUserId($id)
    {
        if ($this->userId !== $id) {
            $this->userId = (int) $id;
            $this->user = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setUser(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->user = null;
            return;
        }
        $this->userId = (int) $model->getId();
        $this->user = $model;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /*
     * @return int
     */
    public function getTypeValue()
    {
        if (isset($this->typeList[$this->type])) {
            return $this->typeList[$this->type];
        }

        return Lang::lang('undefined');
    }

    /*
     * @return array
     */
    public function getTypeList()
    {
        return $this->typeList;
    }

    /*
     * @param int $value
     */
    public function setType($value)
    {
        $this->type = (int) $value;
    }

    /*
     * @param int $length = 0
     * @return string
     */
    public function getDescription($length = 0)
    {
        return Format::toText($this->description, $length);
    }

    /*
     * @param string $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'userId' => $this->getUserId(),
            'title' => $this->getTitle(),
            'type' => $this->getType(),
            'description' => $this->getDescription(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Feedback
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\FeedbackRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Feedback
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\FeedbackRepository');
        return $ns::deleteById($this->id);
    }
}
