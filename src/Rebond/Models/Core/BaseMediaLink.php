<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseMediaLink extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var int */
    protected $package;
    /* @var string */
    protected $entity;
    /* @var string */
    protected $field;
    /* @var string */
    protected $idField;
    /* @var string */
    protected $titleField;
    /* @var string */
    protected $url;
    /* @var string */
    protected $filter;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->package = 0;
        $this->entity = '';
        $this->field = '';
        $this->idField = '';
        $this->titleField = '';
        $this->url = '';
        $this->filter = '';
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return int
     */
    public function getPackage()
    {
        return $this->package;
    }

    /*
     * @return int
     */
    public function getPackageValue()
    {
        return \Rebond\Enums\Core\Package::lang($this->package);
    }

    /*
     * @return array
     */
    public function getPackageList()
    {
        return \Rebond\Enums\Core\Package::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setPackage($value)
    {
        $this->package = (int) $value;
    }

    /*
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /*
     * @param string $value
     */
    public function setEntity($value)
    {
        $this->entity = $value;
    }

    /*
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /*
     * @param string $value
     */
    public function setField($value)
    {
        $this->field = $value;
    }

    /*
     * @return string
     */
    public function getIdField()
    {
        return $this->idField;
    }

    /*
     * @param string $value
     */
    public function setIdField($value)
    {
        $this->idField = $value;
    }

    /*
     * @return string
     */
    public function getTitleField()
    {
        return $this->titleField;
    }

    /*
     * @param string $value
     */
    public function setTitleField($value)
    {
        $this->titleField = $value;
    }

    /*
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /*
     * @param string $value
     */
    public function setUrl($value)
    {
        $this->url = $value;
    }

    /*
     * @return string
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /*
     * @param string $value
     */
    public function setFilter($value)
    {
        $this->filter = $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'package' => $this->getPackage(),
            'entity' => $this->getEntity(),
            'field' => $this->getField(),
            'idField' => $this->getIdField(),
            'titleField' => $this->getTitleField(),
            'url' => $this->getUrl(),
            'filter' => $this->getFilter(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getField();
    }

    /**
     * Save a MediaLink
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\MediaLinkRepository');
        return $ns::save($this);
    }

    /**
     * Delete a MediaLink
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\MediaLinkRepository');
        return $ns::deleteById($this->id);
    }
}
