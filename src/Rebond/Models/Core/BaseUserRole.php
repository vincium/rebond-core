<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseUserRole extends AbstractModel
{
    /* @var \Rebond\Models\Core\User */
    protected $user;
    /* @var int */
    protected $userId;
    /* @var \Rebond\Models\Core\Role */
    protected $role;
    /* @var int */
    protected $roleId;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->user = null;
        $this->userId = 0;
        $this->role = null;
        $this->roleId = 0;
    }

    public function getId()
    {
        return $this->userId . '_' . $this->roleId;
    }
    /*
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getUser($createIfNotExist = false)
    {
        if (!isset($this->user)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->user = $ns::loadById($this->userId, $createIfNotExist);
        }
        return $this->user;
    }

    /*
     * @return array
     */
    public function getUserList()
    {
        if (!isset($this->userList)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRoleRepository');
            $this->userList = $ns::loadAllByRoleId($this->roleId);
        }
        return $this->userList;
    }

    /*
     * @param int $id
     */
    public function setUserId($id)
    {
        if ($this->userId !== $id) {
            $this->userId = (int) $id;
            $this->user = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setUser(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->user = null;
            return;
        }
        $this->userId = (int) $model->getId();
        $this->user = $model;
    }

    /*
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Role
     */
    public function getRole($createIfNotExist = false)
    {
        if (!isset($this->role)) {
            $ns = $this->ns('\Rebond\Repository\Core\RoleRepository');
            $this->role = $ns::loadById($this->roleId, $createIfNotExist);
        }
        return $this->role;
    }

    /*
     * @return array
     */
    public function getRoleList()
    {
        if (!isset($this->roleList)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRoleRepository');
            $this->roleList = $ns::loadAllByUserId($this->userId);
        }
        return $this->roleList;
    }

    /*
     * @param int $id
     */
    public function setRoleId($id)
    {
        if ($this->roleId !== $id) {
            $this->roleId = (int) $id;
            $this->role = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Role $model = null
     */
    public function setRole(\Rebond\Models\Core\Role $model = null)
    {
        if (!isset($model)) {
            $this->role = null;
            return;
        }
        $this->roleId = (int) $model->getId();
        $this->role = $model;
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'userId' => $this->getUserId(),
            'roleId' => $this->getRoleId(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUser();
    }

    /**
     * Save a UserRole
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserRoleRepository');
        return $ns::save($this);
    }

    /**
     * Delete a UserRole
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserRoleRepository');
        return $ns::deleteByIds($this->userId, $this->roleId);
    }
}
