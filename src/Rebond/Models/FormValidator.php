<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

use Rebond\Enums\Core\Result;
use Rebond\Services\Lang;

class FormValidator
{
    private $result;
    private $message;
    /* @var Field[] */
    private $fields;
    private $isClean;

    /** 
     * Constructs Rebond\Core\Form
     */
    public function __construct()
    {
        $this->clear();
    }

    /** 
     * Clear item
     */
    public function clear()
    {
        $this->result = Result::SUCCESS;
        $this->message = '';
        $this->fields = [];
        $this->isClean = false;
    }

    /**
     * Is Valid
     * @return bool
     */
    public function isClean()
    {
        return $this->isClean;
    }

    /**
     * Is Valid
     * @return bool
     */
    public function isValid()
    {
        if ($this->isClean) {
            return $this->result == Result::SUCCESS;
        }

        $this->result = Result::SUCCESS;
        $this->message = '';
        foreach ($this->fields as $field) {
            if ($field->getResult() == Result::ERROR) {
                $this->result = Result::ERROR;
                $this->message = Lang::lang('error_form_invalid');
                break;
            }
        }
        $this->isClean = true;
        return ($this->result == Result::SUCCESS) ? true : false;
    }

    /** 
     * Get Message
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getMessages($firstMessage = false)
    {
        $messages = [];
        foreach ($this->fields as $field) {
            if ($field->getResult() == Result::ERROR) {
                if ($firstMessage) {
                    return $field->getMessage();
                }
                $messages[] = $field->getMessage();
            }
        }
        if ($firstMessage) {
            return null;
        }
        return $messages;
    }

    /** 
     * Set Fields
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->isClean = false;
        $this->fields = $fields;
    }

    /** 
     * Get Fields
     */
    public function getFields()
    {
        return $this->fields;
    }

   /** 
     * Get Field
     * @param string $field
     * @return Field|null
     */
    public function getField($field)
    {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }
        return null;
    }

       /** 
     * Set Fields
     * @param array $fields
     */
    public function addFields(array $fields)
    {
        $this->isClean = false;
        $this->fields = array_merge($this->fields, $fields);
    }

    /** 
     * Add Field
     * @param Field $field
     */
    public function addField(Field $field)
    {
        $this->isClean = false;
        $this->fields[$field->getFieldName()] = $field;
    }
}
