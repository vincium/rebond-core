CREATE TABLE IF NOT EXISTS `cms_content` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(80) COLLATE utf8_unicode_ci NOT NULL
,    module_id INT UNSIGNED NOT NULL
,    `app_id` INT UNSIGNED NOT NULL
,    `content_group` MEDIUMINT UNSIGNED NOT NULL
,    filter_id INT UNSIGNED NOT NULL
,    author_id INT UNSIGNED NOT NULL
,    publisher_id INT UNSIGNED NOT NULL
,    `url_friendly_title` VARCHAR(80) COLLATE utf8_unicode_ci NOT NULL
,    `use_expiration` TINYINT UNSIGNED NOT NULL
,    `go_live_date` DATETIME NOT NULL
,    `expiry_date` DATETIME NOT NULL
,    `published_date` DATETIME NOT NULL
,    `version` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
