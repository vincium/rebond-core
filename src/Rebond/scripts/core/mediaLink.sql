CREATE TABLE IF NOT EXISTS `core_media_link` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `package` TINYINT UNSIGNED NOT NULL
,    `entity` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `field` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `id_field` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `title_field` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `url` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `filter` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
