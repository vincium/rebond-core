CREATE TABLE IF NOT EXISTS `core_folder` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    parent_id INT UNSIGNED NOT NULL
,    `title` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `display_order` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
