CREATE TABLE IF NOT EXISTS `core_media` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    folder_id INT UNSIGNED NOT NULL
,    `tags` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `upload` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `original_filename` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `path` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `extension` VARCHAR(5) COLLATE utf8_unicode_ci NOT NULL
,    `mime_type` VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL
,    `file_size` MEDIUMINT UNSIGNED NOT NULL
,    `width` SMALLINT UNSIGNED NOT NULL
,    `height` SMALLINT UNSIGNED NOT NULL
,    `alt` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `is_selectable` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
