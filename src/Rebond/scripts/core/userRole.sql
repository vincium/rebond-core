CREATE TABLE IF NOT EXISTS `core_user_role` (
    user_id INT UNSIGNED NOT NULL
,    role_id INT UNSIGNED NOT NULL
    , PRIMARY KEY (user_id, role_id)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
