CREATE TABLE IF NOT EXISTS `core_permission` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL
,    `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
