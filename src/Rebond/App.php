<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond;

use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Security as SecurityEnum;
use Rebond\Models\Core\Site;
use Rebond\Models\Core\User;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Core\SiteRepository;
use Rebond\Services\Cache;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserSecurityService;
use Rebond\Services\Error;
use Rebond\Services\Renderer;
use Rebond\Services\Security;
use Rebond\Services\Session;

class App
{
    /* @var App */
    private static $instance;
    /* @var int */
    private $time;
    /* @var int */
    private $appLevel;
    /* @var bool */
    private $isRunning;
    /* @var bool */
    private $isAjax;
    /* @var string
     * set from site CMS */
    private $url;
    /* @var string */
    private $currentLang;
    /* @var bool */
    private $isDebug;
    /* @var string */
    private $env;
    /* @var string */
    private $timezone;
    /* @var string */
    private $skin;
    /* @var bool */
    private $isTest;
    /* @var array */
    private $queries;
    /* @var Path */
    private $path;
    /* @var User */
    private $signedUser;
    /* @var Site */
    private $site;

    /**
     * @param int $appLevel
     * @return App
     */
    public static function create($appLevel)
    {
        self::$instance = new self($appLevel);
        return self::$instance;
    }

    /**
     * @return App
     * @throws \Exception
     */
    public static function instance()
    {
        if (self::$instance === null) {
            throw new \Exception('Application not created', Code::APP_NOT_CREATED);
        }
        return self::$instance;
    }

    /** @param $appLevel */
    protected function __construct($appLevel)
    {
        $this->time = microtime(true);
        $this->isRunning = false;
        $this->appLevel = $appLevel;
        $this->path = new Path($appLevel);
        $this->trimRequest();
        $this->init();
    }

    public function __destruct()
    {
        Session::kill('redirect');
    }

    private function trimRequest()
    {
        Security::trim($_POST);
        Security::trim($_GET);

        foreach ($_FILES as $key => $value) {
            Security::trim($_FILES[$key]['name']);
        }
    }

    private function init()
    {
        $this->isAjax = false;
        $this->queries = [];
        $this->currentLang = 'en';
        $this->isDebug = true;
        $this->env = getenv('APP_ENV');
        $this->skin = 'default';
        $this->isTest = false;

        $this->timezone = 'Europe/Oslo';
        date_default_timezone_set($this->timezone);

        // error reporting
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        set_exception_handler([Error::class, 'exception']);
        set_error_handler([Error::class, 'error']);
    }

    public function launch()
    {
        $cache = new Cache($this);

        $cache->checkConfig($this->env);
        $this->initLang();
        $cache->checkLocalisation($this->currentLang);
        $this->initSession();
        $this->initSite();

        $cache->checkCssAndJs($this->isDebug, $this->skin);
        $cache->checkModels($this->getSite()->getIsCms());

        $this->isRunning = true;
    }

    private function initLang()
    {
        if ($this->appLevel == AppLevel::STANDALONE) {
            return;
        }

        $foundLang = '';

        $langGet = Converter::stringKey('lang');
        if (isset($langGet)) {
            $foundLang = $langGet;
        } else if (isset($_COOKIE['lang'])) {
            $foundLang = $_COOKIE['lang'];
        }

        $allLang = $this->path->getLangList();
        if (!array_key_exists($foundLang, $allLang)) {
            $foundLang = key($allLang);
        }

        $this->currentLang = $foundLang;

        if (!isset($_COOKIE['lang']) || $_COOKIE['lang'] != $this->currentLang) {
            // 7 days
            $expire = time() + 60 * 60 * 24 * 7;
            setcookie('lang', $this->currentLang, $expire, '/', $this->getConfig('cookie-domain'));
        }

        setlocale(LC_ALL, $allLang[$this->currentLang]);
    }

    private function initSession()
    {
        if ($this->appLevel == AppLevel::STANDALONE) {
            return;
        }

        session_set_cookie_params(0, '/', $this->path->getConfig('cookie-domain'), 0, 1);
        session_start([
            'name' => 'rebond',
            'cookie_path' => '/',
            'cookie_domain' => $this->path->getConfig('cookie-domain'),
        ]);
    }

    private function initSite()
    {
        if (in_array($this->appLevel, [AppLevel::STANDALONE, AppLevel::INSTALL])) {
            return;
        }

        $this->site = SiteRepository::loadById(1);
        $this->isDebug = $this->site->getIsDebug();
        $this->skin = $this->site->getSkin();
        $this->timezone = $this->site->getTimezone();
        date_default_timezone_set($this->timezone);
    }

    /**
     * @param int $code
     * @param string $error
     */
    public function checkRedirect($code, $error)
    {
        $redirect = (int)Session::get('redirect');
        if (++$redirect >= 3) {
            $errorRendering = new Renderer($this);
            echo $errorRendering->configError($code, 'Too many redirects (' . $redirect . '): ' . $error);
            Session::close();
        }
        Session::set('redirect', $redirect);
    }

    /**
     * @return Site
     * @throws \Exception
     */
    public function getSite()
    {
        if (!$this->isRunning || in_array($this->appLevel, [AppLevel::STANDALONE, AppLevel::INSTALL])) {
            return new Site();
        }
        if (!isset($this->site)) {
            $this->site = SiteRepository::loadById(Site::MAIN);
        }
        if (!isset($this->site)) {
            throw new \Exception(Site::MAIN, Code::SITE_NOT_FOUND);
        }
        return $this->site;
    }

    /** @return User */
    public function getUser()
    {
        if (isset($this->signedUser)) {
            return $this->signedUser;
        }

        if (!$this->isRunning) {
            return new User();
        }

        if (in_array($this->appLevel, [AppLevel::STANDALONE, AppLevel::INSTALL])) {
            return new User();
        }

        $session = Session::int('signedUser');
        $cookie = Converter::stringKey('signedUser', 'cookie');
        $apiKey = Security::getApiKey();

        $saveToSession = true;
        if ($session != 0) {
            $this->signedUser = UserRepository::loadById($session);
        } else if ($cookie != '') {
            $this->signedUser = UserSecurityService::getUserBySecure($cookie, SecurityEnum::REMEMBER);
        } else if ($apiKey != '') {
            $this->signedUser = UserSecurityService::getUserBySecure($apiKey, SecurityEnum::API);
            $saveToSession = false;
        }

        if (isset($this->signedUser) && $this->signedUser->getId() != 0) {
            if ($saveToSession && $session != $this->signedUser->getId()) {
                Session::set('signedUser', $this->signedUser->getId());
            }
            return $this->signedUser;
        }
        return new User();
    }

    public function setUser(User $user)
    {
        $this->signedUser = $user;
    }

    public function getFullPath()
    {
        return $this->path->getFullPath();
    }

    public function getRebondPath()
    {
        return $this->path->getRebondPath();
    }

    public function getGeneratedPath()
    {
        return $this->path->getGeneratedPath();
    }

    public function getPath($key)
    {
        return $this->path->getPath($key);
    }

    public function getLangList()
    {
        return $this->path->getLangList();
    }

    public function getConfig($key)
    {
        return $this->path->getConfig($key);
    }

    public function getUserId()
    {
        return $this->getUser()->getId();
    }

    public function getAppLevel()
    {
        return $this->appLevel;
    }

    public function isRunning()
    {
        return $this->isRunning;
    }

    public function isAjax()
    {
        return $this->isAjax;
    }

    public function setAjax($value)
    {
        $this->isAjax = (bool)$value;
    }

    public function url()
    {
        return $this->url;
    }

    public function setUrl($value)
    {
        $this->url = $value;
    }

    public function getCurrentLang()
    {
        return $this->currentLang;
    }

    public function isDebug()
    {
        if ($this->getUser()->getIsDev()) {
            $this->isDebug = true;
        }
        return $this->isDebug;
    }

    public function getEnv()
    {
        return $this->env;
    }

    public function getSkin()
    {
        return $this->skin;
    }

    public function isTest()
    {
        return $this->isTest;
    }

    public function setTest($value)
    {
        $this->isTest = (bool)$value;
    }

    public function getQueries()
    {
        return $this->queries;
    }

    public function addQuery($query)
    {
        $this->queries[] = $query;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public function getTimer()
    {
        return round(microtime(true) - $this->time, 2);
    }
}
