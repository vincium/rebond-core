<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Enums;

class View
{
    // TODO replace views with constants
    const ADMIN_APP_TABLE = 'admin-app-table';
    const ADMIN_APP_TRAIL = 'admin-app-trail';
    const ADMIN_APP_EDITOR = 'admin-app-editor';
    const ADMIN_APP_VIEW = 'admin-app-view';
    const ADMIN_BUS_EDITOR = 'admin-bus-editor';
    const ADMIN_BUS_EDITOR_MULTIKEY = 'admin-bus-editor-multikey';
    const ADMIN_BUS_EDITOR_NOKEY = 'admin-bus-editor-nokey';
    const ADMIN_BUS_TABLE = 'admin-bus-table';
    const CARDS = 'cards';
    const EDITOR = 'editor';
    const EDITOR_SUCCESS = 'editor-success';
    const EDITOR_MULTIKEY = 'editor-multikey';
    const EDITOR_NOKEY = 'editor-nokey';
    const SINGLE = 'single';
    const TABLE = 'table';
    const VIEW = 'view';
}