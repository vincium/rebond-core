<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Result extends AbstractEnum
{
    const NO_CHANGE = 0;
    const SUCCESS = 1;
    const ERROR = 2;
}
