<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Code extends AbstractEnum
{
    const E_UNDEFINED = 0;
    const E_ERROR = 1;
    const E_WARNING = 2;
    const E_NOTICE = 8;
    const E_STRICT = 2048;
    const E_RECOVERABLE_ERROR = 4096;
    const CREDENTIAL_ISSUE = 255;
    const EMAIL_ERROR = 535;

    const DEBUG = 100;

    // Info 2xx
    const SQL_LOG = 200;
    const LANG_GENERATED = 201;
    const MIN_GENERATED = 202;
    const PACKAGE_GENERATED = 203;
    const PASSWORD_CHANGE = 204;
    const USER_SIGN_IN = 205;
    const CRON_JOB = 206;
    const TRACK = 207;

    // Warning 3xx
    const TRANSLATION_NOT_FOUND = 300;
    const COMPONENT_NOT_FOUND = 301;
    const CONTENT_NOT_FOUND = 302;
    const GADGET_NOT_FOUND = 303;
    const ENTITY_NOT_FOUND = 304;
    const TEMPLATE_NOT_FOUND = 305;
    const EMPTY_FILTER = 306;
    const INVALID_TOKEN = 307;
    const MEDIA_NOT_SAVED = 308;
    const VALIDATION_NOT_FOUND = 309;
    const NOT_ENOUGH_PRIVILEGE = 310;

    // Error 4xx
    const UNHANDLED_EXCEPTION = 400;
    const UNHANDLED_ERROR = 401;
    const APP_NOT_CREATED = 402;
    const CONFIGURATION = 403;
    const CONFIG_NOT_FOUND = 404;
    const CONFIG_ENV_NOT_FOUND = 405;
    const CONFIG_KEY_NOT_FOUND = 414;
    const LANG_NOT_FOUND = 406;
    const SQL_BUILD = 407;
    const SQL_COLUMN = 408;
    const SQL_SELECT_ONE = 409;
    const MODULE_NOT_FOUND = 410;
    const PAGE_NOT_FOUND = 411;
    const SITE_NOT_FOUND = 412;
    const ADMIN_PAGE_NOT_FOUND = 413;
    const INVALID_PACKAGE = 414;
    const INVALID_REDIRECT = 415;
    const MAIL_TRANSPORT_NOT_FOUND = 416;

    /**
     * @param int $code
     * @return string
     */
    public static function findLevel($code)
    {
        if ($code == Code::DEBUG) {
            return 'debug';
        }

        if (in_array($code, [Code::E_WARNING, Code::E_NOTICE])) {
            return 'warning';
        }

        if (in_array($code, [
            Code::E_UNDEFINED,
            Code::E_ERROR,
            Code::E_STRICT,
            Code::E_RECOVERABLE_ERROR,
            Code::CREDENTIAL_ISSUE,
            Code::EMAIL_ERROR
        ])) {
            return 'error';
        }

        if ($code <= 299) {
            return 'info';
        }

        if ($code <= 399) {
            return 'warning';
        }
        return 'error';
    }
}
