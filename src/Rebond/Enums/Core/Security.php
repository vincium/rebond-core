<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Security extends AbstractEnum
{
    const REMEMBER = 0;
    const CONFIRM = 1;
    const RESET = 2;
    const API = 3;
}
