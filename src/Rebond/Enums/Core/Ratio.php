<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Ratio extends AbstractEnum
{
    const NO_RATIO = 0;
    const KEEP_WIDTH = 1;
    const KEEP_HEIGHT = 2;
    const KEEP_MAX = 3;
    const KEEP_MIN = 4;
    const SMART = 5;
}
