<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Cms;

use Rebond\Enums\AbstractEnum;

class Menu extends AbstractEnum
{
    const NONE = 0;
    const YES_NO_HOME = 1;
    const YES = 2;
}
