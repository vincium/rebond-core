<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Cms;

use Rebond\Enums\AbstractEnum;

class Component extends AbstractEnum
{
    const SINGLE_ELEMENT = 0;
    const CARDS = 1;
    const FILTERED_CARDS = 2;
    const CUSTOM_LISTING = 3;
    const GENERIC = 4;
}
