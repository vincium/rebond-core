<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Template;

class RepositoryGenerator extends AbstractGenerator
{
    private $sqlEntity;
    private $properties;
    private $table;

    /** 
     * Constructor
     * @param App $app
     * @param string $package
     * @param string $entity
     * @param string $sqlEntity
     * @param array $properties
     */
    public function __construct(App $app, $package, $entity, $sqlEntity, array $properties)
    {
        parent::__construct($app, $package, $entity);
        $this->sqlEntity = $sqlEntity;
        $this->properties = $properties;
        $this->table = strtolower($package) . '_' . $sqlEntity;
    }

    /** 
     * Produce repository
     */
    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'Repository classes');
        $this->produceBaseRepository();
        $this->produceRepository();
    }

    /** 
     * Produce base repository
     */
    private function produceBaseRepository()
    {
        // Base class
        $baseTpl = $this->createTemplate('repository');

        $baseTpl->set('sqlEntity', $this->sqlEntity);
        $baseTpl->set('table', $this->table);
        $baseTpl->set('namespace', $this->getNamespace(true, false));
        $baseTpl->set('mainNamespace', $this->getNamespace(false, false));

        $primaryKey = null;
        $isAutoIncrement = null;
        $multipleKeySql = [];
        $multipleKeyVar = [];
        $selectList = [];
        $insertList = [];
        $insertValueList = [];
        $updateStatus = '';
        $loadUserMethods = [];
        $loadBy = [];
        $deleteBy = [];
        $updateList = [];
        $updateOnDuplicate = [];
        $mapList = [];

        foreach ($this->properties as $property => $options) {
            $type = $options['type'];
            if ($type == 'primaryKey') {
                $isAutoIncrement = Converter::boolKey('isAutoIncrement', $options, true);
                $property = ($this->package == 'App') ? 'app_id' : 'id';
                $primaryKey = $property;
                $baseTpl->set('primaryKey', $primaryKey);
                $baseTpl->set('isAutoIncrement', $isAutoIncrement);
            }
            if ($type == 'multipleKey') {
                $multipleKeySql[] = $property . '_id';
                $multipleKeyVar[] = '$' . Converter::toCamelCase($property) . 'Id';
            }
        }

        if (isset($primaryKey)) {
            $baseTpl->set('firstKey', Converter::toCamelCase($primaryKey, true));
        } else if (count($multipleKeyVar) > 0) {
            $baseTpl->set('firstKey', ucfirst($multipleKeyVar[0]));
        }

        foreach ($this->properties as $property => $options) {
            $isPersistent = Converter::boolKey('isPersistent', $options, true);

            if (!$isPersistent)
                continue;

            $type = $options['type'];
            if ($type == 'primaryKey') {
                $property = ($this->package == 'App') ? 'app_id' : 'id';
            }

            $relationRepository = $this->buildRelation($options, $type == 'media', 'Repository');

            $enumClass = null;
            if ($type == 'enum') {
                $enumClass = Converter::stringKey('enumClass', $options, null);
            }

            $selectList[] = $this->selectList($property, $type);
            $insertList[] = $this->insertList($property, $type);
            $temp = $this->insertValueList($property, $type);
            if (trim($temp) !== '') {
                $insertValueList[] = $temp;
            }
            $updateList[] = $this->updateList($property, $type);
            if (count($multipleKeyVar) > 0) {
                $updateOnDuplicate[] = $this->updateOnDuplicate($property, $type);
            }
            $mapList[] = $this->mapList($property, $type, $relationRepository);

            if (in_array($type, ['email'])) {
                $loadUserMethods[] = $this->loadUserMethods($this->package, $this->entity, $this->sqlEntity, $this->table, $property, $primaryKey);
            }
            if (in_array($type, ['foreignKey', 'multipleKey'])) {
                $loadBy[] = $this->loadAllBy($this->package, $this->entity, $this->sqlEntity, $this->table, $property);
                $deleteBy[] = $this->deleteBy($this->entity, $this->table, $property);
            }
            if (in_array($type, ['singleKey'])) {
                $loadBy[] = $this->loadSingleBy($this->package, $this->entity, $this->sqlEntity, $this->table, $property);
                $deleteBy[] = $this->deleteBy($this->entity, $this->table, $property);
            }
            if ($type == 'enum' && isset($primaryKey) && $enumClass == '\Rebond\Enums\Core\Status') {
                $updateStatus = $this->updateStatus($this->entity, $this->table, $property, $primaryKey);
            }
        }

        $baseTpl->set('multipleKeySql', $multipleKeySql);
        $baseTpl->set('multipleKeyVar', $multipleKeyVar);

        // remove , ' . 
        $temp = implode('', $selectList);
        $temp = str_replace("\r", '', $temp); // line endings safety
        $temp = substr($temp, 0, -6) . '\';' . chr(10);
        $baseTpl->set('selectList', $temp);

        // remove , 
        $temp = implode('', $insertList);
        $temp = trim(preg_replace('/\s+/', ' ', $temp));
        $temp = str_replace("\r", '', $temp); // line endings safety
        $baseTpl->set('insertList', substr($temp, 0, -1));

        $baseTpl->set('insertListParams', implode(',', array_fill(0, count($insertValueList), '?')));

        // remove  . ',
        $temp = implode('', $insertValueList);
        $temp = str_replace("\r", '', $temp); // line endings safety
        $baseTpl->set('insertValueList', substr($temp, 0, -2));

        $baseTpl->set('loadBy', implode('', $loadBy));
        $baseTpl->set('loadUserMethods', implode('', $loadUserMethods));
        $baseTpl->set('updateStatus', $updateStatus);
        $baseTpl->set('deleteBy', implode('', $deleteBy));

        // remove . ',
        $temp = implode('', $updateList);
        $temp = str_replace("\r", '', $temp); // line endings safety
        $baseTpl->set('updateList', $temp);

        if (count($multipleKeyVar) > 0) {
            $temp = implode('', $updateOnDuplicate);
            $temp = str_replace("\r", '', $temp); // line endings safety
            $baseTpl->set('updateOnDuplicate', $temp);
        }

        $baseTpl->set('mapList', implode('', $mapList));

        $tplName = ($this->package == 'App') ? 'base-app' : 'base';

        $basePath = $this->getNamespace() . 'Repository/' . $this->package . '/Base' . $this->entity . 'Repository.php';

        $render = str_replace('<#php', '<?php', $baseTpl->render($tplName));
        File::save($basePath, $render);
        $this->addInfo(self::INFO_SUCCESS, 'Repository base class created');
    }

    /** 
     * Produce repository
     */
    private function produceRepository()
    {
        // Data class
        $dataTpl = $this->createTemplate('repository');

        $namespace = $this->getNamespace(false, false);
        $dataTpl->set('namespace', $namespace);
        $dataTpl->set('extends', ($namespace != 'Rebond') ? '\Generated\Repository\\' . $this->package . '\\' : '');

        $tplName = ($this->package == 'App') ? 'repository-app' : 'repository';

        $dataPath = $this->getNamespace(false) . 'Repository/' . $this->package . '/' . $this->entity . 'Repository.php';

        if (!file_exists($dataPath)) {
            $render = str_replace('<#php', '<?php', $dataTpl->render($tplName));
            File::save($dataPath, $render);
            $this->addInfo(self::INFO_SUCCESS, 'Repository class created');
        } else {
            $this->addInfo(self::INFO_WARNING , 'Repository class already exists');
        }
    }

    private function selectList($property, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property, true));
        $tpl->set('type', $type);
        return $tpl->render('select-list');
    }

    private function insertList($property, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('type', $type);
        return $tpl->render('insert-list');
    }

    private function insertValueList($property, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        $tpl->set('type', $type);
        return $tpl->render('insert-value-list');
    }

    private function updateList($property, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        $tpl->set('type', $type);
        return $tpl->render('update-list');
    }

    private function updateOnDuplicate($property, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        $tpl->set('type', $type);
        return $tpl->render('update-on-duplicate');
    }

    private function mapList($property, $type, $relation)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        $tpl->set('type', $type);
        $tpl->set('relation', $relation);
        return $tpl->render('map-list');
    }

    private function loadAllBy($package, $entity, $sqlEntity, $table, $property)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('package', $package);
        $tpl->set('entity', $entity);
        $tpl->set('sqlEntity', $sqlEntity);
        $tpl->set('table', $table);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        return $tpl->render('load-all-by');
    }

    private function loadSingleBy($package, $entity, $sqlEntity, $table, $property)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('package', $package);
        $tpl->set('entity', $entity);
        $tpl->set('sqlEntity', $sqlEntity);
        $tpl->set('table', $table);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        return $tpl->render('load-single-by');
    }

    private function updateStatus($entity, $table, $property, $primaryKey)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('entity', $entity);
        $tpl->set('table', $table);
        $tpl->set('property', $property);
        $tpl->set('primaryKey', $primaryKey);
        return $tpl->render('update-status');
    }

    private function loadUserMethods($package, $entity, $sqlEntity, $table, $property, $primaryKey)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('package', $package);
        $tpl->set('entity', $entity);
        $tpl->set('sqlEntity', $sqlEntity);
        $tpl->set('table', $table);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        $tpl->set('primaryKey', $primaryKey);
        return $tpl->render('load-user-methods');
    }

    private function deleteBy($entity, $table, $property)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'repository', 'parts']);
        $tpl->set('entity', $entity);
        $tpl->set('table', $table);
        $tpl->set('property', $property);
        $tpl->set('propertyName', Converter::toCamelCase($property));
        return $tpl->render('delete-by');
    }
}
