<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Template;

class FormGenerator extends AbstractGenerator
{
    private $properties;
    private $multipleKeys;

    /** 
     * Constructor
     * @param App $app
     * @param string $package
     * @param string $entity
     * @param array $properties
     */
    public function __construct(App $app, $package, $entity, array $properties)
    {
        parent::__construct($app, $package, $entity);
        $this->properties = $properties;
        $this->multipleKeys = [];
    }

    /** 
     * Produce models
     */
    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'Form classes');
        $this->produceBaseForm();
        $this->produceForm();
    }

    /** 
     * Produce base model
     */
    private function produceBaseForm()
    {
        // Base class
        $baseTpl = $this->createTemplate('form');

        $primaryKey = null;
        $isAutoIncrement = null;
        $propertyInitBase = [];
        $propertyList = [];
        $propertySetPost = [];
        $propertyMethod = [];
        $propertyValidate = [];

        $baseTpl->set('namespace', $this->getNamespace(true, false));
        $baseTpl->set('mainNamespace', $this->getNamespace(false, false));

        foreach ($this->properties as $property => $options) {
            $type = $options['type'];
            if ($type == 'primaryKey') {
                $isAutoIncrement = Converter::boolKey('isAutoIncrement', $options, true);
                $primaryKey = ($this->package == 'App') ? 'appId' : 'id';
                $baseTpl->set('primaryKey', $primaryKey);
            } else if ($type == 'multipleKey') {
                $this->multipleKeys[] = $property;
            }
        }

        foreach ($this->properties as $property => $options) {
            $type = $options['type'];
            $isPersistent = Converter::boolKey('isPersistent', $options, true);

            if (!$isPersistent || in_array($type, ['foreignKeyLink', 'singleKeyLink', 'createdDate', 'modifiedDate'])) {
                continue;
            }

            $propertyName = Converter::toCamelCase($property);

            if ($type == 'primaryKey') {
                $propertyName = ($this->package == 'App') ? 'appId' : 'id';
            }

            $link = ($type != 'media') ? Converter::stringKey('link', $options, null) : 'title';
            $relationRepository = $this->buildRelation($options, $type == 'media', 'Repository');

            $validations = Converter::arrayKey('validation', $options, []);
            $validationList = array_merge([$type => true], $validations);

            $propertyList[] = $this->propertyList($propertyName, $type, $isAutoIncrement);
            $propertyInitBase[] = $this->propertyInitBase($propertyName, $type, $validationList, $isAutoIncrement);
            $propertySetPost[] = $this->propertySetPost($propertyName, $type);
            $propertyMethod[] = $this->propertyMethod($property, $propertyName, $type, $relationRepository, $link, $isAutoIncrement);
            $propertyValidate[] = $this->propertyValidate($propertyName, $type, $isAutoIncrement);
        }

        $baseTpl->set('propertyList', implode('', $propertyList));
        $baseTpl->set('propertyInitBase', implode('', $propertyInitBase));
        $baseTpl->set('propertySetPost', implode('', $propertySetPost));
        $baseTpl->set('propertyValidate', implode('', $propertyValidate));
        $baseTpl->set('propertyMethod', implode('', $propertyMethod));

        $tplName = ($this->package == 'App') ? 'base-app' : 'base';

        $basePath = $this->getNamespace() . 'Forms/' . $this->package . '/Base' . $this->entity . 'Form.php';

        $render = str_replace('<#php', '<?php', $baseTpl->render($tplName));
        File::save($basePath, $render);
        $this->addInfo(self::INFO_SUCCESS, 'Form base class created');
    }

    /** 
     * Produce model
     */
    private function produceForm()
    {
        // Form class
        $formTpl = $this->createTemplate('form');

        $namespace = $this->getNamespace(false, false);
        $formTpl->set('namespace', $namespace);
        $formTpl->set('extends', ($namespace != 'Rebond') ? '\Generated\Forms\\' . $this->package . '\\' : '');

        $tplName = ($this->package == 'App') ? 'form-app' : 'form';

        $modelPath = $this->getNamespace(false) . 'Forms/' . $this->package . '/' . $this->entity . 'Form.php';

        if (!file_exists($modelPath)) {
            $render = str_replace('<#php', '<?php', $formTpl->render($tplName));
            File::save($modelPath, $render);
            $this->addInfo(self::INFO_SUCCESS, 'Form class created');
        } else {
            $this->addInfo(self::INFO_WARNING, 'Form class already exists');
        }
    }

    private function propertyList($propertyName, $type, $isAutoIncrement)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        $tpl->set('isAutoIncrement', $isAutoIncrement);
        return $tpl->render('property-list');
    }

    private function propertyInitBase($propertyName, $type, $validationList, $isAutoIncrement)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);

        $propValidation = '';
        if (!in_array($type, ['primaryKey', 'foreignKeyLink', 'singleKeyLink']) || ($type == 'primaryKey' && !$isAutoIncrement)) {
            $propValidation = str_repeat(' ', 8) . '$this->' . $propertyName . 'Validator = [';
            $validation = [];
            if (count($validationList) > 0) {
                foreach ($validationList as $key => $value) {
                    if (is_bool($value)) {
                        $value = ($value) ? 'true' : 'false';
                    }
                    $validation[] = '\'' . $key . '\' => ' . $value;
                }
            }
            $propValidation .= implode(', ', $validation) . '];' . chr(10);
        }
        $tpl->set('propValidation', $propValidation);
        return $tpl->render('init-base');
    }

    private function propertySetPost($propertyName, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('entity', $this->entity);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        return $tpl->render('property-set-post');
    }

    private function propertyValidate($propertyName, $type, $isAutoIncrement)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('package', $this->package);
        $tpl->set('entity', $this->entity);
        $tpl->set('type', $type);
        $tpl->set('isAutoIncrement', $isAutoIncrement);
        return $tpl->render('property-validate');
    }

    private function propertyMethod($property, $propertyName, $type, $relation, $link, $isAutoIncrement)
    {
        // validate
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('package', $this->package);
        $tpl->set('entity', $this->entity);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        $tpl->set('isAutoIncrement', $isAutoIncrement);
        $validate = $tpl->render('validate-method');

        // build
        $tpl = new Template(Template::ADMIN, ['generator', 'form', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('entity', $this->entity);
        $tpl->set('type', $type);
        $tpl->set('relation', $relation);
        $tpl->set('link', $link);
        $tpl->set('isAutoIncrement', $isAutoIncrement);
        if ($type == 'multipleKey') {
            $otherMultipleKey = array_values(array_diff($this->multipleKeys, [$propertyName]))[0];
            $tpl->set('otherMultipleKey', $otherMultipleKey);
        }
        $build = $tpl->render('build-method');
        return $validate . $build;
    }
}
