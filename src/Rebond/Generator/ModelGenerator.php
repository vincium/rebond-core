<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Template;

class ModelGenerator extends AbstractGenerator
{
    private $mainProperty;
    private $isPersistent;
    private $properties;
    private $multipleKeys;

    /** 
     * Constructor
     * @param App $app
     * @param string $package
     * @param string $entity
     * @param string $mainProperty
     * @param bool $isPersistent
     * @param array $properties
     */
    public function __construct(App $app, $package, $entity, $mainProperty, $isPersistent, array $properties)
    {
        parent::__construct($app, $package, $entity);
        $this->mainProperty = $mainProperty;
        $this->isPersistent = $isPersistent;
        $this->properties = $properties;
        $this->multipleKeys = [];
    }

    /** 
     * Produce models
     */
    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'Model classes');
        $this->produceBaseModel();
        $this->produceModel();
    }

    /** 
     * Produce base model
     */
    private function produceBaseModel()
    {
        // Base class
        $baseTpl = $this->createTemplate('model');
        $baseTpl->set('isPersistent', $this->isPersistent);
        $baseTpl->set('namespace', $this->getNamespace(true, false));
        $baseTpl->set('mainNamespace', $this->getNamespace(false, false));

        $toString = '\'' . $this->entity . '\'';
        $primaryKey = null;
        $propertyList = [];
        $propertySetList = [];
        $propertySetDefault = [];
        $propertyMethod = [];
        $toArray = [];

        foreach ($this->properties as $property => $options) {
            $type = $options['type'];
            $propertyName = Converter::toCamelCase($property);
            if ($type == 'primaryKey') {
                $propertyName = ($this->package == 'App') ? 'app_id' : 'id';
                $primaryKey = $propertyName;
                $baseTpl->set('primaryKey', $primaryKey);
                $toString = '\'' . $this->entity . ': \' . $this->getId()';
            }
            if ($type == 'multipleKey') {
                $this->multipleKeys[] = $propertyName;
            }
        }

        foreach ($this->properties as $property => $options) {
            $type = $options['type'];
            $propertyName = Converter::toCamelCase($property);
            if ($type == 'primaryKey') {
                $propertyName = ($this->package == 'App') ? 'appId' : 'id';
            }
            $default = $this->getDefaultValue(Converter::stringKey('default', $options, null), $type);

            $link = ($type != 'media') ? Converter::stringKey('link', $options, null) : 'title';
            $relationRepository = $this->buildRelation($options, $type == 'media', 'Repository');
            $relationModel = $this->buildRelation($options, $type == 'media', 'Models');
            $phpType = $this->getPhpType($type, $relationModel);

            $enumClass = null;
            $enumList = [];

            if ($type == 'enum') {
                $enumList = Converter::arrayKey('enumList', $options, []);
                $enumClass = Converter::stringKey('enumClass', $options, null);
            }

            $propertyList[] = $this->propertyList($propertyName, $type, $enumClass, $phpType, $relationModel);
            $propertySetList[] = $this->propertySetList($propertyName, $type, $enumList);
            $propertySetDefault[] = $this->propertySetDefault($propertyName, $type, $default);
            $propertyMethod[] = $this->propertyMethod($propertyName, $type, $phpType, $relationRepository, $link, $relationModel, $enumClass);

            if ($property == $this->mainProperty) {
                $toString = ($type == 'enum')
                    ? '$this->get' . ucfirst($propertyName) . 'Value()'
                    : '$this->get' . ucfirst($propertyName) . '()';
            }

            $toArray[] = $this->toArray($propertyName, $type);
        }

        $multipleKeyVar = array_map(function($propertyName) {
            return '$this->' . $propertyName . 'Id';
        }, $this->multipleKeys);

        $baseTpl->set('toArray', implode('', $toArray));
        $baseTpl->set('toString', $toString);
        $baseTpl->set('multipleKeyVar', $multipleKeyVar);
        $baseTpl->set('propertyList', implode('', $propertyList));
        $baseTpl->set('propertySetList', implode('', $propertySetList));
        $baseTpl->set('propertySetDefault', implode('', $propertySetDefault));
        $baseTpl->set('propertyMethod', implode('', $propertyMethod));



        $tplName = ($this->package == 'App') ? 'base-app' : 'base';

        $basePath = $this->getNamespace() . 'Models/' . $this->package . '/Base' . $this->entity . '.php';

        $render = str_replace('<#php', '<?php', $baseTpl->render($tplName));
        File::save($basePath, $render);
        $this->addInfo(self::INFO_SUCCESS, 'Model base class created');
    }

    /** 
     * Produce model
     */
    private function produceModel()
    {
        // Model template
        $modelTpl = $this->createTemplate('model');

        $namespace = $this->getNamespace(false, false);
        $modelTpl->set('namespace', $namespace);
        $modelTpl->set('extends', ($namespace != 'Rebond') ? '\Generated\Models\\' . $this->package . '\\' : '');

        $tplName = ($this->package == 'App') ? 'model-app' : 'model';

        $modelPath = $this->getNamespace(false) . 'Models/' . $this->package . '/' . $this->entity . '.php';

        if (!file_exists($modelPath)) {
            $render = str_replace('<#php', '<?php', $modelTpl->render($tplName));
            File::save($modelPath, $render);
            $this->addInfo(self::INFO_SUCCESS, 'Model class created');
        } else {
            $this->addInfo(self::INFO_WARNING , 'Model class already exists');
        }
    }

    private function propertyList($propertyName, $type, $enumClass, $phpType, $relationModel)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'model','parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        $tpl->set('enumClass', $enumClass);
        $tpl->set('relationModel', $relationModel);
        $tpl->set('phpType', $phpType);
        return $tpl->render('property-list');
    }

    private function propertySetList($propertyName, $type, $enumList)
    {
        if ($type != 'enum') {
            return '';
        }

        if (!empty($enumList)) {
            $enum = [];
            foreach ($enumList as $key => $value) {
                $enum[] = $key . ' => \'' . $value . '\'';
            }
            return str_repeat(' ', 8) . '$this->' . $propertyName . 'List = [' . implode(', ', $enum) . '];' . chr(10);
        }
        return '';
    }

    private function propertySetDefault($propertyName, $type, $default)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'model','parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        $tpl->set('default', $default);
        return $tpl->render('set-default');
    }

    private function propertyMethod($propertyName, $type, $phpType, $relationRepository, $link, $relationModel, $enumClass)
    {
        // set
        $tpl = new Template(Template::ADMIN, ['generator', 'model', 'parts']);
        $tpl->set('package', $this->package);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        $tpl->set('phpType', $phpType);
        $tpl->set('relationModel', $relationModel);
        $set = $tpl->render('set-method');

        // get
        $tpl->set('entity', $this->entity);
        $tpl->set('relationRepository', $relationRepository);
        $tpl->set('link', $link);
        $tpl->set('enumClass', $enumClass);
        if ($type == 'multipleKey') {
            $otherMultipleKey = array_values(array_diff($this->multipleKeys, [$propertyName]))[0];
            $tpl->set('otherMultipleKey', $otherMultipleKey);
        }
        $get = $tpl->render('get-method');

        return $get . $set;
    }

    private function toArray($propertyName, $type)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'model', 'parts']);
        $tpl->set('propertyName', $propertyName);
        $tpl->set('type', $type);
        return $tpl->render('to-array');
    }
}
