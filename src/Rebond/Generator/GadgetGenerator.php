<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Enums\Cms\Component;
use Rebond\Repository\Cms\ComponentRepository;
use Rebond\Services\File;

class GadgetGenerator extends AbstractGenerator
{
    private $hasContent;
    private $views;
    private $id;

    /** 
     * Constructor
     * @param string $package
     * @param string $entity
     * @param array $views
     * @param bool $hasContent
     * @param int $id
     */
    public function __construct(App $app, $package, $entity, array $views, $hasContent, $id)
    {
        parent::__construct($app, $package, $entity);
        $this->hasContent = $hasContent;
        $this->views = $views;
        $this->id = $id;
    }

    /** 
     * Produce models
     */
    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'Gadget class');
        $this->produceGadget();
    }

    /** 
     * Produce gadget
     */
    private function produceGadget()
    {
        // Base class
        $gadgetTpl = $this->createTemplate('gadget');
        $gadgetTpl->set('hasContent', $this->hasContent);

        // add component
        $component = new \Rebond\Models\Cms\Component();
        $component->setModuleId($this->id);
        $component->setStatus(0);

        foreach ($this->views as $view) {
            $component->setId(0);
            switch ($view) {
                case 'cards':
                    $options = [];
                    $options['where'][] = ['component.type = ?', Component::CARDS];
                    $options['where'][] = ['component.module_id = ?', $this->id];
                    if (ComponentRepository::count($options) == 0) {
                        $component->setType(Component::CARDS);
                        $component->setTitle('cards');
                        $component->setSummary('display all cards');
                        $component->setMethod('cards');
                        $component->save();
                    }
                    break;
                case 'filtered-cards':
                    $options = [];
                    $options['where'][] = ['component.type = ?', Component::FILTERED_CARDS];
                    $options['where'][] = ['component.module_id = ?', $this->id];
                    if (ComponentRepository::count($options) == 0) {
                        $component->setType(Component::FILTERED_CARDS);
                        $component->setTitle('filtered_cards');
                        $component->setSummary('display filtered cards');
                        $component->setMethod('filteredCards');
                        $component->save();
                    }
                    break;
                case 'single':
                    $options = [];
                    $options['where'][] = ['component.type = ?', Component::SINGLE_ELEMENT];
                    $options['where'][] = ['component.module_id = ?', $this->id];
                    if (ComponentRepository::count($options) == 0) {
                        $component->setType(Component::SINGLE_ELEMENT);
                        $component->setTitle('single_element');
                        $component->setSummary('display single element');
                        $component->setMethod('single');
                        $component->save();
                    }
                    break;
                case 'editor':
                    $options = [];
                    $options['where'][] = ['component.type = ?', Component::GENERIC];
                    $options['where'][] = ['component.module_id = ?', $this->id];
                    $options['where'][] = 'component.method = \'form\'';
                    if (ComponentRepository::count($options) == 0) {
                        $component->setType(Component::GENERIC);
                        $component->setTitle('editor');
                        $component->setSummary('display editor');
                        $component->setMethod('editor');
                        $component->save();
                    }
                    break;
            }
        }

        $gadgetTpl->set('views', $this->views);
        $gadgetPath = $this->app->getPath('own') . 'Gadgets/App/' . $this->entity . 'Gadget.php';
        if (!file_exists($gadgetPath)) {
            $render = str_replace('<#php', '<?php', $gadgetTpl->render('gadget'));
            File::save($gadgetPath, $render);
            $this->addInfo(self::INFO_SUCCESS, 'Gadget class created');
        } else {
            $this->addInfo(self::INFO_WARNING, 'Gadget class already exists');
        }
    }
}
