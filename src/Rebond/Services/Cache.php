<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Generator\ConfigGenerator;
use Rebond\Generator\LangGenerator;

class Cache
{
    /** @var App */
    private $app;

    /** @param App $app */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $name
     * @param string $extra
     * @return mixed|null
     */
    public function getCache($name, $extra)
    {
        $cache = $this->app->getSite()->getCacheTime();
        if ($cache == 0) {
            return null;
        }

        $file = $this->app->getPath('own-cache-file') . $name . '_' . $extra . '.tpl';

        if (file_exists($file) && (filemtime($file) + $cache) > time()) {
            // Output the existing file to the user
            return unserialize(file_get_contents($file));
        }
        return null;
    }

    /**
     * @param string $name
     * @param string $extra
     * @param $data
     */
    public function saveCache($name, $extra, $data)
    {
        $cache = $this->app->getSite()->getCacheTime();
        if ($cache == 0) {
            return;
        }

        $file = $this->app->getPath('own-cache-file') . $name . '_' . $extra . '.tpl';
        File::save($file, serialize($data));
    }

    /**
     * @param string $name
     * @param string $extra
     * @return null|string
     */
    public function getGadgetCache($name, $extra)
    {
        $cache = $this->app->getSite()->getCacheTime();
        if ($cache == 0) {
            return null;
        }

        $file = $this->app->getPath('own-cache-file') . 'gadget_' . $name . '_' . $extra . '.tpl';

        if (file_exists($file) && (filemtime($file) + $cache) > time()) {
            $html = file_get_contents($file);
            return $html;
        }
        return null;
    }

    /**
     * @param string $name
     * @param string $extra
     * @param string $html
     */
    public function saveGadgetCache($name, $extra, $html)
    {
        $cache = $this->app->getSite()->getCacheTime();
        if ($cache == 0) {
            return;
        }

        $file = $this->app->getPath('own-cache-file') . 'gadget_' . $name . '_' . $extra . '.tpl';
        File::save($file, $html);
    }

    /** @param string $env */
    public function checkConfig($env)
    {
        if ($this->app->getAppLevel() == AppLevel::STANDALONE) {
            return;
        }
        $cacheFolder = $this->app->getPath('generated-cache');
        File::createFolder($cacheFolder);
        $ymlFile = $this->app->getPath('own-file') . 'config.yaml';
        $cachedFile = $cacheFolder . 'Config.php';

        if (!class_exists(\Generated\Cache\Config::class) || filemtime($ymlFile) > filemtime($cachedFile)) {
            $config = new ConfigGenerator($this->app, $env);
            $config->generate();
        }
    }

    /** @param string $lang */
    public function checkLocalisation($lang)
    {
        if ($this->app->getAppLevel() == AppLevel::STANDALONE) {
            return;
        }

        $ymlRebond = $this->app->getPath('rebond-lang-file') . 'rebond.yaml';
        $ymlOwn = $this->app->getPath('own-lang-file') . 'own.yaml';
        $cachedRebond = $this->app->getPath('generated-cache') . 'Rebond' . ucfirst($lang) . '.php';
        $cachedOwn = $this->app->getPath('generated-cache') . 'Own' . ucfirst($lang) . '.php';
        $cachedAdminJs = $this->app->getConfig('admin') . 'js/lang-' . $lang . '.js';
        $cachedWwwJs = $this->app->getConfig('site') . 'js/lang-' . $lang . '.js';

        if (!file_exists($cachedRebond) || !file_exists($cachedOwn)
            || !file_exists($cachedAdminJs) || !file_exists($cachedWwwJs)
            || filemtime($ymlRebond) > filemtime($cachedRebond)
            || filemtime($ymlOwn) > filemtime($cachedOwn)
        ) {
            $lang = new LangGenerator($this->app);
            $lang->generate();
        }
    }

    /**
     * @param bool $isDebug
     * @param string $skin
     */
    public function checkCssAndJs($isDebug, $skin)
    {
        if ($isDebug || in_array($this->app->getAppLevel(), [AppLevel::STANDALONE, AppLevel::INSTALL])) {
            return;
        }

        $sitePath = $this->app->getConfig('site');

        // Css
        $run = false;

        $styles = [];
        $styles[] = $sitePath . 'node_modules/normalize-css/normalize.css';
        $styles[] = $sitePath . 'node_modules/components-font-awesome/css/font-awesome.min.css';
        $styles[] = $sitePath . 'node_modules/rebond-assets/css/rebond.css';
        $styles[] = $sitePath . 'node_modules/rebond-assets-site/css/rebond-site.css';
        $styles[] = $sitePath . 'css/skin/' . $skin . '/own.css';

        $minCss = $sitePath . 'css/min-' . $skin . '.css';

        if (!file_exists($minCss)) {
            $run = true;
        } else {
            foreach ($styles as $css) {
                if (filemtime($css) > filemtime($minCss)) {
                    $run = true;
                    break;
                }
            }
        }

        if ($run) {
            $compressor = new CSS();
            foreach ($styles as $style) {
                $compressor->add($style);
            }
            $compressor->minify($minCss);
            Log::log(Code::MIN_GENERATED, 'Generation of css bundle', __FILE__, __LINE__);
        }

        // Js
        $run = false;

        $scripts = [];
        $scripts[] = $sitePath . 'node_modules/rebond-assets/js/rebond.js';
        $scripts[] = $sitePath . 'node_modules/rebond-assets-site/js/rebond-site.js';
        $scripts[] = $sitePath . 'js/own.js';

        $minJs = $sitePath . 'js/min.js';

        if (!file_exists($minJs)) {
            $run = true;
        } else {
            foreach ($scripts as $js) {
                if (filemtime($js) > filemtime($minJs)) {
                    $run = true;
                    break;
                }
            }
        }

        if ($run) {
            $compressor = new JS();
            foreach ($scripts as $script) {
                $compressor->add($script);
            }
            $compressor->minify($minJs);
            Log::log(Code::MIN_GENERATED, 'Generation of js bundle', __FILE__, __LINE__);
        }
    }

    /** @param bool $isCms */
    public function checkModels($isCms)
    {
        if (in_array($this->app->getAppLevel(), [AppLevel::STANDALONE, AppLevel::INSTALL])) {
            return;
        }

        $generatedPath = $this->app->getPath('generated');
        if ($isCms && !file_exists($generatedPath . 'Models/App')) {
            try {
                $generator = new Generator($this->app, 'app', false);
                $generator->run();
            } catch(\Exception $ex) {
                Session::set('allError', $ex->getMessage());
            }
        }

        if (!file_exists($generatedPath . 'Models/Bus')) {
            try {
                $generator = new Generator($this->app, 'bus', false);
                $generator->run();
            } catch(\Exception $ex) {
                Session::set('allError', $ex->getMessage());
            }
        }
    }
}
