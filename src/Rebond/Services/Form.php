<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\Models\AbstractModel;

class Form
{
    /**
     * Is field required
     * @param array $validate
     * @return string
     */
    public static function isReq($validate)
    {
        if (isset($validate) && is_array($validate)) {
            foreach ($validate as $key => $value) {
                if (in_array($key, ['required', 'foreignKey', 'singleKey', 'multipleKey']) && $value) {
                    return ' <span class="text-error">*</span>';
                }
            }
        }
        return '';
    }

    /**
     * Render title
     * @param string $name
     * @param int $id
     * @param string $title = null
     * @return string
     */
    public static function renderTitle($name, $id = 0, $title = null)
    {
        if ($id == 0) {
            return '<h2>' . Lang::lang('new') . ' ' . Lang::lang($name) . '</h2>';
        }
        $title = isset($title) ? ' (' . $title . ')' : '';
        return '<h2>' . Lang::lang('edit') . ' ' . Lang::lang($name) . $title . '</h2>';
    }

    /**
     * Build submit button
     * @param int $id = 0
     * @param string $type = null
     * @return string
     */
    public static function buildSubmit($id = 0, $type = null)
    {
        if (!isset($type)) {
            $type = ($id == 0) ? 'add' : 'save';
            $name = 'Save';
        } else {
            $name = Converter::toCamelCase($type, true);
        }
        return '<button type="submit" class="rb-btn" name="btn' . $name . '" id="btn' . $name . '">' . Lang::lang($type) . '</button>';
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function isSubmitted($name = 'btnSave')
    {
        return Converter::stringKey($name, 'post') !== null;
    }

    /**
     * Build a Field
     * @param string $type
     * @param string $field
     * @param string $value = ''
     * @return string
     */
    public static function buildField($field, $type, $value = '')
    {
        switch ($type) {
            case 'integer':
            case 'foreignKey':
            case 'singleKey':
            case 'staticKey':
            case 'decimal':
            case 'string':
                return '<input type="text" class="input" name="' . $field . '" id="' . $field . '" value="' . $value . '">';

            case 'url':
                return '<input type="text" class="input" name="' . $field . '" id="' . $field . '" value="' . $value . '">';

            case 'email':
                return '<input type="text" class="input" name="' . $field . '" id="' . $field . '" value="' . $value . '">';

            case 'color':
                return '<input type="color" class="input" name="' . $field . '" id="' . $field . '" value="' . $value . '">';

            case 'password':
            case 'confirm':
                $html = '<input type="password" class="input password" id="' . $field . '" name="' . $field . '" value="">';
                $html .= '<a href="#" class="password-viewer" data-id="' . $field . '" tabindex="-1"><i class="fa fa-eye" aria-hidden="true"></i>
</a>';
                return $html;

            case 'text':
                return '<textarea name="' . $field . '" class="input" id="' . $field . '">' . $value . '</textarea>';

            case 'richText':
                return '<textarea class="richText input" name="' . $field . '" id="' . $field . '">' . $value . '</textarea>';

            case 'date':
                return self::buildDate($field, $value);

            case 'datetime':
                $html = self::buildDate($field, $value);
                $html .= self::buildTime($field, $value);
                return $html;

            case 'time':
                return self::buildTime($field, $value);

            case 'media':
                $html = '<div class="input-file" data-field="' . $field . '">';
                $html .= '<img src="' . Media::showFromModel($value) . '" alt="media" />';
                $html .= '<a href="#" class="file-reset" id="file-reset-' . $field . '" data-field="'. $field . '">' . Lang::lang('reset') . '</a>';
                $html .= '<a href="#" class="file-remove" id="file-remove-' . $field . '" data-field="' . $field . '">' . Lang::lang('remove') . '</a>';
                $html .= '<input type="file" class="input" name="' . $field . '" id="' . $field . '" data-field="'. $field . '">';
                $html .= '<a href="#" class="rb-btn file-clear" id="file-clear-' . $field . '" data-field="'. $field . '">' . Lang::lang('clear') . '</a>';
                $html .= '<input type="hidden" id="remove-' . $field . '" name="remove-' . $field . '" value="0">';
                $html .= '</div>';
                return $html;

            case 'primaryKey':
            case 'hidden':
                return '<input type="hidden" name="' . $field . '" id="' . $field . '" value="' . (string) $value . '">';

            case 'submit':
                return '<button type="submit" class="rb-btn" name="' . $field . '" id="' . $field . '">' . $value . '</button>';

            default:
                return '<div class="status-error">' . Lang::lang('error_file_type_not_found', [$type]) . '</div>';
        }
    }

    private static function buildDate($field, $value)
    {
        if (empty($value)) {
            $value = new \DateTime();
        }
        return '<input class="input date-picker" type="text" name="' . $field . '" id="' . $field . '" value="' . $value->format('Y-m-d') . '" readonly="readonly">';
    }

    private static function buildTime($field, $value)
    {
        if (empty($value)) {
            $value = new \DateTime();
        }
        $hour = $value->format('H');

        $html = ' <input type="number" min="0" max="23" class="input input-small" id="' . $field . '-h" name="' . $field . '-h" value="' . $hour . '">';
        $html .= ' : ';
        $html .= '<input type="number" min="0" max="59" class="input input-small" id="' . $field . '-m" name="' . $field . '-m" value="0">';
        return $html;
    }

    /**
     * Build a list of items
     * @param string $field
     * @param AbstractModel[] $items
     * @param string $key
     * @param string $name
     * @param int $selectedValue
     * @param bool $required = true
     * @param bool $isLocalized = false
     * @return string
     */
    public static function buildItemList($field, $items, $key, $name, $selectedValue = null, $required = true, $isLocalized = false)
    {
        $pk = 'get' . ucfirst($key);
        $na = 'get' . ucfirst($name);
        $html = '<select class="input" name="' . $field . '" id="' . $field . '">';
        if (empty($required)) {
            $html .= '<option value="0">' . Lang::lang('option_select') . '</option>';
        }
        foreach ($items as $item) {
            $selected = (isset($selectedValue) && $selectedValue == $item->$pk()) ? ' selected' : '';
            $title = $isLocalized ? Lang::lang($item->$na()) : $item->$na();
            $html .= '<option value="' . $item->$pk() . '"' . $selected . '>' . $title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * Build a static list
     * @param string $field
     * @param array $list
     * @param int $selectedValue = null
     * @param bool $required = true
     * @param array $disabledValues = null
     * @param string $class = null
     * @return string
     */
    public static function buildList($field, array $list, $selectedValue = null, $required = true, $disabledValues = null, $class = null)
    {
        $cl = (isset($class)) ? $class : '';
        $html = '<select class="input ' . $cl . '" name="' . $field . '" id="' . $field . '">';
        if (empty($required)) {
            $html .= '<option value="0">' . Lang::lang('option_select') . '</option>';
        }
        if (is_array($list)) {
            foreach ($list as $key => $val) {
                $selected = (isset($selectedValue) && $selectedValue == $key) ? ' selected' : '';
                $disabled = (isset($disabledValues) && in_array($key, $disabledValues)) ? ' disabled' : '';
                $html .= '<option value="' . $key . '"' . $selected . $disabled . '>' . str_replace('_', ' ', $val) . '</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * Build a radio list
     * @param string $field
     * @param AbstractModel[] $items
     * @param string $key
     * @param string $name
     * @param int $selectedValue
     * @return string
     */
    public static function buildRadioList($field, array $items, $key, $name, $selectedValue = null)
    {
        $pk = 'get' . ucfirst($key);
        $na = 'get' . ucfirst($name);
        $html = '';
        foreach ($items as $item) {
            $checked = (isset($selectedValue) && $selectedValue == $item->$pk()) ? ' checked' : '';
            $html .= '<label class="check">';
            $html .= '<input type="radio" class="checkbox" id="' . $field . '-' . $item->$pk() . '" name="' . $field . '" value="' . $item->$pk() . '"' . $checked . '>';
            $html .= $item->$na() . '</label>';
        }
        return $html;
    }

    /**
     * Build a static radio list
     * @param string $field
     * @param array $list
     * @param int $selectedValue
     * @return string
     */
    public static function buildRadio($field, $list, $selectedValue = null)
    {
        $html = '';
        if (is_array($list)) {
            foreach ($list as $key => $val) {
                $checked = (isset($selectedValue) && $selectedValue == $key) ? ' checked' : '';
                $html .= '<label class="check">';
                $html .= '<input type="radio" class="checkbox" id="' . $field . '-' . $key . '" name="' . $field . '" value="' . $key . '"' . $checked . '>';
                $html .= $val;
                $html .= '</label>';
            }
        }
        return $html;
    }

    /**
     * Build checkbox list
     * @param string $field
     * @param AbstractModel[] $items
     * @param string $key
     * @param string $name
     * @param array $selectedValues
     * @param bool $isLocalized
     * @return string
     */
    public static function buildCheckboxList($field, array $items, $key, $name, $selectedValues = null, $isLocalized = false)
    {
        $pk = 'get' . ucfirst($key);
        $na = 'get' . ucfirst($name);
        $html = '';
        foreach ($items as $item) {
            $checked = (isset($selectedValues) && in_array($item->$pk(), $selectedValues)) ? ' checked' : '';
            $highlight = $checked != '' ? ' highlight' : '';
            $html .= '<label class="check' . $highlight . '">';
            $html .= '<input type="checkbox" class="checkbox" id="' . $field . '-' . $item->$pk() . '" name="' . $field . '[]" value="' . $item->$pk() . '"' . $checked . '>';
            $html .= ($isLocalized ? Lang::lang($item->$na()) : $item->$na());
            $html .= '</label>';
        }
        return $html;
    }

    /**
     * Build a static checkbox
     * @param string $field
     * @param array $list
     * @param array $selectedValues
     * @return string
     */
    public static function buildCheckbox($field, array $list, $selectedValues = null)
    {
        $html = '';
        if (is_array($list)) {
            foreach ($list as $key => $val) {
                $checked = (isset($selectedValues) && in_array($key, $selectedValues)) ? ' checked' : '';
                $highlight = $checked != '' ? ' highlight' : '';
                $html .= '<label class="check' . $highlight . '">';
                $html .= '<input id="' . $field . '-' . $key . '" type="checkbox" class="checkbox" name="' . $field . '[]" value="' . $key . '"' . $checked . '>';
                $html .= $val;
                $html .= '</label>';
            }
        }
        return $html;
    }

    /**
     * Build a boolean checkbox
     * @param string $field
     * @param string $name
     * @param bool $isChecked
     * @return string
     */
    public static function buildBoolean($field, $name, $isChecked = false)
    {
        $checked = ($isChecked) ? ' checked' : '';
        $highlight = $checked != '' ? ' highlight' : '';
        $html = '<input type="hidden" name="' . $field . '" value="0">';
        $html .= '<label class="check' . $highlight . '">';
        $html .= '<input type="checkbox" class="checkbox" id="' . $field . '"  name="' . $field . '" value="1"' . $checked . '>';
        $html .= Lang::lang($name) . '</label>';
        return $html;
    }

    /**
     * Build a media field
     * @param string $field
     * @param \Rebond\Models\Core\Media $media
     * @return string
     */
    public static function buildMedia($field, \Rebond\Models\Core\Media $media)
    {
        $html = '<div class="input-media" data-field="' . $field . '">';
        $html .= '<img src="' . Media::showFromModel($media) . '" alt="media" />';
        $html .= '<input type="text" class="input" id="media-' . $field . '" disabled value="' . $media->getTitle() . '">';
        $html .= '<a href="#" class="rb-btn media-select" id="media-select-' . $field . '" data-field="'. $field . '">' . Lang::lang('select') . '</a>';
        $html .= '<a href="#" class="rb-btn media-clear" id="media-clear-' . $field . '" data-field="' . $field . '">' . Lang::lang('clear') . '</a>';
        $html .= '<input type="hidden" id="' . $field . '" name="' . $field . '" value="' . $media->getId() . '">';
        $html .= '</div>';
        return $html;
    }
}
