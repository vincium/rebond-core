<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services\Core;

use Rebond\App;
use Symfony\Component\Yaml\Parser;

class SiteService
{
    public static function renderGoogleAnalytics($ga)
    {
        if ($ga == '') {
            return '';
        }

        $html = "<script>
        (function(i,s,o,g,r,a,m) {i['GoogleAnalyticsObject']=r;i[r]=i[r]||function() {
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{$ga}', 'auto');
        ga('send', 'pageview');
        </script>";
        return $html;
    }

    public static function updateIsCms($isCms)
    {
        $app = App::instance();
        $sitePath = $app->getConfig('site');
        $indexPath = $app->getPath('rebond-index');
        copy($indexPath . ($isCms ? 'cms.php' : 'mvc.php'), $sitePath . 'index.php');
        return true;
    }

    public static function resetFiles()
    {
        $app = App::instance();
        $sitePath = $app->getConfig('site');
        $adminPath = $app->getConfig('admin');
        $indexPath = $app->getPath('rebond-index');
        copy($indexPath . 'site_install.php', $sitePath . 'index.php');
        copy($indexPath . 'admin_install.php', $adminPath . 'index.php');
        return true;
    }
}
