<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\AppStatus;
use Rebond\Enums\Core\Code;

class Lang
{
    public static function lower($value, array $params = [])
    {
        return mb_strtolower(self::locale($value, $params), 'UTF-8');
    }

    public static function capital($value, array $params = [])
    {
        return mb_strtoupper(self::locale($value, $params), 'UTF-8');
    }

    public static function lang($value, array $params = [])
    {
        $text = self::locale($value, $params);
        return mb_strtoupper(mb_substr($text, 0, 1, 'UTF-8'), 'UTF-8')
            . mb_substr($text, 1, null, 'UTF-8');
    }

    public static function locale($value, array $params = [])
    {
        if ($value == '') {
            return $value;
        }

        if (is_numeric($value)) {
            return $value;
        }

        $app = App::instance();

        if ($app->getAppLevel() == AppLevel::STANDALONE) {
            return trim($value) . (count($params) > 0 ? ' (' . implode(', ', $params) . ')' : '');
        }

        $lang = ucfirst($app->getCurrentLang());
        $folder = ($app->getAppLevel() == AppLevel::WWW) ? 'Own' : 'Rebond';

        $text = self::getText($folder, $lang, $value);

        // try to look in admin localization if not found in front
        if (!isset($text)) {
            $otherFolder = ($folder != 'Rebond') ? 'Rebond' : 'Own';
            $text = self::getText($otherFolder, $lang, $value);
        }

        if (!isset($text)) {
            Log::log(Code::TRANSLATION_NOT_FOUND, 'lang: ' . $lang . ', site: ' . $app->getAppLevel() . ', value: ' . $value,
                __FILE__, __LINE__);
            return trim($value) . '~';
        }

        $params = is_array($params) ? $params : [$params];
        $count = count($params);
        for ($i = 0; $i < $count; $i++) {
            $text = str_replace('{' . $i . '}', $params[$i], $text);
        }

        $text = trim(preg_replace('/{\d+}/', '', $text));
        return $text;
    }

    /**
     * @param $folder
     * @param $lang
     * @param $value
     * @return string|null
     */
    private static function getText($folder, $lang, $value)
    {
        $cachedClass = '\Generated\Cache\\' . $folder . $lang;
        if (class_exists($cachedClass, true)) {
            return $cachedClass::get($value);
        }
        return null;
    }
}
