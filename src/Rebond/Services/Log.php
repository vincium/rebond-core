<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Models\DateTime;

class Log
{
    public static function debug($message)
    {
        self::log(Code::DEBUG, $message, '', '');
    }

    /**
     * @param int $code
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $trace = []
     * @return array
     */
    public static function log($code, $message, $file, $line, array $trace = [])
    {
        $app = App::instance();
        $date = new DateTime();

        $level = Code::findLevel($code);

        $logFile = $app->getPath('own-log-file') . $date->format('Y-m') . '-' . $level . '.json';
        $file = str_replace($app->getFullPath(), '/', str_replace('\\', '/', $file));

        $log = [
            'date' => $date->format('Y-m-d H:i:s'),
            'userId' => $app->getUserId(),
            'referrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            'code' => $code,
            'message' => str_replace($app->getFullPath(), '/', $message),
            'file' => $file,
            'line' => $line,
            'ip' => Converter::stringKey('REMOTE_ADDR', $_SERVER, ''),
            'stack' => !empty($trace) ? $trace : debug_backtrace()
        ];

        $logs = File::readJson($logFile);
        $logs[] = $log;
        File::saveJson($logFile, $logs);
        return $log;
    }
}
