<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\Models\DateTime;

class Converter
{
    /**
     * Set a default value if not set
     * @param object $value
     * @param object $default
     * @return object
     */
    public static function toDefault($value, $default)
    {
        if (!isset($value)) {
            return $default;
        }
        return $value;
    }

    /**
     * Convert a string to snake case
     * @param string $text
     * @param string $separator = '_'
     * @return string
     */
    public static function toSnakeCase($text, $separator = '_')
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $text, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = ($match == strtoupper($match)) ? strtolower($match) : lcfirst($match);
        }
        return implode($separator, $ret);
    }

    /**
     * Convert a string to camel case
     * @param string $text
     * @param bool $capital = false
     * @return string
     */
    public static function toCamelCase($text, $capital = false)
    {
        $temp = preg_split('/[\s_-]+/', strtolower($text));
        $count = count($temp);
        if ($count >= 2) {
            for ($i = ($capital) ? 0 : 1; $i < $count; $i++) {
                $temp[$i] = ucfirst($temp[$i]);
            }
            return implode($temp);
        }

        return ($capital) ? ucfirst(strtolower($text)) : strtolower($text);
    }

    public static function replace($text, $separator = '-')
    {
        $temp = preg_split('/[\s_-]+/', strtolower($text));
        return implode($separator, $temp);
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param int $default = null
     * @return int|null
     */
    public static function intKey($key, $method = 'get', $default = null)
    {
        $value = self::method($method);
        return isset($value[$key]) ? (int)$value[$key] : $default;
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param string $default = null
     * @return null|string
     */
    public static function stringKey($key, $method = 'get', $default = null)
    {
        $value = self::method($method);
        return isset($value[$key]) ? (string)$value[$key] : $default;
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param float $default = null
     * @return float|null
     */
    public static function floatKey($key, $method = 'get', $default = null)
    {
        $value = self::method($method);
        return isset($value[$key]) ? (float)$value[$key] : $default;
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param bool $default = null
     * @return bool|null
     */
    public static function boolKey($key, $method = 'get', $default = null)
    {
        $value = self::method($method);
        return isset($value[$key]) ? (bool)$value[$key] : $default;
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param array $default = null
     * @return array|null
     */
    public static function arrayKey($key, $method = 'get', $default = null)
    {
        $value = self::method($method);
        return isset($value[$key]) ? (array)$value[$key] : $default;
    }

    /**
     * @param string|int $key
     * @param string|array $method
     * @param DateTime $default = null
     * @return null|DateTime
     */
    public static function dateKey($key, $method = 'get', $default = null)
    {
        $datetime = self::method($method);

        if (!isset($datetime[$key]) && !isset($datetime[$key . '-h']) && !isset($datetime[$key . '-m'])) {
            return $default;
        }

        $date = (isset($datetime[$key])) ? $datetime[$key] : (new DateTime())->format('Y-m-d');

        $time = (isset($datetime[$key . '-h']) && isset($datetime[$key . '-m']))
            ? $datetime[$key . '-h'] . ':' . $datetime[$key . '-m'] . ':00'
            : '00:00:00';

        return new DateTime($date . ' ' . $time);
    }

    /**
     * @param string|array $method
     * @return array
     */
    private static function method($method)
    {
        if (is_array($method)) {
            return $method;
        }
        if ($method == 'get') {
            return $_GET;
        }
        if ($method == 'post') {
            return $_POST;
        }
        if ($method == 'cookie') {
            return $_COOKIE;
        }
        if ($method == 'session') {
            return $_SESSION;
        }
        return [];
    }
}
