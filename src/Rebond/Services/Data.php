<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;


use Rebond\App;
use Rebond\Enums\Core\Result;
use Rebond\Enums\Core\Status;

class Data
{
    /**
     * Run SQL script
     * @param string $sqlFile
     * @return array
     */
    public static function runScript($sqlFile)
    {
        $data = new \Rebond\Repository\Data();
        $sql = file_get_contents($sqlFile);
        $queries = explode(';' . PHP_EOL, $sql);
        unset($sql);
        $queries = preg_replace('/\s/', ' ', $queries);

        $result = [];
        $result['status'] = Result::SUCCESS;
        $result['message'] = '';
        foreach ($queries as $query) {
            if (trim($query) == '') {
                continue;
            }
            try {
                $data->pdo()->query($query);
            } catch (\PDOException $ex) {
                $result['status'] = Result::ERROR;
                $result['message'] = 'could not run query: ' . $ex->getMessage() . ' [' . $query . ']';
                return $result;
            }
        }
        return $result;
    }

    /**
     * Backup DB
     * @param null $type
     */
    public static function backup($type = null)
    {
        $app = App::instance();
        $data = new \Rebond\Repository\Data($app);
        $backupPath = $app->getPath('own-backup-file');

        $tables = [];
        $res = $data->pdo()->query('SHOW TABLES');
        while ($row = $res->fetch(\PDO::FETCH_NUM)) {
            $tables[] = $row[0];
        }

        $sqlFile = '';

        // cycle through
        foreach ($tables as $table) {
            $stmt = $data->pdo()->query('SELECT * FROM ' . $table);

            $sqlFile .= 'DROP TABLE IF EXISTS ' . $table . ";\n\n";

            $result2 = $data->pdo()->query('SHOW CREATE TABLE ' . $table);
            $row2 = $result2->fetch(\PDO::FETCH_NUM);
            $sqlFile .= $row2[1] . ";\n\n";

            if (in_array($table, ['core_log'])) {
                $sqlFile .= 'TRUNCATE TABLE ' . $table . ";\n\n";
                continue;
            }

            $sqlFile .= self::backupData($table, $stmt);
        }

        // save file
        $type = isset($type) ? $type . '-' : '';
        $backup = $backupPath . 'db-' . $type . date('Ymd-His') . '.sql';
        File::save($backup, $sqlFile);
    }

    /**
     * @param string $table
     * @param \PDOStatement $stmt
     * @return string
     */
    public static function backupData($table, \PDOStatement $stmt)
    {
        $script = '';
        $num_fields = $stmt->columnCount();
        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
                $script .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    if (isset($row[$j])) {
                        $script .= "'" . $row[$j] . "'";
                    } else {
                        $script .= "''";
                    }
                    if ($j < ($num_fields - 1)) {
                        $script .= ',';
                    }
                }
                $script .= ");\n";
            }
        }
        $script .= "\n\n";
        return $script;
    }

    /**
     * @param string $restore
     * @return array
     */
    public static function restore($restore)
    {
        $app = App::instance();
        $result = [];
        try {
            $result = self::runScript($app->getPath('own-backup-file') . $restore);
        } catch (\Exception $ex) {
            $result['status'] = Result::ERROR;
            $result['message'] = $ex->getMessage();
        }
        return $result;
    }

    /** @return array */
    public static function reset()
    {
        self::backup('auto');
        $app = App::instance();
        $data = new \Rebond\Repository\Data($app);
        $dbName = $app->getConfig('db-name');

        $result = [];
        $result['status'] = Result::SUCCESS;
        $result['message'] = '';

        $sql = 'SHOW TABLES
            WHERE Tables_in_' . $dbName . ' LIKE \'app_%\'
            OR Tables_in_' . $dbName . ' LIKE \'bus_%\'
            OR Tables_in_' . $dbName . ' LIKE \'core_%\'
            OR Tables_in_' . $dbName . ' LIKE \'cms_%\'';

        $res = $data->pdo()->query($sql);
        while ($row = $res->fetch(\PDO::FETCH_NUM)) {
            try {
                $sql = 'DROP TABLE IF EXISTS ' . $row[0];
                $data->pdo()->query($sql);
            } catch (\PDOException $ex) {
                $result['status'] = false;
                $result['message'] = 'could not run query: ' . $ex->getMessage() . ' [' . $sql . ']';
                return $result;
            }
        }
        return $result;
    }
}