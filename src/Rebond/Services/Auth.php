<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Models\Core\User;
use Rebond\Repository\Core\UserRepository;

class Auth
{
    public static function isAuth(User $user)
    {
        return $user->getId() > 0;
    }

    /**
     * Is user authorized to access a resource in the admin site
     * @param User $user
     * @param string $permission = null
     * @param bool $show = true
     * @param string $redirect = null
     * @return bool
     */
    public static function isAdminAuthorized(User $user, $permission = null, $show = false, $redirect = null)
    {
        if (!self::isAuth($user)) {
            if (isset($redirect)) {
                Session::redirect($redirect);
            }
            return false;
        }

        $app = App::instance();

        if (!$user->getIsAdmin()) {
            Session::redirect($app->getConfig('site-url'));
        }

        if (!isset($permission) || $permission == '' || $user->getIsDev()) {
            return true;
        }

        if ($user->hasAccess($permission, true)) {
            return true;
        }

        if ($show) {
            $perm = $app->isDebug() ? [$permission] : [];
            Session::set('adminError', Lang::lang('access_non_authorized', [$perm]));
        }
        Log::log(Code::NOT_ENOUGH_PRIVILEGE, Lang::lang('access_non_authorized', [$permission]), __FILE__, __LINE__);
        if (isset($redirect)) {
            Session::redirect($redirect);
        }
        return false;
    }

    /**
     * Is user authorized to access a resource
     * @param User $user
     * @param string $permission = null
     * @param bool $show = true
     * @param string $redirect = null
     * @return bool
     */
    public static function isAuthorized(User $user, $permission = null, $show = true, $redirect = null)
    {
        if (!isset($permission) || $permission == '') {
            return true;
        }

        if (!self::isAuth($user)) {
            if (isset($redirect)) {
                Session::redirect($redirect);
            }
            return false;
        }

        if ($user->hasAccess($permission, false)) {
            return true;
        }

        if ($show) {
            Session::add('siteError', Lang::lang('error_access', [$permission]));
        }
        Log::log(Code::NOT_ENOUGH_PRIVILEGE, Lang::lang('error_access', [$permission]), __FILE__, __LINE__);
        if (isset($redirect)) {
            Session::redirect($redirect);
        }
        return false;
    }
}
