<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Result;
use Rebond\Models\Field;

class Security
{
    const SALT = 'rebond';

    /**
     * Build a token for a form
     * @param string $unique
     * @return string
     */
    public static function buildToken($unique)
    {
        $token = Session::get('token', null);
        if (!isset($token)) {
            $token = self::generateToken();
            Session::set('token', $token);
        }
        return '<input type="hidden" name="token' . $unique . '" id="token' . $unique . '" value="' . $token . '">';
    }

    /**
     * Validate a token from a form
     * @param string $unique
     * @param string $tokenPost
     * @return Field
     */
    public static function validateToken($unique, $tokenPost)
    {
        $vrf = new Field('token');

        if (Session::get('token') != $tokenPost) {
            $log = 'field: token ' . $unique . ', tokenPost: ' . $tokenPost;
            Log::log(Code::INVALID_TOKEN, $log, __FILE__, __LINE__);
            $vrf->setResult(Result::ERROR);
            $vrf->setMessage(Lang::lang('error_token_invalid'));
        }
        return $vrf;
    }
    
    /**
     * Generate a token
     * @return string
     */
    public static function generateToken()
    {
        return sha1(self::SALT . uniqid());
    }

    /**
     * Encrypt a value
     * @param string $value
     * @param string $salt = null
     * @return string
     */
    public static function encode($value, $salt = null)
    {
        return base64_encode(self::findSalt($salt) . $value);
    }

    /**
     * @param $value
     * @param string $salt = null
     * @return string
     */
    public static function decode($value, $salt = null)
    {
        $decrypted = base64_decode($value);
        return substr($decrypted, strlen(self::findSalt($salt)));
    }

    /**
     * Encrypt a password
     * @param string $value
     * @param string $salt = null
     * @return string
     */
    public static function encryptPassword($value, $salt = null)
    {
        return (!empty($value))
            ? password_hash(self::findSalt($salt) . $value, PASSWORD_DEFAULT)
            : '';
    }

    /**
     * Is password valid
     * @param string $value
     * @param string $encryptedValue
     * @param string $salt = null
     * @return bool
     */
    public static function isValidPassword($value, $encryptedValue, $salt = null)
    {
        return password_verify(self::findSalt($salt) . $value, $encryptedValue);
    }

    public static function getApiKey()
    {
        $headers = self::getAllHeaders();
        return isset($headers['X-Api-Key'])
            ? $headers['X-Api-Key']
            : null;
    }

    /**
     * Trim data
     * @param $data
     */
    public static function trim(&$data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                self::trim($data[$key]);
            }
        } else {
            $data = trim($data);
        }
    }

    /**
     * Find the secret key
     * @param string $salt = null
     * @return string
     */
    private static function findSalt($salt = null)
    {
        if (!isset($salt)) {
            $app = App::instance();
            $salt = ($app->getAppLevel() == AppLevel::STANDALONE) ? self::SALT : $app->getConfig('salt');
        }
        return $salt;
    }

    private static function getAllHeaders()
    {
        if (function_exists('getallheaders')) {
            return getallheaders();
        }

        if (!is_array($_SERVER)) {
            return [];
        }

        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
