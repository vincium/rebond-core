<?php
namespace Rebond\Gadgets;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\AbstractModel;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Log;
use Rebond\Services\Session;
use Rebond\Services\Template;

abstract class AbstractGadget
{
    /* @var App */
    protected $app;
    /* @var string */
    protected $moduleName;
    /* @var bool|null */
    protected $hasFilter;

    /**
     * @param App $app
     * @param string $moduleName
     */
    public function __construct(App $app, $moduleName)
    {
        $this->app = $app;
        $this->moduleName = $moduleName;
        $this->hasFilter = null;
    }

    /**
     * @param AbstractModel[] $items
     * @return string
     */
    public function renderCards(array $items)
    {
        $tpl = new Template(Template::MODULE, ['app', lcfirst($this->moduleName)]);
        $tpl->set('items', $items);
        $tpl->set('filter', $this->hasFilter());
        $tpl->set('url', $this->app->url());
        return $tpl->render('cards');
    }

    /**
     * @param int $contentGroup
     * @param AbstractModel $item
     * @return string
     */
    public function renderSingle($contentGroup, AbstractModel $item = null)
    {
        if (isset($item)) {
            $tpl = new Template(Template::MODULE, ['app', lcfirst($this->moduleName)]);
            $tpl->set('item', $item);
            $tpl->set('filter', $this->hasFilter());
            $tpl->set('url', $this->app->url());
            return $tpl->render('single');
        } else {
            Log::log(Code::CONTENT_NOT_FOUND, 'Module contentGroup: ' . $contentGroup, __FILE__, __LINE__);
            return '';
        }
    }

    public function renderEditor(AbstractForm $form)
    {
        $tpl = new Template(Template::MODULE, ['app', lcfirst($this->moduleName)]);

        $form->getModel()->loadModule();

        if (Form::isSubmitted('btnSend')) {
            if ($form->setFromPost()->validate()->isValid()) {
                $form->getModel()->save();
                return $tpl->render('editor-success');
            } else {
                Session::set('siteError', $form->getValidation()->getMessage());
            }
        }

        $tpl->set('item', $form);
        $tpl->set('filter', $this->hasFilter());
        return $tpl->render('editor');
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    protected function hasFilter()
    {
        if (!isset($this->hasFilter)) {
            $module = ModuleRepository::loadByName($this->moduleName);
            if (!isset($module)) {
                throw new \Exception($this->moduleName, Code::MODULE_NOT_FOUND);
            }
            $this->hasFilter = $module->getHasFilter();
        }
        return $this->hasFilter;
    }
}
