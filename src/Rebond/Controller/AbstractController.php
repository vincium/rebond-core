<?php
namespace Rebond\Controller;

use Rebond\App;
use Rebond\Enums\Core\ResponseFormat;
use Rebond\Enums\Core\Result;
use Rebond\Models\Core\User;
use Rebond\Services\Converter;
use Rebond\Services\Security;
use Rebond\Services\Template;

abstract class AbstractController
{
    protected $app;
    /* @var User */
    protected $signedUser;
    /* @var Template */
    protected $tplMaster;
    /* @var Template */
    protected $tplLayout;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->signedUser = $this->app->getUser();
    }

    protected abstract function setBaseTpl($format = ResponseFormat::HTML);

    protected function getFormat()
    {
        $apiKey = null !== Security::getApiKey()
            ? ResponseFormat::value(ResponseFormat::JSON)
            : ResponseFormat::value(ResponseFormat::HTML);

        $format = Converter::stringKey('format', 'get', $apiKey);
        $format = array_search(strtolower($format), ResponseFormat::toArray());
        return ($format !== false) ? $format : $apiKey;
    }

    /**
     * Render a response
     * @param string $master
     * @param array $masterData
     * @param string $layout
     * @param array $columns
     * @return string (json)
     */
    protected function response($master, array $masterData, $layout, array $columns)
    {
        $format = $this->getFormat();
        $this->setBaseTpl($format);

        if ($format != ResponseFormat::JSON || count($columns) > 1) {
            foreach ($columns as $column => $data) {
                if (is_array($data)) {
                    foreach ($data as $content) {
                        $this->tplLayout->add($column, $content);
                    }
                } else {
                    $this->tplLayout->set($column, $data);
                }
            }
        }

        if ($format == ResponseFormat::JSON) {
            $json = [
                'status' => Result::SUCCESS,
                'html' => (count($columns) == 1)
                    ? array_values($columns)[0]
                    : $this->tplLayout->render($layout)
                ];
            return json_encode($json);
        }

        foreach ($masterData as $key => $data) {
            if ($key == 'css') {
                if (is_array($data)) {
                    foreach ($data as $css) {
                        $this->tplMaster->addCss($css);
                    }
                } else {
                    $this->tplMaster->addCss($data);
                }
            } else if ($key == 'js') {
                if (is_array($data)) {
                    foreach ($data as $js) {
                        $this->tplMaster->addJs($js);
                    }
                } else {
                    $this->tplMaster->addJs($data);
                }
            } else {
                $this->tplMaster->set($key, $data);
            }
        }

        $this->tplMaster->set('layout', $this->tplLayout->render($layout));
        return $this->tplMaster->render($master);
    }
}
