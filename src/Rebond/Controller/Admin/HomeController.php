<?php
namespace Rebond\Controller\Admin;

use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Services\Auth;
use Rebond\Services\Lang;
use Rebond\Services\Menu;
use Rebond\Services\Template;

class HomeController extends BaseAdminController
{
    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('home'));
    }

    public function index()
    {
        Auth::isAdminAuthorized($this->signedUser, null, false, 'profile/sign-in');

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin']);
        $tplMain->set('title', $this->app->getSite()->getTitle());
        $tplMain->set('menu', Menu::buildHomeMenu($this->signedUser));

        // right
        $tplChangeLog = new Template(Template::ADMIN, ['admin', 'home']);
        $tplPending = new Template(Template::ADMIN, ['admin', 'home']);

        $pendingItems = [];

        if ($this->app->getSite()->getIsCms()) {
            $options = [];
            $options['where'][] = 'module.status = 1';
            $options['where'][] = 'module.has_content = 1';
            $modules = ModuleRepository::loadAll($options);
            if ($modules) {
                foreach ($modules as $module) {
                    $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
                    if (class_exists($appData)) {
                        $pendingItems = $appData::loadByVersion('pending');
                    }
                }
            }
        }

        $tplPending->set('items', $pendingItems);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('index'));
        $this->tplLayout->set('column2', $tplChangeLog->render('changelog'));
        $this->tplLayout->add('column2', $tplPending->render('pending'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-col'));
        return $this->tplMaster->render('tpl-default');
    }
}
