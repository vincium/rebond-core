<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\InstallStatus;
use Rebond\Enums\Core\Result;
use Rebond\Enums\Core\Status;
use Rebond\Forms\Core\UserForm;
use Rebond\Models\Core\User;
use Rebond\Repository\Core\SiteRepository;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Security;
use Rebond\Services\Session;
use Rebond\Services\Template;

class InstallController
{
    const TABLE_COUNT = 22;
    private $app;
    private $authenticated;
    private $step;
    private $info;
    private $tplMaster;
    private $tplLayout;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->authenticated = Session::int('auth');
        $this->step = InstallStatus::AUTH;
        $this->info = [];
        $this->tplMaster = new Template(Template::ADMIN, ['admin']);
        $this->tplLayout = new Template(Template::ADMIN, ['admin']);
    }

    public function index()
    {
        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'install']);

        $this->checkAuth();
        $this->checkConfig();
        $this->checkDb();
        $this->checkUser($tplMain);
        $this->checkLaunch();

        // menu
        $tplMenu = new Template(Template::ADMIN, ['admin', 'install']);
        $tplMenu->set('active', InstallStatus::value($this->step));

        // layout
        $this->tplLayout->set('column1', $tplMenu->render('menu'));
        $this->tplLayout->add('column1', implode($this->info));

        $this->tplLayout->add('column1', $tplMain->render(InstallStatus::value($this->step)));

        // master
        $this->tplMaster->set('notifications', Session::renderAdminResp());
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'install');

        $this->tplMaster->addCss('/node_modules/normalize-css/normalize.css');
        $this->tplMaster->addCss('/node_modules/components-font-awesome/css/font-awesome.min.css');
        $this->tplMaster->addCss('https://fonts.googleapis.com/css?family=Work+Sans');
        $this->tplMaster->addCss('/node_modules/rebond-assets/css/rebond.css');
        $this->tplMaster->addCss('/node_modules/rebond-assets-admin/css/rebond-admin.css');
        $this->tplMaster->addCss('/css/own.css');

        $this->tplMaster->addJs('/node_modules/jquery/dist/jquery.min.js');
        $this->tplMaster->addJs('/node_modules/rebond-assets/js/rebond.js');
        $this->tplMaster->addJs('/node_modules/rebond-assets-admin/js/rebond-admin.js');
        $this->tplMaster->addJs('/js/own.js');

        return $this->tplMaster->render('tpl-install');
    }

    /** @return bool */
    private function checkAuth()
    {
        if ($this->authenticated > 0) {
            $this->step = InstallStatus::CONFIG;
            return true;
        }

        $authFile = $this->app->getPath('own-file') . 'authentication.txt';

        if (!file_exists($authFile)) {
            $auth = Security::generateToken();
            File::save($authFile, $auth);
        } else {
            $auth = File::read($authFile);
        }

        $authPost = Converter::stringKey('auth', 'post');
        if (Form::isSubmitted('btnAuth')) {
            if ($auth == $authPost) {
                $this->authenticated = 1;
                Session::set('auth', $this->authenticated);
                unlink($authFile);
                $this->addInfo('You have been authenticated!', 'success');
                $this->step = InstallStatus::CONFIG;
                return true;
            }
            $this->addInfo('The identification number you entered is incorrect. Please try again.', 'error');
        }
        return false;
    }

    /** @return bool */
    private function checkConfig()
    {
        if ($this->authenticated == 0) {
            return false;
        }

        if ($this->authenticated == 2) {
            $this->step = InstallStatus::DB;
            return true;
        }
        if (Form::isSubmitted('btnConfig')) {
            $this->authenticated = 2;
            Session::set('auth', $this->authenticated);
            $this->step = InstallStatus::DB;
            return true;
        }
        return false;
    }

    /** @return bool */
    private function checkDb()
    {
        if ($this->step != InstallStatus::DB) {
            return false;
        }

        $db = new Data($this->app);

        $tableListSql = "SELECT COUNT(table_name)
            FROM information_schema.tables
            WHERE table_schema = '" . $this->app->getConfig('db-name') . "'
            AND (table_name LIKE 'app_%' OR table_name LIKE 'cms_%' OR table_name LIKE 'core_%')";
        $userTableSql = 'SHOW TABLES LIKE "core_user"';

        $tableCount = $db->count($tableListSql);
        $userTable = $db->select($userTableSql);

        if ($tableCount == self::TABLE_COUNT && $userTable->rowCount() == 1) {
            $this->step = InstallStatus::USER;
            return true;
        }

        if ($tableCount != 0) {
            $this->addInfo('Your database contains ' . $tableCount . ' table(s).
                A fresh install of Rebond should contain ' . self::TABLE_COUNT . ' tables.
                Please check your database name to make sure that you want to install Rebond in this database.', 'error');

            if (Form::isSubmitted('btnSkipDb')) {
                $this->addInfo('The database install was skipped.', 'success');
                $this->step = InstallStatus::USER;
                return true;
            }
            return false;
        }

        if (Form::isSubmitted('btnInstallDb')) {
            $scripts = File::getFiles($this->app->getPath('rebond-install-file'));
            sort($scripts);

            foreach ($scripts as $script) {
                $result = \Rebond\Services\Data::runScript($this->app->getPath('rebond-install-file') . $script);
                if ($result['status'] == Result::SUCCESS) {
                    $this->addInfo($script . '... success.');
                } else {
                    $this->addInfo($script . '... failed: ' . $result['message'], 'error');
                    break;
                }
            }

            $tableCount = $db->count($tableListSql);
            if ($tableCount != self::TABLE_COUNT) {
                $this->addInfo('The database has NOT been installed correctly. Please try to reinstall the database.', 'error');
                return false;
            } else {
                $this->addInfo('The database has been installed successfully.', 'success');
                $this->step = InstallStatus::USER;
                return true;
            }
        }
        return false;
    }

    /** @return bool */
    private function checkUser(Template $tplMain)
    {
        if ($this->step != InstallStatus::USER) {
            return false;
        }

        if (UserRepository::count() >= 1) {
            $this->step = InstallStatus::LAUNCH;
            return true;
        }

        $user = new User();
        $userForm = new UserForm($user);
        $tplMain->set('form', $userForm);

        if (Form::isSubmitted('btnCreateUser')) {
            if ($userForm->setFromPost()->validate()->isValid()) {
                $user->setPassword(Security::encryptPassword($user->getPassword()));
                $user->setIsAdmin(true);
                $user->setIsDev(true);
                $user->setStatus(Status::ACTIVE);
                $user->save();
                $this->step = InstallStatus::LAUNCH;
                return true;
            } else {
                $this->addInfo($userForm->getValidation()->getMessage(), 'error');
            }
        }
        return false;
    }

    /** @return bool */
    private function checkLaunch()
    {
        $isMvc = Converter::boolKey('site', 'post', true);

        if (!Form::isSubmitted('btnLaunch')) {
            return false;
        }

        $site = SiteRepository::loadById(1);
        $site->setIsCms(!$isMvc);
        $site->save();

        $adminPath = $this->app->getConfig('admin');
        $sitePath = $this->app->getConfig('site');
        $indexPath = $this->app->getPath('rebond-index');
        copy($indexPath . 'admin.php', $adminPath . 'index.php');
        copy($indexPath . 'cms.php', $sitePath . 'index.php');
        \Rebond\Services\Data::backup('launch');
        Session::kill('auth');
        Session::set('signedUser', 1);
        Session::adminSuccess('rebond_welcome', '/configuration/site');
        return true;
    }

    /**
     * @param $text
     * @param $type
     */
    private function addInfo($text, $type = 'info')
    {
        switch($type) {
            case 'error':
                $format = '<div class="status-error">%s</div>';
                break;
            case 'success':
                $format = '<div class="status-success">%s</div>';
                break;
            case 'ok':
                $format = '<div class="status-info">%s</div>';
                break;
            default:
                $format = '<div class="status-info">%s</div>';
        }
        $this->info[] = sprintf($format, $text);
    }
}
