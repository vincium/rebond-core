<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Forms\Core\UserForm;
use Rebond\Models\Core\UserRole;
use Rebond\Repository\Core\RoleRepository;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Core\UserRoleRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserSecurityService;
use Rebond\Services\Core\UserService;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class UserController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.user', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('user'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', Template::loading());

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'userView');
        return $this->tplMaster->render('tpl-default');
    }

    public function edit()
    {
        $id = Converter::intKey('id');
        $view = Converter::boolKey('view');
        $user = UserRepository::loadById($id, true);
        $newUser = $user->getId() == 0;

        $userForm = new UserForm($user);
        $withPassword = true;
        $sendMail = true;

        if (Form::isSubmitted() && UserService::isEditable($this->signedUser, $id)) {
            $withPassword = Converter::boolKey('withPassword', 'post');
            $sendMail = Converter::boolKey('sendMail', 'post');
            if (UserService::createOrEdit($userForm, $withPassword, $sendMail)) {
                Session::allSuccess('saved', '/user');
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);

        // layout
        if (!$view
            && Auth::isAdminAuthorized($this->signedUser, 'admin.user.edit')
            && UserService::isEditable($this->signedUser, $id)
        ) {
            $tplMain->set('canEditIsDev', $this->signedUser->getIsDev());
            $tplMain->set('item', $userForm);
            $tplMain->set('withPassword', $withPassword);
            $tplMain->set('sendMail', $sendMail);
            $this->tplLayout->set('column1', $tplMain->render($newUser ? 'register' : 'editor'));
        } else {
            $tplMain->set('item', $user);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'userEdit');
        return $this->tplMaster->render('tpl-default');
    }

    public function changePassword()
    {
        Auth::isAdminAuthorized($this->signedUser, 'admin.user.password', true, '/user');

        // check
        $id = Converter::intKey('id');

        $user = UserRepository::loadById($id);
        if (!isset($user)) {
            Session::adminError('item_not_found', '/user', [Lang::lang('user'), $id]);
        }

        $userForm = new UserForm($user);

        // action
        if (Form::isSubmitted()) {
            if ($userForm->changePassword(false, false)) {
                Session::allSuccess('password_changed', '/user');
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tplMain->set('item', $userForm);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('editor-password-admin'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'userPassword');
        return $this->tplMaster->render('tpl-default');
    }

    public function userRoles()
    {
        // check
        $id = Converter::intKey('id');
        $roleIds = Converter::arrayKey('role', 'post');

        $user = UserRepository::loadById($id);
        if (!isset($user)) {
            Session::adminError('item_not_found', '/user', [Lang::lang('user'), $id]);
        }

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.user.role', true, '/user/user-roles?id=' . $id);
            UserRoleRepository::deleteByUserId($user->getId());
            $newRoles = [];
            if (isset($roleIds)) {
                foreach ($roleIds as $roleId) {
                    $userRole = new UserRole();
                    $userRole->setUserId($user->getId());
                    $userRole->setRoleId($roleId);
                    $newRoles[] = $userRole;
                }
            }
            UserRoleRepository::saveAll($newRoles);
            Session::adminSuccess('saved', '/user');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.user.role', false)) {
            $tplMain->set('item', new UserForm($user));
            $this->tplLayout->set('column1', $tplMain->render('editor-role'));
        } else {
            $allRoles = RoleRepository::loadAll();
            $roles = RoleRepository::loadByUserId($user->getId());
            $tplMain->set('allRoles', $allRoles);
            $tplMain->set('roles', $roles);
            $this->tplLayout->set('column1', $tplMain->render('view-role'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function security()
    {
        $id = Converter::intKey('id');
        $user = UserRepository::loadById($id);

        if (!isset($user)) {
            Session::adminError('item_not_found', '/user', [Lang::lang('user'), $id]);
        }

        $items = UserSecurityService::getAllSecureByUserId($id);

        $btnNewKey = Form::isSubmitted('btnNewKey');
        $btnExtendKey = Form::isSubmitted('btnExtendKey');
        if ($btnNewKey || $btnExtendKey) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.user.security', true, '/user/security?id=' . $id);
            if (UserSecurityService::renewApiKey($id, $items, $btnNewKey, $btnExtendKey)) {
                Session::adminSuccess('saved', '/user/security?id=' . $id);
            }
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'userSecurity']);
        $tplMain->set('items', $items);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'userSecurity');
        return $this->tplMaster->render('tpl-default');
    }
}
