<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\Result;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Repository\Core\FeedbackRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\SiteService;
use Rebond\Models\DateTime;
use Rebond\Services\Data;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Generator;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;
use Rebond\Test\Launch;

class DevController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.dev', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('developer'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // main
        $tplDefault = new Template(Template::ADMIN, ['admin', 'dev']);

        // layout
        $this->tplLayout->set('column1', $tplDefault->render('index'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function generator()
    {
        // check
        $backup = Converter::boolKey('backup', 'post', true);
        $generateTpl = Converter::boolKey('generateTpl', 'post', true);
        $package = Converter::stringKey('package', 'post');

        $result = $this->generate($package, $generateTpl, $backup);

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplMain->set('package', $package);
        $tplMain->set('message', $result['message']);
        $tplMain->set('success', $result['success']);
        $tplMain->set('errorCount', $result['errors']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('generator'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    /**
     * @param string|null $package
     * @param bool $generateTpl
     * @param bool $backup
     * @return array
     */
    public function generate($package, $generateTpl, $backup, $checkAuth = true)
    {
        if ($checkAuth) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.dev', true, '/');
        }

        if (!isset($package)) {
            return [
                'success' => false,
                'message' => '',
                'errors' => '',
            ];
        }
        // backup database first
        if ($backup) {
            try {
                Data::backup('auto');
            } catch (\Exception $ex) {
                Session::set('adminError', $ex->getMessage());
            }
        }
        try {
            $generator = new Generator($this->app, $package, $generateTpl);
            $generator->run();
            return [
                'success' => true,
                'message' => $generator->getInfo(),
                'errors' => $generator->getErrors(),
            ];

        } catch (\Exception $ex) {
            return [
                'success' => false,
                'message' => $ex->getMessage(),
                'errors' => '',
            ];
        }
    }

    public function generatorEdit()
    {
        // check
        $model = Converter::stringKey('g');
        $fileContent = Converter::stringKey('fileContent', 'post');

        if (!in_array($model, ['app', 'bus', 'cms', 'core'])) {
            Session::adminError('item_not_found', '/dev/generator', [Lang::lang('model'), $model]);
        }

        $namespace = in_array($model, ['core', 'cms'])
            ? $this->app->getPath('rebond-model-file')
            : $this->app->getPath('own-model-file');
        $filePath = $namespace . $model . '.yaml';

        if (!file_exists($filePath)) {
            Session::adminError('error_file_not_found', '/dev/generator', [$filePath]);
        }

        // action
        if (Form::isSubmitted()) {
            file_put_contents($filePath, $fileContent);
            Session::adminSuccess('saved', '/dev/generator');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplMain->set('model', $model);
        $tplMain->set('filePath', $filePath);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('model-form'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'devModel');
        $this->tplMaster->addCss('/node_modules/codemirror/lib/codemirror.css');
        $this->tplMaster->addJs('/node_modules/codemirror/lib/codemirror.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/yaml/yaml.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function database()
    {
        // check
        $backup = Converter::stringKey('btnBackup', 'post');
        $restore = Converter::stringKey('btnRestore', 'post');

        $backupPath = $this->app->getPath('own-backup-file');

        // action
        if (isset($backup)) {
            try {
                Data::backup();
                Session::set('adminSuccess', Lang::lang('backup_created'));
            } catch (\Exception $ex) {
                Session::set('adminError', $ex->getMessage());
            }
        }
        if (isset($restore)) {
            $file = Converter::stringKey('file', 'post');
            $filePath = $backupPath . $file;
            if (!file_exists($filePath)) {
                Session::set('adminError', Lang::lang('error_file_not_found', [$filePath]));
            } else {
                $result = Data::restore($file);
                if ($result['status'] == Result::SUCCESS) {
                    Session::set('adminSuccess', Lang::lang('backup_restored'));
                } else {
                    Session::set('adminError', $result['message']);
                }
            }
        }

        // list backups
        $files = File::getFiles($backupPath);
        $files = array_reverse($files);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplFilter->set('count', count($files));

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplMain->set('files', $files);
        $tplMain->set('path', $backupPath);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('db-filter'));
        $this->tplLayout->set('column2', $tplMain->render('db'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function download()
    {
        $backup = Converter::stringKey('backup');

        $path = isset($backup)
            ? $this->app->getPath('own-backup-file') . $backup
            : null;

        if (!file_exists($path)) {
            return;
        }

        if ($fd = fopen($path, 'r')) {
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename="' . $backup . '"');
            header('Content-length: ' . filesize($path));
            header('Cache-control: private');
            while (!feof($fd)) {
                $buffer = fread($fd, 2048);
                echo $buffer;
            }
        }
        fclose($fd);
    }

    public function feedback()
    {
        $options = [];
        $options['where'][] = 'feedback.status IN (0,1)';
        $options['order'][] = 'feedback.created_date DESC';
        $feedbacks = FeedbackRepository::loadAll($options);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'feedback']);
        $tplFilter->set('count', count($feedbacks));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'feedback']);
        $tplMain->set('items', $feedbacks);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function feedbackView()
    {
        // check
        $id = Converter::intKey('id');

        $feedback = FeedbackRepository::loadById($id);
        if (!isset($feedback)) {
            Session::redirect('/dev/feedback');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'feedback']);
        $tplMain->set('item', $feedback);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('view'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function test()
    {
        // check
        $result = '';
        $run = Converter::boolKey('run');

        if ($run) {
            $test = new Launch();
            $result = $test->start();
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplMain->set('result', $result);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('test'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function bin()
    {
        // check
        $result = '';
        $empty = Converter::boolKey('empty');
        $contentItems = [];

        // content items
        $options = [];
        $options['where'][] = 'module.has_content = 1';
        $modules = ModuleRepository::loadAll($options);

        $options = [];
        $options['where'][] = ['content.version IN (?)', [4, 5]];
        $options['order'][] = 'content.modified_date';
        foreach ($modules as $module) {
            $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
            $items = $appData::loadAll($options);
            if (count($items) > 0) {
                if ($empty) {
                    foreach ($items as $item) {
                        $item->delete();
                    }
                } else {
                    $contentItems[$module->getTitle()] = $items;
                }
            }
        }

        // CSS

        $folderPath = $this->app->getConfig('site') . '/css/skin/';
        $skins = File::getFolders($folderPath);

        $cssFiles = [];
        foreach ($skins as $skin) {
            $cssFiles[$skin] = [];
            $filePath = $folderPath . $skin . '/';
            $files = File::getFiles($filePath);
            foreach ($files as $file) {
                if (File::getExtension($file) == 'css') {
                    continue;
                }
                if ($empty) {
                    File::delete($filePath, $file);
                } else {
                    $modifiedDate = new DateTime();
                    $modifiedDate->setTimestamp(filemtime($filePath . $file));
                    $cssFiles[$skin][] = ['name' => $file, 'date' => $modifiedDate->format()];
                }
            }
        }

        // Main Templates

        $viewPath = $this->app->getPath('own-view');
        $mainTemplates = File::getViews(false, false);

        $mainTpl = [];
        foreach ($mainTemplates as $folder => $templates) {
            foreach ($templates as $template) {
                if ($empty) {
                    File::delete($viewPath . $folder . '/', $template);
                } else {
                    $modifiedDate = new DateTime();
                    $modifiedDate->setTimestamp(filemtime($viewPath . $folder . '/' . $template));
                    $mainTpl[] = ['folder' => $folder, 'name' => $template, 'date' => $modifiedDate->format()];
                }
            }
        }

        // App Templates

        $viewPath = $this->app->getPath('own-view') . 'App/';
        $appTemplates = File::getViews(true, false);

        $appTpl = [];
        foreach ($appTemplates as $module => $templates) {
            $appTpl[$module] = [];
            foreach ($templates as $template) {
                if ($empty) {
                    File::delete($viewPath . $module, $template);
                } else {
                    $modifiedDate = new DateTime();
                    $modifiedDate->setTimestamp(filemtime($viewPath . $module . '/' . $template));
                    $appTpl[$module][] = ['name' => $template, 'date' => $modifiedDate->format()];
                }
            }
        }

        if ($empty) {
            $result = '<p class="status-success">' . Lang::lang('bin_emptied') . '</p>';
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);
        $tplMain->set('contentItems', $contentItems);
        $tplMain->set('cssFiles', $cssFiles);
        $tplMain->set('mainTpl', $mainTpl);
        $tplMain->set('appTpl', $appTpl);
        $tplMain->set('result', $result);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('bin'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }

    public function reinitialize()
    {
        if (Form::isSubmitted('btnFiles')) {
            if (!Converter::boolKey('files', 'post', false)) {
                Session::adminError('confirm_must', '/dev/reinitialize');
            }

            SiteService::resetFiles();
            Session::adminSuccess('reset_success', '/');
        }

        if (Form::isSubmitted('btnRestore')) {
            if (!Converter::boolKey('restore', 'post', false)) {
                Session::adminError('confirm_must', '/dev/reinitialize');
            }

            Data::backup('reset');

            $files = File::getFiles($this->app->getPath('own-backup-file'));
            $launchFile = null;
            foreach ($files as $file) {
                if (stripos($file, 'launch') !== false) {
                    $launchFile = $file;
                    break;
                }
            }

            if (!isset($launchFile)) {
                Session::adminError('db_launch_not_found', '/dev/reinitialize');
            }

            $result = Data::restore($launchFile);
            if ($result['status'] == Result::ERROR) {
                Session::setAndRedirect('adminError', $result['message'], '/dev/reinitialize');
            }
            SiteService::resetFiles();
            Session::adminSuccess('reset_success', '/');
        }

        if (Form::isSubmitted('btnFull')) {
            if (!Converter::boolKey('full', 'post', false)) {
                Session::adminError('confirm_must', '/dev/reinitialize');
            }

            $result = Data::reset();
            if ($result['status'] == Result::ERROR) {
                Session::setAndRedirect('adminError', $result['message'], '/dev/reinitialize');
            }

            SiteService::resetFiles();
            Session::adminSuccess('reset_success', '/');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'dev']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('reinitialize'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'dev');
        return $this->tplMaster->render('tpl-default');
    }
}
