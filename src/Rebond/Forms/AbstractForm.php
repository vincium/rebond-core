<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Forms;

use Rebond\Models\AbstractModel;
use Rebond\Models\FormValidator;
use Rebond\Services\Form;
use Rebond\Services\Security;

abstract class AbstractForm
{
    /* @var AbstractModel */
    protected $model;
    /* @var string */
    protected $token;
    /* @var string */
    protected $unique;
    /* @var FormValidator */
    protected $validation;

    public function __construct(AbstractModel $model, $unique)
    {
        $this->model = $model;
        $this->token = null;
        $this->unique = $unique;
        $this->validation = new FormValidator();
    }

    /** @return AbstractForm */
    abstract function setFromPost();

    /** @return FormValidator */
    abstract function validate();

    /**
     * Set Model
     * @param AbstractModel $model
     */
    public function setModel(AbstractModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get Model
     * @return AbstractModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set Validation
     * @param FormValidator $validation
     */
    public function setValidation(FormValidator $validation)
    {
        $this->validation = $validation;
    }

    /**
     * Get Validation
     * @return FormValidator
     */
    public function getValidation()
    {
        if (!isset($this->validation)) {
            $this->validation = new FormValidator();
        }
        return $this->validation;
    }

    // token methods

    public function buildToken()
    {
        return Security::buildToken($this->unique);
    }

    public function validateToken()
    {
        return Security::validateToken($this->unique, $this->token);
    }

    // unique methods

    public function getUnique()
    {
        return $this->unique;
    }

    public function setUnique($value)
    {
        $this->unique = $value;
    }

    public function req($member)
    {
        $v = $member . 'Validator';
        return Form::isReq($this->$v);
    }

    public function getFieldError($field)
    {
        if ($this->validation->getField($field) !== null && !$this->validation->getField($field)->isValid())
            return '<div class="rb-form-error">' . $this->validation->getField($field)->getMessage() . '</div>';
        return '';
    }
}
