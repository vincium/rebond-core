<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\Role;
use Rebond\Repository\Core\PermissionRepository;
use Rebond\Repository\Core\RolePermissionRepository;
use Rebond\Services\Form;

class RoleForm extends BaseRoleForm
{
    /**
     * @param Role $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildPermissions()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = PermissionRepository::getList(['id', 'title']);
        $options['where'][] = 'permission.status IN (0,1)';
        $options['order'][] = 'permission.title';
        $items = PermissionRepository::loadAll($options);

        $options['clearSelect'] = true;
        $options['select'][] = RolePermissionRepository::getList(['permission_id']);
        $permissions = RolePermissionRepository::loadAllByRoleId($this->getModel()->getId());
        $selectedValues = [];
        foreach ($permissions as $permission) {
            $selectedValues[] = $permission->getPermissionId();
        }
        return Form::buildCheckboxList('permission' . $this->unique, $items, 'id', 'title', $selectedValues);
    }
}
