<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Enums\Core\Code;
use Rebond\Models\Core\User;
use Rebond\Repository\Core\RoleRepository;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Core\UserRoleRepository;
use Rebond\Repository\Core\UserSecurityRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserSecurityService;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Security;
use Rebond\Services\Session;
use Rebond\Services\Validate;

class UserForm extends BaseUserForm
{
    /**
     * @param User $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildRoles()
    {
        $items = RoleRepository::loadAll();
        $roles = UserRoleRepository::loadAllByUserId($this->getModel()->getId());
        $selectedValues = [];
        foreach ($roles as $role) {
            $selectedValues[] = $role->getRoleId();
        }
        return Form::buildCheckboxList('role' . $this->unique, $items, 'id', 'title', $selectedValues, true);
    }

    /**
     * Signing
     */
    public function signIn()
    {
        // auth
        if (Auth::isAuth($this->getModel())) {
            Session::redirect('/');
        }

        if (!Form::isSubmitted('btnSignIn')) {
            return;
        }

        $this->setFromPost();

        $fields = [];
        $fields['token'] = $this->validateToken();
        $fields['email'] = $this->validateEmail(false);
        $fields['password'] = $this->validatePassword();
        $this->getValidation()->setFields($fields);

        if ($this->getValidation()->isValid()) {
            $signedUser = UserRepository::loadByEmailAndPassword($this->getModel()->getEmail(), $this->getModel()->getPassword());
            if (isset($signedUser) && Auth::isAuth($signedUser)) {
                Session::set('allSuccess', Lang::lang('hi', [$signedUser->getFullName()]));
                Session::set('signedUser', $signedUser->getId());
                if (!empty($_POST['persistentCookie'])) {
                    UserSecurityService::saveSecure($signedUser->getId(), \Rebond\Enums\Core\Security::REMEMBER);
                }
                Log::log(Code::USER_SIGN_IN, $signedUser->getId(), __FILE__, __LINE__);
                $this->setModel($signedUser);
            } else {
                Session::set('allError', Lang::lang('error_credentials_invalid'));
            }
        } else {
            Session::set('allError', $this->getValidation()->getMessage());
        }
    }

    /**
     * @param bool $setUser
     * @param bool $checkCurrentPassword
     */
    public function changePassword($setUser, $checkCurrentPassword)
    {
        $encryptedOldPassword = $this->getModel()->getPassword();
        $this->setFromPost();

        $clearNewPassword = Converter::stringKey('passwordnew', 'post');

        $fields = [];
        $passwordValidator = ['password' => true, 'required' => true, 'minLength' => 4, 'maxLength' => 40];
        $fields['token'] = $this->validateToken();
        if ($checkCurrentPassword) {
            $fields['password'] = $this->validatePassword();
            $passwordValidator['different'] = $this->getModel()->getPassword();
        }
        $fields['passwordnew'] = Validate::validate('passwordnew', $clearNewPassword, $passwordValidator);
        $this->getValidation()->setFields($fields);

        if ($this->getValidation()->isValid()) {
            if (!$checkCurrentPassword
                || Security::isValidPassword($this->getModel()->getPassword(), $encryptedOldPassword)
            ) {
                $this->getModel()->setPassword(Security::encryptPassword($clearNewPassword));
                UserRepository::savePassword($this->getModel());
                UserSecurityRepository::deleteSecure($this->getModel()->getId(), \Rebond\Enums\Core\Security::RESET);

                if ($setUser) {
                    Session::set('signedUser', $this->getModel()->getId());
                }
                return true;
            }
            Session::set('allError', Lang::lang('error_password'));
            return false;
        }
        Session::set('allError', $this->getValidation()->getMessage());
        return false;

    }
}
