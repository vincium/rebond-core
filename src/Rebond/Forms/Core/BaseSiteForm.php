<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\Site;
use Rebond\Repository\Core\SiteRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseSiteForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $googleAnalyticsValidator;
    /* @var string */
    protected $googleAnalyticsBuilder;
    /* @var array */
    protected $keywordsValidator;
    /* @var string */
    protected $keywordsBuilder;
    /* @var array */
    protected $descriptionValidator;
    /* @var string */
    protected $descriptionBuilder;
    /* @var array */
    protected $cssValidator;
    /* @var string */
    protected $cssBuilder;
    /* @var array */
    protected $jsValidator;
    /* @var string */
    protected $jsBuilder;
    /* @var array */
    protected $signInUrlValidator;
    /* @var string */
    protected $signInUrlBuilder;
    /* @var array */
    protected $isDebugValidator;
    /* @var array */
    protected $logSqlValidator;
    /* @var array */
    protected $timezoneValidator;
    /* @var string */
    protected $timezoneBuilder;
    /* @var array */
    protected $isCmsValidator;
    /* @var array */
    protected $cacheTimeValidator;
    /* @var string */
    protected $cacheTimeBuilder;
    /* @var array */
    protected $useDeviceTemplateValidator;
    /* @var array */
    protected $skinValidator;
    /* @var string */
    protected $skinBuilder;
    /* @var array */
    protected $sendMailOnErrorValidator;
    /* @var array */
    protected $mailListOnErrorValidator;
    /* @var string */
    protected $mailListOnErrorBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Site $model
     * @param string $unique
     */
    public function __construct(Site $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 50];
        $this->titleBuilder = 'string';
        $this->googleAnalyticsValidator = ['string' => true, 'alphaNumDash' => false, 'maxLength' => 20];
        $this->googleAnalyticsBuilder = 'string';
        $this->keywordsValidator = ['string' => true, 'maxLength' => 500];
        $this->keywordsBuilder = 'string';
        $this->descriptionValidator = ['string' => true, 'maxLength' => 1000];
        $this->descriptionBuilder = 'string';
        $this->cssValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 500];
        $this->cssBuilder = 'string';
        $this->jsValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 500];
        $this->jsBuilder = 'string';
        $this->signInUrlValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 50];
        $this->signInUrlBuilder = 'string';
        $this->isDebugValidator = ['bool' => true];
        $this->logSqlValidator = ['bool' => true];
        $this->timezoneValidator = ['string' => true, 'alphaNumSafe' => true, 'maxLength' => 40];
        $this->timezoneBuilder = 'string';
        $this->isCmsValidator = ['bool' => true];
        $this->cacheTimeValidator = ['integer' => true, 'required' => true, 'minValue' => 0];
        $this->cacheTimeBuilder = 'integer';
        $this->useDeviceTemplateValidator = ['bool' => true];
        $this->skinValidator = ['string' => true, 'alphaNum' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->skinBuilder = 'string';
        $this->sendMailOnErrorValidator = ['bool' => true];
        $this->mailListOnErrorValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 200];
        $this->mailListOnErrorBuilder = 'string';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseSiteForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('googleAnalytics', $properties)) {
            $value = Converter::stringKey('googleAnalytics' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setGoogleAnalytics($value);
            }
        }
        if (!isset($properties) || in_array('keywords', $properties)) {
            $value = Converter::stringKey('keywords' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setKeywords($value);
            }
        }
        if (!isset($properties) || in_array('description', $properties)) {
            $value = Converter::stringKey('description' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDescription($value);
            }
        }
        if (!isset($properties) || in_array('css', $properties)) {
            $value = Converter::stringKey('css' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCss($value);
            }
        }
        if (!isset($properties) || in_array('js', $properties)) {
            $value = Converter::stringKey('js' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setJs($value);
            }
        }
        if (!isset($properties) || in_array('signInUrl', $properties)) {
            $value = Converter::stringKey('signInUrl' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSignInUrl($value);
            }
        }
        if (!isset($properties) || in_array('isDebug', $properties)) {
            $value = Converter::boolKey('isDebug' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIsDebug($value);
            }
        }
        if (!isset($properties) || in_array('logSql', $properties)) {
            $value = Converter::boolKey('logSql' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setLogSql($value);
            }
        }
        if (!isset($properties) || in_array('timezone', $properties)) {
            $value = Converter::stringKey('timezone' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTimezone($value);
            }
        }
        if (!isset($properties) || in_array('isCms', $properties)) {
            $value = Converter::boolKey('isCms' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIsCms($value);
            }
        }
        if (!isset($properties) || in_array('cacheTime', $properties)) {
            $value = Converter::intKey('cacheTime' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCacheTime($value);
            }
        }
        if (!isset($properties) || in_array('useDeviceTemplate', $properties)) {
            $value = Converter::boolKey('useDeviceTemplate' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUseDeviceTemplate($value);
            }
        }
        if (!isset($properties) || in_array('skin', $properties)) {
            $value = Converter::stringKey('skin' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSkin($value);
            }
        }
        if (!isset($properties) || in_array('sendMailOnError', $properties)) {
            $value = Converter::boolKey('sendMailOnError' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSendMailOnError($value);
            }
        }
        if (!isset($properties) || in_array('mailListOnError', $properties)) {
            $value = Converter::stringKey('mailListOnError' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMailListOnError($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('googleAnalytics', $properties)) {
            $fields['googleAnalytics'] = $this->validateGoogleAnalytics();
        }
        if (!isset($properties) || in_array('keywords', $properties)) {
            $fields['keywords'] = $this->validateKeywords();
        }
        if (!isset($properties) || in_array('description', $properties)) {
            $fields['description'] = $this->validateDescription();
        }
        if (!isset($properties) || in_array('css', $properties)) {
            $fields['css'] = $this->validateCss();
        }
        if (!isset($properties) || in_array('js', $properties)) {
            $fields['js'] = $this->validateJs();
        }
        if (!isset($properties) || in_array('signInUrl', $properties)) {
            $fields['signInUrl'] = $this->validateSignInUrl();
        }
        if (!isset($properties) || in_array('timezone', $properties)) {
            $fields['timezone'] = $this->validateTimezone();
        }
        if (!isset($properties) || in_array('cacheTime', $properties)) {
            $fields['cacheTime'] = $this->validateCacheTime();
        }
        if (!isset($properties) || in_array('skin', $properties)) {
            $fields['skin'] = $this->validateSkin();
        }
        if (!isset($properties) || in_array('mailListOnError', $properties)) {
            $fields['mailListOnError'] = $this->validateMailListOnError();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateGoogleAnalytics()
    {
        return Validate::validate('googleAnalytics', $this->getModel()->getGoogleAnalytics(), $this->googleAnalyticsValidator);
    }

    public function buildGoogleAnalytics()
    {
        return Form::buildField('googleAnalytics' . $this->unique, $this->googleAnalyticsBuilder, $this->getModel()->getGoogleAnalytics());
    }

    public function validateKeywords()
    {
        return Validate::validate('keywords', $this->getModel()->getKeywords(), $this->keywordsValidator);
    }

    public function buildKeywords()
    {
        return Form::buildField('keywords' . $this->unique, $this->keywordsBuilder, $this->getModel()->getKeywords());
    }

    public function validateDescription()
    {
        return Validate::validate('description', $this->getModel()->getDescription(), $this->descriptionValidator);
    }

    public function buildDescription()
    {
        return Form::buildField('description' . $this->unique, $this->descriptionBuilder, $this->getModel()->getDescription());
    }

    public function validateCss()
    {
        return Validate::validate('css', $this->getModel()->getCss(), $this->cssValidator);
    }

    public function buildCss()
    {
        return Form::buildField('css' . $this->unique, $this->cssBuilder, $this->getModel()->getCss());
    }

    public function validateJs()
    {
        return Validate::validate('js', $this->getModel()->getJs(), $this->jsValidator);
    }

    public function buildJs()
    {
        return Form::buildField('js' . $this->unique, $this->jsBuilder, $this->getModel()->getJs());
    }

    public function validateSignInUrl()
    {
        return Validate::validate('signInUrl', $this->getModel()->getSignInUrl(), $this->signInUrlValidator);
    }

    public function buildSignInUrl()
    {
        return Form::buildField('signInUrl' . $this->unique, $this->signInUrlBuilder, $this->getModel()->getSignInUrl());
    }

    public function buildIsDebug()
    {
        return Form::buildBoolean('isDebug' . $this->unique, 'is_debug', $this->getModel()->getIsDebug());
    }

    public function buildLogSql()
    {
        return Form::buildBoolean('logSql' . $this->unique, 'log_sql', $this->getModel()->getLogSql());
    }

    public function validateTimezone()
    {
        return Validate::validate('timezone', $this->getModel()->getTimezone(), $this->timezoneValidator);
    }

    public function buildTimezone()
    {
        return Form::buildField('timezone' . $this->unique, $this->timezoneBuilder, $this->getModel()->getTimezone());
    }

    public function buildIsCms()
    {
        return Form::buildBoolean('isCms' . $this->unique, 'is_cms', $this->getModel()->getIsCms());
    }

    public function validateCacheTime()
    {
        return Validate::validate('cacheTime', $this->getModel()->getCacheTime(), $this->cacheTimeValidator);
    }

    public function buildCacheTime()
    {
        return Form::buildField('cacheTime' . $this->unique, $this->cacheTimeBuilder, $this->getModel()->getCacheTime());
    }

    public function buildUseDeviceTemplate()
    {
        return Form::buildBoolean('useDeviceTemplate' . $this->unique, 'use_device_template', $this->getModel()->getUseDeviceTemplate());
    }

    public function validateSkin()
    {
        return Validate::validate('skin', $this->getModel()->getSkin(), $this->skinValidator);
    }

    public function buildSkin()
    {
        return Form::buildField('skin' . $this->unique, $this->skinBuilder, $this->getModel()->getSkin());
    }

    public function buildSendMailOnError()
    {
        return Form::buildBoolean('sendMailOnError' . $this->unique, 'send_mail_on_error', $this->getModel()->getSendMailOnError());
    }

    public function validateMailListOnError()
    {
        return Validate::validate('mailListOnError', $this->getModel()->getMailListOnError(), $this->mailListOnErrorValidator);
    }

    public function buildMailListOnError()
    {
        return Form::buildField('mailListOnError' . $this->unique, $this->mailListOnErrorBuilder, $this->getModel()->getMailListOnError());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
