<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Enums\Core\Result;
use Rebond\Models\Core\Media;
use Rebond\Repository\Core\MediaRepository;
use Rebond\Services\Validate;

class MediaForm extends BaseMediaForm
{
    /**
     * @param Media $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function validateUpload()
    {
        $v = Validate::validate('upload', $this->getModel()->getUpload(), $this->uploadValidator);
        if ($v->getResult() == Result::ERROR)
            return $v;
        $options = [];
        $options['where'][] = ['media.upload = ?', $this->getModel()->getUpload()];
        $options['where'][] = ['media.id != ?', $this->getModel()->getId()];
        $options['where'][] = 'media.status IN (0,1)';
        $exist = MediaRepository::count($options);
        if ($exist >= 1) {
            $v->setResult(Result::ERROR);
            $v->setMessage('a media already exists with this url');
        }
        return $v;
    }
}
