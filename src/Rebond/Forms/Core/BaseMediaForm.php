<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\Media;
use Rebond\Repository\Core\MediaRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseMediaForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $folderValidator;
    /* @var array */
    protected $tagsValidator;
    /* @var string */
    protected $tagsBuilder;
    /* @var array */
    protected $uploadValidator;
    /* @var string */
    protected $uploadBuilder;
    /* @var array */
    protected $originalFilenameValidator;
    /* @var string */
    protected $originalFilenameBuilder;
    /* @var array */
    protected $pathValidator;
    /* @var string */
    protected $pathBuilder;
    /* @var array */
    protected $extensionValidator;
    /* @var string */
    protected $extensionBuilder;
    /* @var array */
    protected $mimeTypeValidator;
    /* @var string */
    protected $mimeTypeBuilder;
    /* @var array */
    protected $fileSizeValidator;
    /* @var string */
    protected $fileSizeBuilder;
    /* @var array */
    protected $widthValidator;
    /* @var string */
    protected $widthBuilder;
    /* @var array */
    protected $heightValidator;
    /* @var string */
    protected $heightBuilder;
    /* @var array */
    protected $altValidator;
    /* @var string */
    protected $altBuilder;
    /* @var array */
    protected $isSelectableValidator;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Media $model
     * @param string $unique
     */
    public function __construct(Media $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->folderValidator = ['foreignKey' => true];
        $this->tagsValidator = ['string' => true, 'alphaNumSafe' => false, 'minLength' => 2, 'maxLength' => 100];
        $this->tagsBuilder = 'string';
        $this->uploadValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 50];
        $this->uploadBuilder = 'string';
        $this->originalFilenameValidator = ['string' => true, 'alphaNumSafe' => true, 'minLength' => 3, 'maxLength' => 50];
        $this->originalFilenameBuilder = 'string';
        $this->pathValidator = ['string' => true, 'alphaNumSafe' => false, 'minLength' => 2, 'maxLength' => 50];
        $this->pathBuilder = 'string';
        $this->extensionValidator = ['string' => true, 'alphaNum' => true, 'maxLength' => 5];
        $this->extensionBuilder = 'string';
        $this->mimeTypeValidator = ['string' => true, 'alphaNumSafe' => true, 'maxLength' => 64];
        $this->mimeTypeBuilder = 'string';
        $this->fileSizeValidator = ['integer' => true, 'required' => true];
        $this->fileSizeBuilder = 'integer';
        $this->widthValidator = ['integer' => true, 'required' => true];
        $this->widthBuilder = 'integer';
        $this->heightValidator = ['integer' => true, 'required' => true];
        $this->heightBuilder = 'integer';
        $this->altValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 100];
        $this->altBuilder = 'string';
        $this->isSelectableValidator = ['bool' => true];
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseMediaForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('folderId', $properties)) {
            $value = Converter::intKey('folderId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFolderId($value);
            }
        }
        if (!isset($properties) || in_array('tags', $properties)) {
            $value = Converter::stringKey('tags' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTags($value);
            }
        }
        if (!isset($properties) || in_array('upload', $properties)) {
            $value = Converter::stringKey('upload' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUpload($value);
            }
        }
        if (!isset($properties) || in_array('originalFilename', $properties)) {
            $value = Converter::stringKey('originalFilename' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setOriginalFilename($value);
            }
        }
        if (!isset($properties) || in_array('path', $properties)) {
            $value = Converter::stringKey('path' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPath($value);
            }
        }
        if (!isset($properties) || in_array('extension', $properties)) {
            $value = Converter::stringKey('extension' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setExtension($value);
            }
        }
        if (!isset($properties) || in_array('mimeType', $properties)) {
            $value = Converter::stringKey('mimeType' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMimeType($value);
            }
        }
        if (!isset($properties) || in_array('fileSize', $properties)) {
            $value = Converter::intKey('fileSize' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFileSize($value);
            }
        }
        if (!isset($properties) || in_array('width', $properties)) {
            $value = Converter::intKey('width' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setWidth($value);
            }
        }
        if (!isset($properties) || in_array('height', $properties)) {
            $value = Converter::intKey('height' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setHeight($value);
            }
        }
        if (!isset($properties) || in_array('alt', $properties)) {
            $value = Converter::stringKey('alt' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setAlt($value);
            }
        }
        if (!isset($properties) || in_array('isSelectable', $properties)) {
            $value = Converter::boolKey('isSelectable' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIsSelectable($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('folderId', $properties)) {
            $fields['folder'] = $this->validateFolder();
        }
        if (!isset($properties) || in_array('tags', $properties)) {
            $fields['tags'] = $this->validateTags();
        }
        if (!isset($properties) || in_array('upload', $properties)) {
            $fields['upload'] = $this->validateUpload();
        }
        if (!isset($properties) || in_array('originalFilename', $properties)) {
            $fields['originalFilename'] = $this->validateOriginalFilename();
        }
        if (!isset($properties) || in_array('path', $properties)) {
            $fields['path'] = $this->validatePath();
        }
        if (!isset($properties) || in_array('extension', $properties)) {
            $fields['extension'] = $this->validateExtension();
        }
        if (!isset($properties) || in_array('mimeType', $properties)) {
            $fields['mimeType'] = $this->validateMimeType();
        }
        if (!isset($properties) || in_array('fileSize', $properties)) {
            $fields['fileSize'] = $this->validateFileSize();
        }
        if (!isset($properties) || in_array('width', $properties)) {
            $fields['width'] = $this->validateWidth();
        }
        if (!isset($properties) || in_array('height', $properties)) {
            $fields['height'] = $this->validateHeight();
        }
        if (!isset($properties) || in_array('alt', $properties)) {
            $fields['alt'] = $this->validateAlt();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateFolder()
    {
        return Validate::validate('folder', $this->getModel()->getFolderId(), $this->folderValidator);
    }

    public function buildFolder()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\FolderRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Core\FolderRepository::loadAll($options);
        return Form::buildItemList('folderId' . $this->unique, $models, 'id', 'title', $this->getModel()->getFolderId(), $this->folderValidator['foreignKey']);
    }

    public function validateTags()
    {
        return Validate::validate('tags', $this->getModel()->getTags(), $this->tagsValidator);
    }

    public function buildTags()
    {
        return Form::buildField('tags' . $this->unique, $this->tagsBuilder, $this->getModel()->getTags());
    }

    public function validateUpload()
    {
        return Validate::validate('upload', $this->getModel()->getUpload(), $this->uploadValidator);
    }

    public function buildUpload()
    {
        return Form::buildField('upload' . $this->unique, $this->uploadBuilder, $this->getModel()->getUpload());
    }

    public function validateOriginalFilename()
    {
        return Validate::validate('originalFilename', $this->getModel()->getOriginalFilename(), $this->originalFilenameValidator);
    }

    public function buildOriginalFilename()
    {
        return Form::buildField('originalFilename' . $this->unique, $this->originalFilenameBuilder, $this->getModel()->getOriginalFilename());
    }

    public function validatePath()
    {
        return Validate::validate('path', $this->getModel()->getPath(), $this->pathValidator);
    }

    public function buildPath()
    {
        return Form::buildField('path' . $this->unique, $this->pathBuilder, $this->getModel()->getPath());
    }

    public function validateExtension()
    {
        return Validate::validate('extension', $this->getModel()->getExtension(), $this->extensionValidator);
    }

    public function buildExtension()
    {
        return Form::buildField('extension' . $this->unique, $this->extensionBuilder, $this->getModel()->getExtension());
    }

    public function validateMimeType()
    {
        return Validate::validate('mimeType', $this->getModel()->getMimeType(), $this->mimeTypeValidator);
    }

    public function buildMimeType()
    {
        return Form::buildField('mimeType' . $this->unique, $this->mimeTypeBuilder, $this->getModel()->getMimeType());
    }

    public function validateFileSize()
    {
        return Validate::validate('fileSize', $this->getModel()->getFileSize(), $this->fileSizeValidator);
    }

    public function buildFileSize()
    {
        return Form::buildField('fileSize' . $this->unique, $this->fileSizeBuilder, $this->getModel()->getFileSize());
    }

    public function validateWidth()
    {
        return Validate::validate('width', $this->getModel()->getWidth(), $this->widthValidator);
    }

    public function buildWidth()
    {
        return Form::buildField('width' . $this->unique, $this->widthBuilder, $this->getModel()->getWidth());
    }

    public function validateHeight()
    {
        return Validate::validate('height', $this->getModel()->getHeight(), $this->heightValidator);
    }

    public function buildHeight()
    {
        return Form::buildField('height' . $this->unique, $this->heightBuilder, $this->getModel()->getHeight());
    }

    public function validateAlt()
    {
        return Validate::validate('alt', $this->getModel()->getAlt(), $this->altValidator);
    }

    public function buildAlt()
    {
        return Form::buildField('alt' . $this->unique, $this->altBuilder, $this->getModel()->getAlt());
    }

    public function buildIsSelectable()
    {
        return Form::buildBoolean('isSelectable' . $this->unique, 'is_selectable', $this->getModel()->getIsSelectable());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
