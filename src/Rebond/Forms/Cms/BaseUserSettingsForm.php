<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\UserSettings;
use Rebond\Repository\Cms\UserSettingsRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseUserSettingsForm extends AbstractForm
{
    /* @var array */
    protected $idValidator;
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $mediaViewValidator;
    /* @var array */
    protected $mediaPagingValidator;
    /* @var array */
    protected $contentPagingValidator;
    /* @var array */
    protected $pagingValidator;
    /*
     * @param UserSettings $model
     * @param string $unique
     */
    public function __construct(UserSettings $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idValidator = ['primaryKey' => true];
        $this->idBuilder = 'primaryKey';
        $this->mediaViewValidator = ['enum' => true];
        $this->mediaPagingValidator = ['enum' => true];
        $this->contentPagingValidator = ['enum' => true];
        $this->pagingValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseUserSettingsForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('mediaView', $properties)) {
            $value = Converter::intKey('mediaView' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMediaView($value);
            }
        }
        if (!isset($properties) || in_array('mediaPaging', $properties)) {
            $value = Converter::intKey('mediaPaging' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMediaPaging($value);
            }
        }
        if (!isset($properties) || in_array('contentPaging', $properties)) {
            $value = Converter::intKey('contentPaging' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setContentPaging($value);
            }
        }
        if (!isset($properties) || in_array('paging', $properties)) {
            $value = Converter::intKey('paging' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPaging($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('id', $properties)) {
            $fields['id'] = $this->validateId();
        }
        if (!isset($properties) || in_array('mediaView', $properties)) {
            $fields['mediaView'] = $this->validateMediaView();
        }
        if (!isset($properties) || in_array('mediaPaging', $properties)) {
            $fields['mediaPaging'] = $this->validateMediaPaging();
        }
        if (!isset($properties) || in_array('contentPaging', $properties)) {
            $fields['contentPaging'] = $this->validateContentPaging();
        }
        if (!isset($properties) || in_array('paging', $properties)) {
            $fields['paging'] = $this->validatePaging();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function validateId()
    {
        return Validate::validate('id', $this->getModel()->getId(), $this->idValidator);
    }

    public function buildId()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('id' . $this->unique, $models, 'id', 'email', $this->getModel()->getId(), $this->idValidator['primaryKey']);
    }

    public function validateMediaView()
    {
        return Validate::validate('mediaView', $this->getModel()->getMediaView(), $this->mediaViewValidator);
    }

    public function buildMediaView()
    {
        return Form::buildList('mediaView' . $this->unique, $this->getModel()->getMediaViewList(), $this->getModel()->getMediaView());
    }

    public function validateMediaPaging()
    {
        return Validate::validate('mediaPaging', $this->getModel()->getMediaPaging(), $this->mediaPagingValidator);
    }

    public function buildMediaPaging()
    {
        return Form::buildList('mediaPaging' . $this->unique, $this->getModel()->getMediaPagingList(), $this->getModel()->getMediaPaging());
    }

    public function validateContentPaging()
    {
        return Validate::validate('contentPaging', $this->getModel()->getContentPaging(), $this->contentPagingValidator);
    }

    public function buildContentPaging()
    {
        return Form::buildList('contentPaging' . $this->unique, $this->getModel()->getContentPagingList(), $this->getModel()->getContentPaging());
    }

    public function validatePaging()
    {
        return Validate::validate('paging', $this->getModel()->getPaging(), $this->pagingValidator);
    }

    public function buildPaging()
    {
        return Form::buildList('paging' . $this->unique, $this->getModel()->getPagingList(), $this->getModel()->getPaging());
    }

}
