<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Template;
use Rebond\Repository\Cms\TemplateRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseTemplateForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $summaryValidator;
    /* @var string */
    protected $summaryBuilder;
    /* @var array */
    protected $filenameValidator;
    /* @var string */
    protected $filenameBuilder;
    /* @var array */
    protected $menuValidator;
    /* @var array */
    protected $menuLevelValidator;
    /* @var string */
    protected $menuLevelBuilder;
    /* @var array */
    protected $inBreadcrumbValidator;
    /* @var array */
    protected $sideNavValidator;
    /* @var array */
    protected $sideNavLevelValidator;
    /* @var string */
    protected $sideNavLevelBuilder;
    /* @var array */
    protected $inFooterValidator;
    /* @var array */
    protected $footerLevelValidator;
    /* @var string */
    protected $footerLevelBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Template $model
     * @param string $unique
     */
    public function __construct(Template $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->summaryValidator = ['string' => true, 'required' => true, 'maxLength' => 200];
        $this->summaryBuilder = 'string';
        $this->filenameValidator = ['string' => true, 'alphaNumDash' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->filenameBuilder = 'string';
        $this->menuValidator = ['enum' => true, 'required' => true];
        $this->menuLevelValidator = ['integer' => true, 'required' => true];
        $this->menuLevelBuilder = 'integer';
        $this->inBreadcrumbValidator = ['bool' => true];
        $this->sideNavValidator = ['enum' => true, 'required' => true];
        $this->sideNavLevelValidator = ['integer' => true, 'required' => true];
        $this->sideNavLevelBuilder = 'integer';
        $this->inFooterValidator = ['bool' => true];
        $this->footerLevelValidator = ['integer' => true, 'required' => true];
        $this->footerLevelBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseTemplateForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $value = Converter::stringKey('summary' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSummary($value);
            }
        }
        if (!isset($properties) || in_array('filename', $properties)) {
            $value = Converter::stringKey('filename' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFilename($value);
            }
        }
        if (!isset($properties) || in_array('menu', $properties)) {
            $value = Converter::intKey('menu' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMenu($value);
            }
        }
        if (!isset($properties) || in_array('menuLevel', $properties)) {
            $value = Converter::intKey('menuLevel' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMenuLevel($value);
            }
        }
        if (!isset($properties) || in_array('inBreadcrumb', $properties)) {
            $value = Converter::boolKey('inBreadcrumb' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInBreadcrumb($value);
            }
        }
        if (!isset($properties) || in_array('sideNav', $properties)) {
            $value = Converter::intKey('sideNav' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSideNav($value);
            }
        }
        if (!isset($properties) || in_array('sideNavLevel', $properties)) {
            $value = Converter::intKey('sideNavLevel' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSideNavLevel($value);
            }
        }
        if (!isset($properties) || in_array('inFooter', $properties)) {
            $value = Converter::boolKey('inFooter' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInFooter($value);
            }
        }
        if (!isset($properties) || in_array('footerLevel', $properties)) {
            $value = Converter::intKey('footerLevel' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFooterLevel($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $fields['summary'] = $this->validateSummary();
        }
        if (!isset($properties) || in_array('filename', $properties)) {
            $fields['filename'] = $this->validateFilename();
        }
        if (!isset($properties) || in_array('menu', $properties)) {
            $fields['menu'] = $this->validateMenu();
        }
        if (!isset($properties) || in_array('menuLevel', $properties)) {
            $fields['menuLevel'] = $this->validateMenuLevel();
        }
        if (!isset($properties) || in_array('sideNav', $properties)) {
            $fields['sideNav'] = $this->validateSideNav();
        }
        if (!isset($properties) || in_array('sideNavLevel', $properties)) {
            $fields['sideNavLevel'] = $this->validateSideNavLevel();
        }
        if (!isset($properties) || in_array('footerLevel', $properties)) {
            $fields['footerLevel'] = $this->validateFooterLevel();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateSummary()
    {
        return Validate::validate('summary', $this->getModel()->getSummary(), $this->summaryValidator);
    }

    public function buildSummary()
    {
        return Form::buildField('summary' . $this->unique, $this->summaryBuilder, $this->getModel()->getSummary());
    }

    public function validateFilename()
    {
        return Validate::validate('filename', $this->getModel()->getFilename(), $this->filenameValidator);
    }

    public function buildFilename()
    {
        return Form::buildField('filename' . $this->unique, $this->filenameBuilder, $this->getModel()->getFilename());
    }

    public function validateMenu()
    {
        return Validate::validate('menu', $this->getModel()->getMenu(), $this->menuValidator);
    }

    public function buildMenu()
    {
        return Form::buildList('menu' . $this->unique, $this->getModel()->getMenuList(), $this->getModel()->getMenu());
    }

    public function validateMenuLevel()
    {
        return Validate::validate('menuLevel', $this->getModel()->getMenuLevel(), $this->menuLevelValidator);
    }

    public function buildMenuLevel()
    {
        return Form::buildField('menuLevel' . $this->unique, $this->menuLevelBuilder, $this->getModel()->getMenuLevel());
    }

    public function buildInBreadcrumb()
    {
        return Form::buildBoolean('inBreadcrumb' . $this->unique, 'in_breadcrumb', $this->getModel()->getInBreadcrumb());
    }

    public function validateSideNav()
    {
        return Validate::validate('sideNav', $this->getModel()->getSideNav(), $this->sideNavValidator);
    }

    public function buildSideNav()
    {
        return Form::buildList('sideNav' . $this->unique, $this->getModel()->getSideNavList(), $this->getModel()->getSideNav());
    }

    public function validateSideNavLevel()
    {
        return Validate::validate('sideNavLevel', $this->getModel()->getSideNavLevel(), $this->sideNavLevelValidator);
    }

    public function buildSideNavLevel()
    {
        return Form::buildField('sideNavLevel' . $this->unique, $this->sideNavLevelBuilder, $this->getModel()->getSideNavLevel());
    }

    public function buildInFooter()
    {
        return Form::buildBoolean('inFooter' . $this->unique, 'in_footer', $this->getModel()->getInFooter());
    }

    public function validateFooterLevel()
    {
        return Validate::validate('footerLevel', $this->getModel()->getFooterLevel(), $this->footerLevelValidator);
    }

    public function buildFooterLevel()
    {
        return Form::buildField('footerLevel' . $this->unique, $this->footerLevelBuilder, $this->getModel()->getFooterLevel());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
