<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\UserSettings;
use Rebond\Services\Form;

class UserSettingsForm extends BaseUserSettingsForm
{
    /**
     * @param UserSettings $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildHiddenUser()
    {
        return Form::buildField('userId' . $this->unique, 'hidden', $this->getModel()->getUserId());
    }
}
