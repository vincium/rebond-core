<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Filter;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Services\Form;

class FilterForm extends BaseFilterForm
{
    /**
     * @param Filter $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildModule()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = ModuleRepository::getList(['id', 'title']);
        $options['where'][] = 'module.status = 1';
        $items = ModuleRepository::loadAll($options);
        return Form::buildItemList('moduleId' . $this->unique, $items, 'id', 'title', $this->getModel()->getModuleId(), true, true);
    }
}
