<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Module;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseModuleForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $nameValidator;
    /* @var string */
    protected $nameBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $summaryValidator;
    /* @var string */
    protected $summaryBuilder;
    /* @var array */
    protected $workflowValidator;
    /* @var array */
    protected $hasFilterValidator;
    /* @var array */
    protected $hasContentValidator;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Module $model
     * @param string $unique
     */
    public function __construct(Module $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->nameValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->nameBuilder = 'string';
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->summaryValidator = ['string' => true, 'required' => true, 'maxLength' => 200];
        $this->summaryBuilder = 'string';
        $this->workflowValidator = ['enum' => true];
        $this->hasFilterValidator = ['bool' => true];
        $this->hasContentValidator = ['bool' => true];
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseModuleForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('name', $properties)) {
            $value = Converter::stringKey('name' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setName($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $value = Converter::stringKey('summary' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSummary($value);
            }
        }
        if (!isset($properties) || in_array('workflow', $properties)) {
            $value = Converter::intKey('workflow' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setWorkflow($value);
            }
        }
        if (!isset($properties) || in_array('hasFilter', $properties)) {
            $value = Converter::boolKey('hasFilter' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setHasFilter($value);
            }
        }
        if (!isset($properties) || in_array('hasContent', $properties)) {
            $value = Converter::boolKey('hasContent' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setHasContent($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('name', $properties)) {
            $fields['name'] = $this->validateName();
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $fields['summary'] = $this->validateSummary();
        }
        if (!isset($properties) || in_array('workflow', $properties)) {
            $fields['workflow'] = $this->validateWorkflow();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateName()
    {
        return Validate::validate('name', $this->getModel()->getName(), $this->nameValidator);
    }

    public function buildName()
    {
        return Form::buildField('name' . $this->unique, $this->nameBuilder, $this->getModel()->getName());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateSummary()
    {
        return Validate::validate('summary', $this->getModel()->getSummary(), $this->summaryValidator);
    }

    public function buildSummary()
    {
        return Form::buildField('summary' . $this->unique, $this->summaryBuilder, $this->getModel()->getSummary());
    }

    public function validateWorkflow()
    {
        return Validate::validate('workflow', $this->getModel()->getWorkflow(), $this->workflowValidator);
    }

    public function buildWorkflow()
    {
        return Form::buildList('workflow' . $this->unique, $this->getModel()->getWorkflowList(), $this->getModel()->getWorkflow());
    }

    public function buildHasFilter()
    {
        return Form::buildBoolean('hasFilter' . $this->unique, 'has_filter', $this->getModel()->getHasFilter());
    }

    public function buildHasContent()
    {
        return Form::buildBoolean('hasContent' . $this->unique, 'has_content', $this->getModel()->getHasContent());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
