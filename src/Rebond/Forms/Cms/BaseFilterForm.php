<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Filter;
use Rebond\Repository\Cms\FilterRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseFilterForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $moduleValidator;
    /* @var array */
    protected $displayOrderValidator;
    /* @var string */
    protected $displayOrderBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Filter $model
     * @param string $unique
     */
    public function __construct(Filter $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->moduleValidator = ['foreignKey' => true];
        $this->displayOrderValidator = ['integer' => true, 'required' => true];
        $this->displayOrderBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseFilterForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $value = Converter::intKey('moduleId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setModuleId($value);
            }
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $value = Converter::intKey('displayOrder' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDisplayOrder($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $fields['module'] = $this->validateModule();
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $fields['displayOrder'] = $this->validateDisplayOrder();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateModule()
    {
        return Validate::validate('module', $this->getModel()->getModuleId(), $this->moduleValidator);
    }

    public function buildModule()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\ModuleRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\ModuleRepository::loadAll($options);
        return Form::buildItemList('moduleId' . $this->unique, $models, 'id', 'title', $this->getModel()->getModuleId(), $this->moduleValidator['foreignKey']);
    }

    public function validateDisplayOrder()
    {
        return Validate::validate('displayOrder', $this->getModel()->getDisplayOrder(), $this->displayOrderValidator);
    }

    public function buildDisplayOrder()
    {
        return Form::buildField('displayOrder' . $this->unique, $this->displayOrderBuilder, $this->getModel()->getDisplayOrder());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
