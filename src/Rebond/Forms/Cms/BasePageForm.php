<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Page;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BasePageForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $parentValidator;
    /* @var array */
    protected $templateValidator;
    /* @var array */
    protected $layoutValidator;
    /* @var array */
    protected $cssValidator;
    /* @var string */
    protected $cssBuilder;
    /* @var array */
    protected $jsValidator;
    /* @var string */
    protected $jsBuilder;
    /* @var array */
    protected $inNavHeaderValidator;
    /* @var array */
    protected $inNavSideValidator;
    /* @var array */
    protected $inSitemapValidator;
    /* @var array */
    protected $inBreadcrumbValidator;
    /* @var array */
    protected $inNavFooterValidator;
    /* @var array */
    protected $friendlyUrlPathValidator;
    /* @var string */
    protected $friendlyUrlPathBuilder;
    /* @var array */
    protected $friendlyUrlValidator;
    /* @var string */
    protected $friendlyUrlBuilder;
    /* @var array */
    protected $redirectValidator;
    /* @var string */
    protected $redirectBuilder;
    /* @var array */
    protected $classValidator;
    /* @var string */
    protected $classBuilder;
    /* @var array */
    protected $permissionValidator;
    /* @var string */
    protected $permissionBuilder;
    /* @var array */
    protected $displayOrderValidator;
    /* @var string */
    protected $displayOrderBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Page $model
     * @param string $unique
     */
    public function __construct(Page $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 40];
        $this->titleBuilder = 'string';
        $this->parentValidator = ['foreignKey' => true];
        $this->templateValidator = ['foreignKey' => true];
        $this->layoutValidator = ['foreignKey' => true];
        $this->cssValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 200];
        $this->cssBuilder = 'string';
        $this->jsValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 200];
        $this->jsBuilder = 'string';
        $this->inNavHeaderValidator = ['bool' => true];
        $this->inNavSideValidator = ['bool' => true];
        $this->inSitemapValidator = ['bool' => true];
        $this->inBreadcrumbValidator = ['bool' => true];
        $this->inNavFooterValidator = ['bool' => true];
        $this->friendlyUrlPathValidator = ['hidden' => true, 'maxLength' => 200];
        $this->friendlyUrlPathBuilder = 'hidden';
        $this->friendlyUrlValidator = ['string' => true, 'alphaNumDash' => true, 'minLength' => 2, 'maxLength' => 60];
        $this->friendlyUrlBuilder = 'string';
        $this->redirectValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 100];
        $this->redirectBuilder = 'string';
        $this->classValidator = ['string' => true, 'maxLength' => 20];
        $this->classBuilder = 'string';
        $this->permissionValidator = ['string' => true, 'maxLength' => 20];
        $this->permissionBuilder = 'string';
        $this->displayOrderValidator = ['integer' => true, 'required' => true];
        $this->displayOrderBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BasePageForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('parentId', $properties)) {
            $value = Converter::intKey('parentId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setParentId($value);
            }
        }
        if (!isset($properties) || in_array('templateId', $properties)) {
            $value = Converter::intKey('templateId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTemplateId($value);
            }
        }
        if (!isset($properties) || in_array('layoutId', $properties)) {
            $value = Converter::intKey('layoutId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setLayoutId($value);
            }
        }
        if (!isset($properties) || in_array('css', $properties)) {
            $value = Converter::stringKey('css' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCss($value);
            }
        }
        if (!isset($properties) || in_array('js', $properties)) {
            $value = Converter::stringKey('js' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setJs($value);
            }
        }
        if (!isset($properties) || in_array('inNavHeader', $properties)) {
            $value = Converter::boolKey('inNavHeader' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInNavHeader($value);
            }
        }
        if (!isset($properties) || in_array('inNavSide', $properties)) {
            $value = Converter::boolKey('inNavSide' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInNavSide($value);
            }
        }
        if (!isset($properties) || in_array('inSitemap', $properties)) {
            $value = Converter::boolKey('inSitemap' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInSitemap($value);
            }
        }
        if (!isset($properties) || in_array('inBreadcrumb', $properties)) {
            $value = Converter::boolKey('inBreadcrumb' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInBreadcrumb($value);
            }
        }
        if (!isset($properties) || in_array('inNavFooter', $properties)) {
            $value = Converter::boolKey('inNavFooter' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setInNavFooter($value);
            }
        }
        if (!isset($properties) || in_array('friendlyUrlPath', $properties)) {
            $value = Converter::stringKey('friendlyUrlPath' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFriendlyUrlPath($value);
            }
        }
        if (!isset($properties) || in_array('friendlyUrl', $properties)) {
            $value = Converter::stringKey('friendlyUrl' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFriendlyUrl($value);
            }
        }
        if (!isset($properties) || in_array('redirect', $properties)) {
            $value = Converter::stringKey('redirect' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setRedirect($value);
            }
        }
        if (!isset($properties) || in_array('class', $properties)) {
            $value = Converter::stringKey('class' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setClass($value);
            }
        }
        if (!isset($properties) || in_array('permission', $properties)) {
            $value = Converter::stringKey('permission' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPermission($value);
            }
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $value = Converter::intKey('displayOrder' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDisplayOrder($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('parentId', $properties)) {
            $fields['parent'] = $this->validateParent();
        }
        if (!isset($properties) || in_array('templateId', $properties)) {
            $fields['template'] = $this->validateTemplate();
        }
        if (!isset($properties) || in_array('layoutId', $properties)) {
            $fields['layout'] = $this->validateLayout();
        }
        if (!isset($properties) || in_array('css', $properties)) {
            $fields['css'] = $this->validateCss();
        }
        if (!isset($properties) || in_array('js', $properties)) {
            $fields['js'] = $this->validateJs();
        }
        if (!isset($properties) || in_array('friendlyUrl', $properties)) {
            $fields['friendlyUrl'] = $this->validateFriendlyUrl();
        }
        if (!isset($properties) || in_array('redirect', $properties)) {
            $fields['redirect'] = $this->validateRedirect();
        }
        if (!isset($properties) || in_array('class', $properties)) {
            $fields['class'] = $this->validateClass();
        }
        if (!isset($properties) || in_array('permission', $properties)) {
            $fields['permission'] = $this->validatePermission();
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $fields['displayOrder'] = $this->validateDisplayOrder();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateParent()
    {
        return Validate::validate('parent', $this->getModel()->getParentId(), $this->parentValidator);
    }

    public function buildParent()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\PageRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\PageRepository::loadAll($options);
        return Form::buildItemList('parentId' . $this->unique, $models, 'id', 'title', $this->getModel()->getParentId(), $this->parentValidator['foreignKey']);
    }

    public function validateTemplate()
    {
        return Validate::validate('template', $this->getModel()->getTemplateId(), $this->templateValidator);
    }

    public function buildTemplate()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\TemplateRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\TemplateRepository::loadAll($options);
        return Form::buildItemList('templateId' . $this->unique, $models, 'id', 'title', $this->getModel()->getTemplateId(), $this->templateValidator['foreignKey']);
    }

    public function validateLayout()
    {
        return Validate::validate('layout', $this->getModel()->getLayoutId(), $this->layoutValidator);
    }

    public function buildLayout()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\LayoutRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\LayoutRepository::loadAll($options);
        return Form::buildItemList('layoutId' . $this->unique, $models, 'id', 'title', $this->getModel()->getLayoutId(), $this->layoutValidator['foreignKey']);
    }

    public function validateCss()
    {
        return Validate::validate('css', $this->getModel()->getCss(), $this->cssValidator);
    }

    public function buildCss()
    {
        return Form::buildField('css' . $this->unique, $this->cssBuilder, $this->getModel()->getCss());
    }

    public function validateJs()
    {
        return Validate::validate('js', $this->getModel()->getJs(), $this->jsValidator);
    }

    public function buildJs()
    {
        return Form::buildField('js' . $this->unique, $this->jsBuilder, $this->getModel()->getJs());
    }

    public function buildInNavHeader()
    {
        return Form::buildBoolean('inNavHeader' . $this->unique, 'in_nav_header', $this->getModel()->getInNavHeader());
    }

    public function buildInNavSide()
    {
        return Form::buildBoolean('inNavSide' . $this->unique, 'in_nav_side', $this->getModel()->getInNavSide());
    }

    public function buildInSitemap()
    {
        return Form::buildBoolean('inSitemap' . $this->unique, 'in_sitemap', $this->getModel()->getInSitemap());
    }

    public function buildInBreadcrumb()
    {
        return Form::buildBoolean('inBreadcrumb' . $this->unique, 'in_breadcrumb', $this->getModel()->getInBreadcrumb());
    }

    public function buildInNavFooter()
    {
        return Form::buildBoolean('inNavFooter' . $this->unique, 'in_nav_footer', $this->getModel()->getInNavFooter());
    }

    public function buildFriendlyUrlPath()
    {
        return Form::buildField('friendlyUrlPath' . $this->unique, $this->friendlyUrlPathBuilder, $this->getModel()->getFriendlyUrlPath());
    }

    public function validateFriendlyUrl()
    {
        return Validate::validate('friendlyUrl', $this->getModel()->getFriendlyUrl(), $this->friendlyUrlValidator);
    }

    public function buildFriendlyUrl()
    {
        return Form::buildField('friendlyUrl' . $this->unique, $this->friendlyUrlBuilder, $this->getModel()->getFriendlyUrl());
    }

    public function validateRedirect()
    {
        return Validate::validate('redirect', $this->getModel()->getRedirect(), $this->redirectValidator);
    }

    public function buildRedirect()
    {
        return Form::buildField('redirect' . $this->unique, $this->redirectBuilder, $this->getModel()->getRedirect());
    }

    public function validateClass()
    {
        return Validate::validate('class', $this->getModel()->getClass(), $this->classValidator);
    }

    public function buildClass()
    {
        return Form::buildField('class' . $this->unique, $this->classBuilder, $this->getModel()->getClass());
    }

    public function validatePermission()
    {
        return Validate::validate('permission', $this->getModel()->getPermission(), $this->permissionValidator);
    }

    public function buildPermission()
    {
        return Form::buildField('permission' . $this->unique, $this->permissionBuilder, $this->getModel()->getPermission());
    }

    public function validateDisplayOrder()
    {
        return Validate::validate('displayOrder', $this->getModel()->getDisplayOrder(), $this->displayOrderValidator);
    }

    public function buildDisplayOrder()
    {
        return Form::buildField('displayOrder' . $this->unique, $this->displayOrderBuilder, $this->getModel()->getDisplayOrder());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
