<div class="rb-paging">
    <div class="items">
        <span id="item-count"><?php echo $count ?></span> <?php echo $this->lang('items') ?>
    </div>
    <div class="nav rb-activator" data-active="<?php echo $current ?>" id="paging">
        <?php if ($current > 5) { ?>
            <a class="prev" href="<?php echo $url . ($current - 5) . $urlExtension ?>"><<</a>
        <?php } else { ?>
            <span class="prev"><<</span>
        <?php } ?>
        <?php if ($current > 1) { ?>
            <a class="prev" href="<?php echo $url . ($current - 1) . $urlExtension ?>"><</a>
        <?php } else { ?>
            <span class="prev"><</span>
        <?php }
        for ($i = $first; $i <= $last; $i++) { ?>
            <a data-menu="<?php echo $i ?>" href="<?php echo $url . $i . $urlExtension ?>"><?php echo $i ?></a>
        <?php }
        if ($current < $pages) { ?>
            <a class="next" href="<?php echo $url . ($current + 1) . $urlExtension ?>">></a>
        <?php } else { ?>
            <span class="next">></span>
        <?php } ?>
        <?php if ($current + 5 < $pages) { ?>
            <a class="next" href="<?php echo $url . ($current + 5) . $urlExtension ?>">>></a>
        <?php } else { ?>
            <span class="next">>></span>
        <?php } ?>
    </div>
</div>
