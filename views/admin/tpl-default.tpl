<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title ?> - <?php echo $site ?></title>
    <link rel="apple-touch-icon" href="/images/brand-touch.png" />
    <?php $this->renderCss() ?>
</head>
<body>
    <?php echo $notifications ?>
    <div class="rb-header">
        <div class="logo">
            <h1>Rebond</h1>
            <a href="/"><img alt="Rebond" src="/images/brand-icon.png" /></a>
        </div>
        <div class="right">
            <a href="#" id="r-search"><?php echo $this->lang('search') ?></a>
            <a href="<?php echo $siteUrl ?>" target="_blank"><?php echo $this->lang('site_view') ?></a>
            <?php echo $langSelector ?>
            <a href="#" id="rb-profile" class="modal-link"
               data-title="<?php echo $signedUser->getFullName() ?>"
               data-modal="profile"
               data-class="rb-profile-bloc"
            >
                <img src="<?php echo $this->showFromModel($signedUser->getAvatar(), 42, 42) ?>" alt="profile" />
            </a>
        </div>
        <div id="modal-profile" class="modal">
            <img src="<?php echo $this->showFromModel($signedUser->getAvatar(), 120, 120) ?>" alt="profile" />
            <div class="option">
                <a href="/profile"><?php echo $this->lang('profile_my') ?></a>
                <a href="/profile/settings"><?php echo $this->lang('settings_my') ?></a>
                <a href="/profile/feedback"><?php echo $this->lang('feedback') ?></a>
                <a href="/profile/help"><?php echo $this->lang('help') ?></a>
                <form action="/profile/sign-out" method="POST" name="form" id="form">
                    <input type="submit" class="rb-link" name="sign-out" value="<?php echo $this->lang('sign_out') ?>">
                </form>
            </div>
        </div>
    </div>
    <?php echo $menu ?>
    <div class="rb-container">
        <?php if (isset($navSide)) { ?>
            <div id="rb-nav-side">
                <i id="rb-expand" class="fa fa-lg fa-arrow-circle-right"></i>
                <i id="rb-collapse" class="fa fa-lg fa-arrow-circle-left"></i>
                <?php echo $navSide ?>
            </div>
        <?php } ?>
        <div id="rb-layout">
            <?php echo $layout ?>
        </div>
    </div>
    <a href="#" id="up"><i class="fa fa-2x fa-arrow-circle-up"></i></a>
    <input type="hidden" id="user-id" value="<?php echo $signedUser->getId() ?>" />
    <input type="hidden" id="user-dev" value="<?php echo $signedUser->getIsDev() ?>" />
    <input type="hidden" id="js-launcher" value="<?php $this->e($jsLauncher) ?>" />
    <?php $this->debug() ?>
    <?php $this->renderJs() ?>
    <?php $this->e($footer) ?>
</body>
</html>