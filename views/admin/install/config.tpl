<p>Now that you have been authenticated, you need to open the configuration file [<b class="text-important">\files\config.yaml</b>] and edit it according to your settings.</p>
<ul>
    <li class="text-warning">Update your salt key. It should be unique for each site.</li>
    <li>Create a database in your MySQL database, and update your database settings. The password needs to be encrypted using:
        <i class="text-important">vendor/bin/encode password</i>. It will return your password encrypted.</li>
</ul>
<p>Once done, you can start the installation.</p>

<form method="POST">
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnConfig">Config file checked</button>
    </div>
</form>
