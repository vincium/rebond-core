<h2>
    Rebond installation
</h2>
<div class="rb-step rb-activator" data-active="<?php echo $active ?>">
    <div data-menu="auth">1. Authentication</div>
    <div data-menu="config">2. Configuration</div>
    <div data-menu="db">3. Database</div>
    <div data-menu="user">4. User</div>
    <div data-menu="launch">5. Launch</div>
</div>
