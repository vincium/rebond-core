<div class="rb-filter">
    <div>
        <div><?php echo $this->lang('module') ?></div>
        <?php echo \Rebond\Services\Form::buildList('module', $modules, null, false) ?>
    </div>
    <div id="status-bar" class="hide">
        <div><?php echo $this->lang('version') ?></div>
        <div class="rb-activator rb-option" data-active="published" id="content-version">
            <a href="#published" data-menu="published" id="published">
                <?php echo $this->lang('published') ?>
            </a>
            <a href="#pending" data-menu="pending" id="pending">
                <?php echo $this->lang('pending') ?>
            </a>
            <a href="#deleted" data-menu="deleted" id="deleted">
                <?php echo $this->lang('deleted') ?>
            </a>
        </div>
    </div>
</div>