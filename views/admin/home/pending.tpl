<?php if (count($items) > 0) { ?>
<h2><?php echo $this->lang('pending_items') ?></h2>
<table>
    <tr>
        <th><?php echo $this->lang('module') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('created_date') ?></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo $this->lang($item->getModule()->getTitle()) ?></td>
        <td><a href="/content/#<?php echo $item->getModule()->getName() ?>/pending"><?php echo $item->getTitle()
            ?></a></td>
        <td><?php echo $item->getCreatedDate()->format() ?></td>
    </tr>
    <?php } ?>
</table>
<?php } ?>
