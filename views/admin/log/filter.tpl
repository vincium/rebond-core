<div class="rb-filter">

    <div>
        <form action="/tools/logs?log=<?php echo $selectedLog ?>" method="POST">
            <button type="submit" class="rb-btn" name="btnClear"><?php echo $this->lang('clear') ?></button>
        </form>
    </div>

    <div class="log">
        <div><?php echo $this->lang('logs') ?></div>
        <select class="input" id="log-files">
        <?php foreach ($logList as $log) { ?>
            <option value="/tools/logs?log=<?php echo $log['file'] ?>"<?php echo $log['selected'] ?>>
                <?php echo $log['file'] ?>
            </option>
        <?php } ?>
        </select>
    </div>
</div>