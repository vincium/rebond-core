<!DOCTYPE html>
<html class="signin">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title ?> - <?php echo $site ?></title>
    <link rel="apple-touch-icon" href="/images/brand-touch.png" />
    <?php $this->renderCss() ?>
</head>
<body>
    <?php echo $notifications ?>
    <div class="rb-header">
        <div class="right">
            <a href="<?php echo $siteUrl ?>" target="_blank"><?php echo $this->lang('site_view') ?></a>
            <?php echo $langSelector ?>
        </div>
    </div>
    <div class="rb-container">
        <div class="rb-signin">
            <?php $this->e($column1) ?>
        </div>
    </div>
    <input type="hidden" id="js-launcher" value="<?php $this->e($jsLauncher) ?>" />
    <?php $this->renderJs() ?>
    <?php $this->e($footer) ?>
</body>
</html>