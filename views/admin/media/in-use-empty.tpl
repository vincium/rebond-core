<div class="in-use">
    <h2><?php echo $media->getTitle() ?></h2>
    <a href="/media/edit/?id=<?php echo $media->getId() ?>">
        <img src="<?php echo $this->showFromModel($media, 128, 128) ?>" />
    </a>
</div>

<div class="status-error"><?php echo $this->lang('error') ?></div>
<p><?php echo $this->lang('error_media_link_setup') ?></p>
