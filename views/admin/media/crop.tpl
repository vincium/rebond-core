<div class="crop">
    <h2><?php echo $this->lang('crop') ?>: <?php echo $model->getTitle() ?></h2>
    <p><?php echo $this->lang('intro_crop') ?></p>
    <div id="crop-interface">
        <img id="crop-img" src="<?php echo $this->showFromModel($model, 920, 900, \Rebond\Enums\Core\Ratio::KEEP_MIN) ?>" alt="<?php echo $model->getAlt() ?>" />
    </div>
    <br />
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
        <div class="rb-form-item">
            x: <input id="x" type="text" class="input mini" name="x" value="0" readonly="readonly" />
            y: <input id="y" type="text" class="input mini" name="y" value="0" readonly="readonly" />
            w: <input id="w" type="text" class="input mini" name="w" value="0" readonly="readonly" />
            h: <input id="h" type="text" class="input mini" name="h" value="0" readonly="readonly" />
        </div>
        <div class="rb-form-item">
            <button type="submit" class="rb-btn" name="btnSave" id="btnSaveCrop"><?php echo $this->lang('image_crop') ?></button>
            <a href="#" class="rb-btn rb-btn-blank go-back"><?php echo $this->lang('cancel') ?></a>
        </div>
    </form>
</div>