<h2><?php echo $this->lang('models_edit') ?></h2>
<div class="rb-list">
    <a href="/dev/generator-edit/?g=app">App</a>
    <a href="/dev/generator-edit/?g=bus">Bus</a>
    <a href="/dev/generator-edit/?g=core">Core</a>
    <a href="/dev/generator-edit/?g=cms">Cms</a>
</div>

<h2><?php echo $this->lang('generate') ?></h2>

<?php if($success) { ?>
<div class="status-success"><?php echo $this->lang('generated', [$package]) ?></div>
<?php } ?>

<?php if ($errorCount > 0) { ?>
    <a href="#" id="generate-filter"><?php echo $this->lang('error_toggle') ?></a>
    <div class="status-error"><?php echo $errorCount . ' ' . $this->lang('errors') ?></div>
<?php } ?>

<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" class="editor">
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildBoolean('backup', 'backup_create') ?>
        <?php echo \Rebond\Services\Form::buildBoolean('generateTpl', 'generated_create') ?>
    </div>
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="package" value="app">App</button>
        <button type="submit" class="rb-btn" name="package" value="bus">Bus</button>
        <button type="submit" class="rb-btn" name="package" value="core">Core</button>
        <button type="submit" class="rb-btn" name="package" value="cms">Cms</button>
    </div>
</form>

<?php if ($message != '') { ?>
    <div id="rb-generator-response">
        <?php echo $message ?>
    </div>
<?php } ?>
