<h2><?php echo $this->lang('developer') ?></h2>
<p><?php echo $this->lang('intro_dev') ?></p>
<ul class="list-menu">
    <li>
        <i class="fa fa-2x fa-fw fa-magic"></i>
        <a href="/dev/generator"><?php echo $this->lang('generator') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-database"></i>
        <a href="/dev/database"><?php echo $this->lang('database') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-reply"></i>
        <a href="/dev/feedback"><?php echo $this->lang('feedback') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-medkit"></i>
        <a href="/dev/test"><?php echo $this->lang('tests') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-trash"></i>
        <a href="/dev/bin"><?php echo $this->lang('bin') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-refresh"></i>
        <a href="/dev/reinitialize"><?php echo $this->lang('reinitialize') ?></a>
    </li>
</ul>
