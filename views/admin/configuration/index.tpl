<h2><?php echo $this->lang('configuration') ?></h2>
<p><?php echo $this->lang('intro_configuration') ?></p>
<ul class="list-menu">
    <li>
        <i class="fa fa-2x fa-fw fa-home"></i>
        <a href="/configuration/site"><?php echo $this->lang('site') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-link"></i>
        <a href="/configuration/media-link"><?php echo $this->lang('media_link') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-language"></i>
        <a href="/configuration/lang"><?php echo $this->lang('lang') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-bookmark-o"></i>
        <a href="/configuration/cache"><?php echo $this->lang('cache') ?></a>
    </li>
</ul>
