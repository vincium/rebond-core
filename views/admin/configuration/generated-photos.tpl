<h2><?php echo $this->lang('generated_photos') ?> (<?php echo count($media) ?>)</h2>

<div class="rb-filter">
    <div>
        <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
            <button type="submit" class="rb-btn" name="btnCleanup"><?php echo $this->lang('cleanup') ?></button>
        </form>
    </div>
</div>
<h3><?php echo $this->lang('media') ?></h3>

<div class="rb-grid">
    <?php foreach($media as $main => $generated) { ?>
    <div>
        <a href="<?php echo $path . $main ?>" target="_blank">
            <img src="<?php echo $path . $main ?>" width="64px"/>
        </a>
        <?php foreach ($generated as $media) { ?>
        <div><a href="<?php echo $path . $media ?>" target="_blank"><?php echo $media ?></a></div>
        <?php } ?>
    </div>
    <?php } ?>
</div>
