<h2>UserSecurity</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('user') ?></th>
        <td><?php echo $item->getUser() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('secure') ?></th>
        <td><?php echo $item->getSecure() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('type') ?></th>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('expiry_date') ?></th>
        <td><?php echo $item->getExpiryDate()->format('date') ?></td>
    </tr>

</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>