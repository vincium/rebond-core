<?php echo $this->renderTitle('role', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('summary') ?>
            <?php echo $item->req('summary') ?>
            <?php echo $item->buildSummary() ?>
        </label>
        <?php echo $item->getFieldError('summary') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/configuration/roles" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
