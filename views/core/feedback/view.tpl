<h2>Feedback</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('user') ?></th>
        <td><?php echo $item->getUser() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('type') ?></th>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('description') ?></th>
        <td><?php echo $item->getDescription() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('created_date') ?></th>
        <td><?php echo $item->getCreatedDate()->format() ?></td>
    </tr>
</table>
<a href="/dev/feedback"><?php echo $this->lang('back_to_list') ?></a>