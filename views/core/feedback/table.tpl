<table class="list">
    <tr>
        <th><?php echo $this->lang('user') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('type') ?></th>
        <th><?php echo $this->lang('created_date') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo $item->getUser() ?></td>
        <td><a href="/dev/feedback-view/?id=<?php echo $item->getId() ?>"><?php echo $item->getTitle() ?></a></td>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
        <td><?php echo $item->getCreatedDate()->format() ?></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Core" data-entity="Feedback" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Core" data-entity="Feedback" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>