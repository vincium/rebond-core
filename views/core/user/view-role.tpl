<h2>User Roles</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('roles') ?></th>
        <td>
            <?php echo Rebond\Services\View::viewForeignKeyLink($roles, $allRoles) ?>
        </td>
    </tr>

</table>
<a href="/user"><?php echo $this->lang('back_to_list') ?></a>
