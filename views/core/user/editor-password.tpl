<h2><?php echo $this->lang('password_change') ?></h2>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('password_current') ?>
            <?php echo $item->req('password') ?>
            <?php echo $item->buildPassword() ?>
        </label>
        <?php echo $item->getFieldError('password') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('password_new') ?>
            <?php echo $item->req('password') ?>
            <?php echo $item->buildPassword('new') ?>
        </label>
        <?php echo $item->getFieldError('passwordnew') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId(),'password_reset') ?>
        <a href="/user" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
