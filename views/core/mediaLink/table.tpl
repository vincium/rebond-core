<table class="list">
    <tr>
<th><?php echo $this->lang('field') ?></th>
<th><?php echo $this->lang('package') ?></th>
<th><?php echo $this->lang('module') ?></th>
<th><?php echo $this->lang('id_field') ?></th>
<th><?php echo $this->lang('title_field') ?></th>
<th><?php echo $this->lang('url') ?></th>
<th><?php echo $this->lang('filter') ?></th>
<th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
<td><a href="/configuration/media-link/edit?id=<?php echo $item->getId() ?>"><?php echo $item->getField() ?></a></td>
<td><?php echo ucfirst($item->getPackageValue()) ?></td>
<td><?php echo $item->getEntity() ?></td>
<td><?php echo $item->getIdField() ?></td>
<td><?php echo $item->getTitleField() ?></td>
<td><?php echo $item->getUrl() ?></td>
<td><?php echo $item->getFilter() ?></td>
<td><?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
            <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Core" data-entity="MediaLink" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
        <?php } else { ?>
            <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
        <?php } ?></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Core" data-entity="MediaLink" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Core" data-entity="MediaLink" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>