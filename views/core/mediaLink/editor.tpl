<?php echo $this->renderTitle('media_link', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('package') ?>
            <?php echo $item->req('package') ?>
            <?php echo $item->buildPackage() ?>
        </label>
        <?php echo $item->getFieldError('package') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('entity') ?>
            <?php echo $item->req('entity') ?>
            <?php echo $item->buildEntity() ?>
        </label>
        <?php echo $item->getFieldError('entity') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('field') ?>
            <?php echo $item->req('field') ?>
            <?php echo $item->buildField() ?>
        </label>
        <?php echo $item->getFieldError('field') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('id_field') ?>
            <?php echo $item->req('idField') ?>
            <?php echo $item->buildIdField() ?>
        </label>
        <?php echo $item->getFieldError('idField') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title_field') ?>
            <?php echo $item->req('titleField') ?>
            <?php echo $item->buildTitleField() ?>
        </label>
        <?php echo $item->getFieldError('titleField') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('url') ?>
            <?php echo $item->req('url') ?>
            <?php echo $item->buildUrl() ?>
        </label>
        <?php echo $item->getFieldError('url') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('filter') ?>
            <?php echo $item->req('filter') ?>
            <?php echo $item->buildFilter() ?>
        </label>
        <?php echo $item->getFieldError('filter') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('status') ?>
            <?php echo $item->req('status') ?>
            <?php echo $item->buildStatus() ?>
        </label>
        <?php echo $item->getFieldError('status') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/configuration/media-link" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
