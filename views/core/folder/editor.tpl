<?php echo $this->renderTitle('folder', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('parent') ?>
            <?php echo $item->req('parent') ?>
            <?php echo $item->buildParent() ?>
        </label>
        <?php echo $item->getFieldError('parent') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('display_order') ?>
            <?php echo $item->req('displayOrder') ?>
            <?php echo $item->buildDisplayOrder() ?>
        </label>
        <?php echo $item->getFieldError('displayOrder') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('status') ?>
            <?php echo $item->req('status') ?>
            <?php echo $item->buildStatus() ?>
        </label>
        <?php echo $item->getFieldError('status') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/media" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
