<h2>Folder</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('parent') ?></th>
        <td><?php echo $item->getParent() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('display_order') ?></th>
        <td><?php echo $item->getDisplayOrder() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>