<input type="hidden" class="selected" id="media-selected" value="<?php echo $field ?>" />
<div id="media-grid">
    <?php foreach ($items as $item) { ?>
        <div class="media" data-id="<?php echo $item->getId() ?>" data-title="<?php echo $item->getTitle() ?>">
            <div class="action">
                <span class="title"><?php echo $item->getTitle() ?></span>
            </div>
            <div class="view">
                <img src="<?php echo $this->showFromModel($item, 128, 128) ?>" alt="<?php echo $item->getAlt() ?>" />
            </div>
        </div>
    <?php } ?>
</div>