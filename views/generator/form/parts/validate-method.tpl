<?php if (in_array($type, ['email'])) { ?>
    public function validate<?php echo ucfirst($propertyName) ?>($checkExisting = true)
    {
        $vrf = Validate::validate('<?php echo $propertyName ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>(), $this-><?php echo $propertyName ?>Validator);
        if ($vrf->getResult() == Result::ERROR) {
            return $vrf;
        }
        if ($checkExisting && \Rebond\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository::<?php echo $propertyName ?>Exists($this->getModel()->get<?php echo ucfirst($propertyName) ?>(), $this->getModel()->getId()) > 0) {
            $vrf->setResult(Result::ERROR);
            $vrf->setMessage(Lang::lang('<?php echo $type ?>_exist'));
        }
        return $vrf;
    }

<?php } else if (in_array($type, ['password'])) { ?>
    public function validate<?php echo ucfirst($propertyName) ?>($confirm = null)
    {
        if (isset($confirm)) {
            $this-><?php echo $propertyName ?>Validator[] = ['equal' => $confirm];
        }
        return Validate::validate('<?php echo $propertyName ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>(), $this-><?php echo $propertyName ?>Validator);
    }

<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    public function validate<?php echo ucfirst($propertyName) ?>()
    {
        return Validate::validate('<?php echo $propertyName ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>Id(), $this-><?php echo $propertyName ?>Validator);
    }

<?php } else if (!in_array($type, ['primaryKey', 'bool', 'time', 'hidden']) || ($type == 'primaryKey' && !$isAutoIncrement)) { ?>
    public function validate<?php echo ucfirst($propertyName) ?>()
    {
        return Validate::validate('<?php echo $propertyName ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>(), $this-><?php echo $propertyName ?>Validator);
    }

<?php } ?>
