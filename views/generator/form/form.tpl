<#php
<?php echo $license ?>

namespace <?php echo $namespace ?>\Forms\<?php echo $package ?>;

use Own\Models\<?php echo $package ?>\<?php echo $entity ?>;

class <?php echo $entity ?>Form extends <?php echo $extends ?>Base<?php echo $entity ?>Form
{
    /**
     * @param <?php echo $entity ?> $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }
}
