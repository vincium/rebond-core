    <tr>
        <th><#php echo $this->lang('<?php echo $property ?>') ?></th>
<?php if ($type == 'primaryKey' && isset($pkRelation)) { ?>
        <td><#php echo $item->get<?php echo ucfirst($property) ?>Item() ?></td>
<?php } else if (in_array($type, ['media'])) { ?>
        <td class="no-padding"><img src="<#php echo $this->showFromModel($item->get<?php echo ucfirst($property) ?>(), 128, 128) ?>" alt="<#php echo $item->get<?php echo ucfirst($property) ?>()->getTitle() ?>" /></td>
<?php } else if (in_array($type, ['datetime', 'date', 'time'])) { ?>
        <td><#php echo $item->get<?php echo ucfirst($property) ?>()->format('<?php echo $type ?>') ?></td>
<?php } else if (in_array($type, ['enum'])) { ?>
        <td><span class="enum <?php echo $property ?>-<#php echo $item->get<?php echo ucfirst($property) ?>() ?>"><#php echo $item->get<?php echo ucfirst($property) ?>Value() ?></span></td>
<?php } else if (in_array($type, ['bool'])) { ?>
        <td><span class="bool-<#php echo $item->get<?php echo ucfirst($property) ?>() ?>"><#php echo $item->get<?php echo ucfirst($property) ?>() ?></span></td>
<?php } else { ?>
        <td><#php echo $item->get<?php echo ucfirst($property) ?>() ?></td>
<?php } ?>
    </tr>
