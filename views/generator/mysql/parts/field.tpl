<?php if (in_array($type, ['primaryKey'])) { ?>
    <?php echo $property ?> <?php echo $sqlType ?> NOT NULL <?php echo $autoIncrement ?> 
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    <?php echo $property ?>_id <?php echo $sqlType ?> NOT NULL
<?php } else if (in_array($type, ['integer'])) { ?>
    `<?php echo $property ?>` <?php echo $sqlType ?> NOT NULL
<?php } else if (in_array($type, ['numeric'])) { ?>
    `<?php echo $property ?>` FLOAT NOT NULL
<?php } else if (in_array($type, ['enum', 'status', 'bool'])) { ?>
    `<?php echo $property ?>` TINYINT UNSIGNED NOT NULL
<?php } else if (in_array($type, ['datetime', 'createdDate', 'modifiedDate'])) { ?>
    `<?php echo $property ?>` DATETIME NOT NULL
<?php } else if (in_array($type, ['date'])) { ?>
    `<?php echo $property ?>` DATE NOT NULL
<?php } else if (in_array($type, ['time'])) { ?>
    `<?php echo $property ?>` TIME NOT NULL
<?php } else if (in_array($type, ['text', 'richText'])) { ?>
    `<?php echo $property ?>` TEXT COLLATE utf8_unicode_ci NOT NULL
<?php } else if (!in_array($type, ['foreignKeyLink', 'singleKeyLink'])) { ?>
    `<?php echo $property ?>` VARCHAR(<?php echo $maxLength ?>) COLLATE utf8_unicode_ci NOT NULL
<?php } ?>
