    /**
     * @param int $id
     * @return int
     */
    public static function deleteBy<?php echo ucfirst($propertyName) ?>Id($id)
    {
        $db = new Data();
        $query = 'DELETE FROM <?php echo $table ?> WHERE <?php echo $property ?>_id = ?';
        return $db->execute($query, [$id]);
    }
