    /**
     * @param string $<?php echo $propertyName ?>

     * @return <?php echo $entity ?>

     */
    public static function loadBy<?php echo ucfirst($propertyName) ?>($<?php echo $propertyName ?>)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $property ?> = ?', $<?php echo $propertyName ?>]);
        return self::map($db);
    }

    /**
     * @param string $<?php echo $propertyName ?>

     * @param int $id
     * @return bool
     */
    public static function <?php echo $propertyName ?>Exists($<?php echo $propertyName ?>, $id)
    {
        $db = new Data();
        $db->buildQuery('select', 'count(<?php echo $sqlEntity ?>.<?php echo $primaryKey ?>)');
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $property ?> = ?', $<?php echo $propertyName ?>]);
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $primaryKey ?> != ?', $id]);
        return $db->count();
    }

