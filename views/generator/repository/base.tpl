<#php
<?php echo $license ?>

namespace <?php echo $namespace ?>\Repository\<?php echo $package ?>;

use <?php echo $mainNamespace ?>\Models\<?php echo $package ?>\<?php echo $entity ?>;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class Base<?php echo $entity ?>Repository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = <?php echo $sqlEntity ?>

     * @return string
     */
    public static function getList(array $properties = [], $alias = '<?php echo $sqlEntity ?>')
    {
        if (empty($properties)) {
            $list =
<?php echo $selectList ?>
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
<?php if (isset($primaryKey)) { ?>
        $db->buildQuery('select', 'count(<?php echo $sqlEntity ?>.<?php echo $primaryKey ?>)');
<?php } else { ?>
        $db->buildQuery('select', 'count(*)');
<?php } ?>
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->extendQuery($options);
        return $db->count();
    }

<?php if (isset($primaryKey)) { ?>
    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return <?php echo $entity ?>

     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new <?php echo $entity ?>() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $primaryKey ?> = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new <?php echo $entity ?>();
        }
        return $model;
    }
<?php } else if (count($multipleKeyVar) > 0) { ?>
    /**
<?php foreach ($multipleKeyVar as $key) { ?>
     * @param int <?php echo $key ?>

<?php } ?>
     * @return <?php echo $entity ?>

     */
    public static function loadById(<?php echo implode(', ', $multipleKeyVar) ?>)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
<?php foreach ($multipleKeySql as $key => $column) { ?>
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $column ?> = ?', <?php echo $multipleKeyVar[$key] ?>]);
<?php } ?>
        return self::map($db);
    }
<?php } ?>

    /**
     * @param array $options = []
     * @return <?php echo $entity ?>

     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

<?php echo $loadBy ?>
<?php echo $loadUserMethods ?>

    /**
     * @param array $options = []
     * @return <?php echo $entity ?>[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

<?php if (isset($primaryKey) && $isAutoIncrement) { ?>
    /**
     * @param <?php echo $entity ?> $model
     * @return int
     */
    public static function save(<?php echo $entity ?> $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO <?php echo $table ?> (<?php echo $insertList ?>) VALUES (<?php echo $insertListParams ?>)';
            $params = [
<?php echo $insertValueList ?> 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE <?php echo $table ?> SET ';
            $params = [];
<?php echo $updateList ?>
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE <?php echo $primaryKey ?> = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }
<?php } else if (isset($primaryKey) && !$isAutoIncrement) { ?>
    /**
     * @param <?php echo $entity ?> $model
     * @return int
     */
    public static function save(<?php echo $entity ?> $model)
    {
        $db = new Data();
        $query = 'INSERT INTO <?php echo $table ?> (<?php echo $primaryKey ?>, <?php echo $insertList ?>) VALUES (?,<?php echo $insertListParams ?>) ON DUPLICATE KEY UPDATE <?php echo $primaryKey ?> = LAST_INSERT_ID(<?php echo $primaryKey ?>), ';
        $params = [
            $model->getId(),
<?php echo $insertValueList ?> 
        ];
<?php echo $updateList ?> 
        $query = rtrim(trim($query), ',');
        $id = $db->execute($query, $params);
        $model->setId($id);
        return $id;
    }
<?php } else if (count($multipleKeyVar) > 0) { ?>
    /**
     * @param <?php echo $entity ?>[] $models
     * @return bool
     */
    public static function saveAll(array $models)
    {
        if (count($models) == 0) {
            return true;
        }
        foreach ($models as $model) {
            $model->save();
        }
        return true;
    }

    /**
     * @param <?php echo $entity ?> $model
     * @return int
     */
    public static function save(<?php echo $entity ?> $model)
    {
        $query = 'INSERT INTO <?php echo $table ?> (<?php echo $insertList ?>) VALUES (<?php echo $insertListParams ?>)';
        $query .= ' ON DUPLICATE KEY UPDATE ';
        $params = [
<?php echo $insertValueList ?> 
        ];

<?php echo $updateOnDuplicate ?>
        $query = rtrim(trim($query), ',');
        $db = new Data();
        return $db->execute($query, $params);
    }
<?php } else { ?>
    /**
     * @param <?php echo $entity ?>[] $models
     * @return bool
     */
    public static function saveAll(array $models)
    {
        if (count($models) === 0) {
            return true;
        }
        foreach ($models as $model) {
            $model->insert();
        }
        return true;
    }

    /**
     * Insert a <?php echo $entity ?>

     * @param <?php echo $entity ?>\Model $model
     * @return int
     */
    public static function insert(Model $model)
    {
        $query = 'INSERT INTO <?php echo $table ?> (<?php echo $insertList ?>) VALUES (<?php echo $insertListParams ?>)';
        $params = [
<?php echo $insertValueList ?> 
        ];
        $db = new Data();
        return $db->execute($query, $params);
    }
<?php } ?>

<?php echo $updateStatus ?>
<?php if (isset($primaryKey)) { ?>
    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM <?php echo $table ?> WHERE <?php echo $primaryKey ?> = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }
<?php } else if (count($multipleKeyVar) > 0) { ?>
    /**
<?php foreach ($multipleKeyVar as $key) { ?>
     * @param int <?php echo $key ?>

<?php } ?>
     * @return int
     */
    public static function deleteByIds(<?php echo implode(', ', $multipleKeyVar) ?>)
    {
        $query = 'DELETE FROM <?php echo $table ?> WHERE 1';
        $params = [];
<?php
foreach ($multipleKeySql as $key) {  ?>
        $query .= ' AND <?php echo $key ?> = ?';
        $params[] = $<?php echo \Rebond\Services\Converter::toCamelCase($key) ?>;
<?php } ?>
        $db = new Data();
        return $db->execute($query, $params);
    }
<?php } ?>

<?php echo $deleteBy ?>

    /**
     * @param array $row
     * @param string $alias = <?php echo $sqlEntity ?>

     * @return <?php echo $entity ?>

     */
    public static function join(array $row, $alias = '<?php echo $sqlEntity ?>')
    {
        if (!isset($row[$alias . '<?php echo isset($firstKey) ? $firstKey : 'Id' ?>'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return <?php echo $entity ?>

     */
    protected static function mapper(array $row, $alias = '<?php echo $sqlEntity ?>')
    {
        $model = new <?php echo $entity ?>(false);
<?php echo $mapList ?>
        return $model;
    }
}
