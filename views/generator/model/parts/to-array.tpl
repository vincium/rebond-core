<?php if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
            '<?php echo $propertyName ?>Id' => $this->get<?php echo ucfirst($propertyName) ?>Id(),
<?php } else if (in_array($type, ['date', 'datetime', 'time'])) { ?>
            '<?php echo $propertyName ?>' => $this->get<?php echo ucfirst($propertyName) ?>()->format(),
<?php } else if (!in_array($type, ['modifiedDate', 'createdDate', 'password', 'foreignKeyLink', 'singleKeyLink'])) { ?>
            '<?php echo $propertyName ?>' => $this->get<?php echo ucfirst($propertyName) ?>(),
<?php } ?>