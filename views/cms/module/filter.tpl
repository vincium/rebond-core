<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
<div class="rb-filter">
    <div>
        <div><?php echo $this->lang('module') ?></div>
        <?php echo \Rebond\Services\Form::buildItemList('moduleId', $items, 'id', 'title', '0', false) ?>
        <?php echo $this->buildSubmit(0, 'add') ?>
    </div>
    <div class="paging">
        <div class="items">
            <span id="item-count"><?php echo $count ?></span> <?php echo $this->lang('items') ?>
        </div>
    </div>
</div>
</form>