<table class="list">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('summary') ?></th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr>
            <td><a class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Module" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a></td>
            <td><?php echo $item->getTitle() ?></td>
            <td><?php echo $item->getSummary('preview') ?></td>
        </tr>
    <?php } ?>
</table>