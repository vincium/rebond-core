<table class="list">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('name') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('workflow') ?></th>
        <th><?php echo $this->lang('has_filter') ?></th>
        <th><?php echo $this->lang('has_content') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Module" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a></td>
        <td><?php echo $item->getName() ?></td>
        <td><a href="/cms/module-edit/?id=<?php echo $item->getId() ?>"><?php echo $this->lang($item->getTitle()) ?></a></td>
        <td><span class="enum workflow-<?php echo $item->getWorkflow() ?>"><?php echo $item->getWorkflowValue() ?></span></td>
        <td><span class="bool-<?php echo $item->getHasFilter() ?>"><?php echo $item->getHasFilter() ?></span></td>
        <td><span class="bool-<?php echo $item->getHasContent() ?>"><?php echo $item->getHasContent() ?></span></td>
        <td><a href="#" class="status-3" data-package="Cms" data-entity="Module" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a></td>
    </tr>
    <?php } ?>
</table>