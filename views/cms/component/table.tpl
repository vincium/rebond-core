<table class="list">
    <tr>
        <th><?php echo $this->lang('module') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('method') ?></th>
        <th><?php echo $this->lang('type') ?></th>
        <th><?php echo $this->lang('can_be_cached') ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo $this->lang($item->getModule()->getTitle()) ?></td>
        <td><a href="/cms/component-edit/?id=<?php echo $item->getId() ?>"><?php echo $this->lang($item->getTitle()) ?></a></td>

        <td><?php echo $item->getMethod() ?></td>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
        <td><span class="bool-<?php echo $item->getCanBeCached() ?>"><?php echo $item->getCanBeCached() ?></span></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Component" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Cms" data-entity="Component" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Cms" data-entity="Component" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>