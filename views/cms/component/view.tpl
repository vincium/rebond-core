<h2>Component</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $this->lang($item->getTitle()) ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('module') ?></th>
        <td><?php echo $item->getModule() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('summary') ?></th>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('method') ?></th>
        <td><?php echo $item->getMethod() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('type') ?></th>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('can_be_cached') ?></th>
        <td><?php echo $item->getCanBeCached() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<p>
    <a href="#" class="rb-btn rb-btn-blank"><?php echo $this->lang('back_to_list') ?></a>
</p>