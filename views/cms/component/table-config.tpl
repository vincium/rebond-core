<table class="list">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('module') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('method') ?></th>
        <th><?php echo $this->lang('type') ?></th>
        <th><?php echo $this->lang('can_be_cached') ?></th>

    </tr>
    <?php foreach ($items as $item) { ?>
            <tr>
                <td><a class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Component" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a></td>
                <td><?php echo $item->getModule() ?></td>
                <td><?php echo $item->getTitle() ?></td>
                <td><?php echo $item->getMethod() ?></td>
                <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
            </tr>
    <?php } ?>
</table>