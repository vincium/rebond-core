<div class="rb-filter">
    <div>
        <span><?php echo $this->lang('components') ?></span>
    </div>
    <div class="paging">
        <div class="items">
            <span id="item-count"><?php echo $count ?></span> <?php echo $this->lang('items') ?>
        </div>
    </div>
</div>