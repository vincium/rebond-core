<?php echo $this->renderTitle('gadget', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('page') ?>
            <?php echo $item->req('page') ?>
            <?php echo $item->buildPage() ?>
        </label>
        <?php echo $item->getFieldError('page') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('component') ?>
            <?php echo $item->req('component') ?>
            <?php echo $item->buildComponent() ?>
        </label>
        <?php echo $item->getFieldError('component') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('col') ?>
            <?php echo $item->req('col') ?>
            <?php echo $item->buildCol() ?>
        </label>
        <?php echo $item->getFieldError('col') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('filter') ?>
            <?php echo $item->req('filter') ?>
            <?php echo $item->buildFilter() ?>
        </label>
        <?php echo $item->getFieldError('filter') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('custom_filter') ?>
            <?php echo $item->req('customFilter') ?>
            <?php echo $item->buildCustomFilter() ?>
        </label>
        <?php echo $item->getFieldError('customFilter') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('display_order') ?>
            <?php echo $item->req('displayOrder') ?>
            <?php echo $item->buildDisplayOrder() ?>
        </label>
        <?php echo $item->getFieldError('displayOrder') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('status') ?>
            <?php echo $item->req('status') ?>
            <?php echo $item->buildStatus() ?>
        </label>
        <?php echo $item->getFieldError('status') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="#" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
