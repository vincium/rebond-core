<h2>Page</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('parent') ?></th>
        <td><?php echo $item->getParent() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('template') ?></th>
        <td><?php echo $item->getTemplate() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('layout') ?></th>
        <td><?php echo $item->getLayout() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('css') ?></th>
        <td><?php echo $item->getCss() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('js') ?></th>
        <td><?php echo $item->getJs() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_nav_header') ?></th>
        <td><span class="bool-<?php echo $item->getInNavHeader() ?>"><?php echo $item->getInNavHeader() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_nav_side') ?></th>
        <td><span class="bool-<?php echo $item->getInNavSide() ?>"><?php echo $item->getInNavSide() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_sitemap') ?></th>
        <td><span class="bool-<?php echo $item->getInSitemap() ?>"><?php echo $item->getInSitemap() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_breadcrumb') ?></th>
        <td><span class="bool-<?php echo $item->getInBreadcrumb() ?>"><?php echo $item->getInBreadcrumb() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_nav_footer') ?></th>
        <td><span class="bool-<?php echo $item->getInNavFooter() ?>"><?php echo $item->getInNavFooter() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('friendly_url') ?></th>
        <td><?php echo $item->getFriendlyUrl() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('redirect') ?></th>
        <td><?php echo $item->getRedirect() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('class') ?></th>
        <td><?php echo $item->getClass() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('permission') ?></th>
        <td><?php echo $item->getPermission() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('display_order') ?></th>
        <td><?php echo $item->getDisplayOrder() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>