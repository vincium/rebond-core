<?php
require '../vendor/autoload.php';

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Services\Nav;

$app = App::create(AppLevel::ADMIN);
$app->launch();

// read request uri
list($module, $action) = Nav::readRequest($_SERVER['REQUEST_URI']);

$adminController = '\Rebond\Controller\Admin\\' . $module . 'Controller';
$ownController = '\Own\Controller\Admin\\' . $module . 'Controller';

// check controller
$request = class_exists($ownController) ? $ownController : $adminController;

if (!class_exists($request)) {
    throw new \Exception($module . '/' . $action, Code::ADMIN_PAGE_NOT_FOUND);
}

// call controller
$controller = new $request($app);

// check method
if (!method_exists($controller, $action)) {
    throw new \Exception($module . '/' . $action, Code::ADMIN_PAGE_NOT_FOUND);
}

// call method
echo $controller->$action();
