DROP TABLE IF EXISTS app_article;

CREATE TABLE `app_article` (
  `app_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `media_id` INT UNSIGNED NOT NULL,
  `description` TEXT COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`app_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO app_article VALUES (NULL, 0, '<p>Welcome to your site!</p>');
INSERT INTO app_article VALUES (NULL, 0, '<p>The site is not available for the moment.</p>');
INSERT INTO app_article VALUES (NULL, 0, '<p>An error occured. Go back to the <a href="/">homepage</a>.</p>');
